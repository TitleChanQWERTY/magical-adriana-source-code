import os

import pygame
from scene import scene_create_conf, scene_start, scene_player_profile
from start_logo_load import start
from sfx_colection import set_volume

icon = pygame.image.load("Assets/sprite/icon/1.png").convert_alpha()

pygame.display.set_icon(icon)
pygame.display.set_caption("[Magical Adriana]")

if __name__ == "__main__":
    start()
    set_volume()
    if os.path.exists("config.json"):
        scene_player_profile.scene()
    else:
        scene_create_conf.start_message()
    pygame.quit()
