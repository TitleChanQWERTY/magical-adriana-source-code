import group_conf
from scene import scene_config
from config import WINDOW_SIZE
import collider


def reset():
    collider.timeDamage = 120
    scene_config.isBoss = False
    scene_config.isBoss2 = False
    scene_config.isBoss3 = False
    scene_config.time_spawn_bos = 0
    scene_config.p.HEALTH = 100
    scene_config.p.AMMO = 150
    scene_config.p.MIND = 0
    scene_config.p.SCORE = 0
    scene_config.p.BOSS_KILL_COUNT = 0
    scene_config.p.ARMOR = 100
    scene_config.p.get_shield = 0
    scene_config.p.kill_count = 0
    scene_config.p.rect.x = WINDOW_SIZE[0]//2
    scene_config.p.rect.y = WINDOW_SIZE[1]//2+105
    scene_config.p.time_take_damage = 0
    scene_config.p.alphaMind = 0
    for i in range(4):
        scene_config.p.speed[i] = 0
    scene_config.p.timeDamageMind = 0
    scene_config.p.select_weapon = 0

    for enemy in group_conf.enemy_group:
        enemy.kill()
    for item in group_conf.item_group:
        item.kill()
    for particle in group_conf.particle_group:
        particle.kill()
    for bullet_p in group_conf.bullet_player_group:
        bullet_p.kill()
    for bullet_e in group_conf.bullet_enemy_group:
        bullet_e.kill()

    scene_config.timeCreateEye = 0
    scene_config.timeCreateSpider = 0
    scene_config.timeCreateProtator = 0
    scene_config.timeCreateDroneContainer = 0
    scene_config.timeCreateChain = 0
