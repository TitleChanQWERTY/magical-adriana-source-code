import pygame
import config
from config import sc, joystick_m, clock, WINDOW_SIZE, standart_font, key_image, check_gamepad
from scene import scene_config
import language_system
import os
import json

input_button = [pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha()]

input_txt = []

input_sys = 0

def save_calibrate():
    try:
        os.remove("gamepad.json")
    except FileNotFoundError:
        pass
    data = {"A": scene_config.control_gamepad["A"],
            "B": scene_config.control_gamepad["B"],
            "X": scene_config.control_gamepad["X"],
            "Y": scene_config.control_gamepad["Y"],
            "L_BUMPER": scene_config.control_gamepad["L_BUMPER"],
            "R_BUMPER": scene_config.control_gamepad["R_BUMPER"],
            "START": scene_config.control_gamepad["START"]}
    with open("gamepad.json", "w") as file:
        json.dump(data, file)
    return 0

def select_input(back_scene):
    global input_button, input_txt, input_sys
    running = True
    FPS = 60

    select = 0

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (55 * 5, 15 * 5))

    xPosSelector = WINDOW_SIZE[0]/2-205

    size = [0, 0, 0, 0]


    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["MAIN_MENU"],
                                                                 scene_config.AA_TEXT,
                                                                 (255, 95, 255))
    txt_exit_rect = txt_exit.get_rect(center=(103, 667))

    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    time_event = 0

    txt_select_info = pygame.font.Font(standart_font, 27).render(language_system.txt_lang["SELECT_ICON_GAMEPAD"], scene_config.AA_TEXT, (255, 0, 245))

    while running:
        if time_event >= 2:
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    running = False
                if e.type == pygame.JOYHATMOTION:
                    if e.value == (-1, 0):
                        select -= 1
                    elif e.value == (1, 0):
                        select += 1
                    select %= 3
                elif e.type == pygame.JOYBUTTONDOWN:
                    if e.button == scene_config.control_gamepad["A"]:
                        match select:
                            case 0:
                                input_sys = 0
                                input_button = [pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/b.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/x.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/y.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/lb.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/rb.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/start_xbox.png").convert_alpha()]
                                input_txt = ["A", "B", "X", "Y", "LB", "RB", "START"]
                            case 1:
                                input_sys = 1
                                input_button = [pygame.image.load("Assets/sprite/ui/gamepad_key/play/x.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/circle.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/square.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/triangle.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/l1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/r1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/start.png").convert_alpha()]
                                input_txt = ["X", "Cirlce", "Square", "Triangle", "L1", "R1", "START"]
                            case 2:
                                input_sys = 2
                                input_button = [pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/b.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/x.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/y.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/l1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/r1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/start_deck.png").convert_alpha()]
                                input_txt = ["A", "B", "X", "Y", "L1", "R1", "START"]
                        running = False
                        calibrate(back_scene)
                        return 0
                    elif e.button == scene_config.control_gamepad["B"]:
                        running = False
                        back_scene()
                        return 0
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_RETURN:
                        match select:
                            case 0:
                                input_sys = 0
                                input_button = [pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/b.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/x.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/y.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/lb.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/rb.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/start.png").convert_alpha()]
                                input_txt = ["A", "B", "X", "Y", "LB", "RB", "START"]
                            case 1:
                                input_sys = 1
                                input_button = [pygame.image.load("Assets/sprite/ui/gamepad_key/play/x.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/circle.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/square.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/triangle.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/l1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/r1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/start.png").convert_alpha()]
                                input_txt = ["X", "Cirlce", "Square", "Triangle", "L1", "R1", "START"]
                            case 2:
                                input_sys = 2
                                input_button = [pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/b.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/x.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/xbox/y.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/l1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/play/r1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/ui/gamepad_key/start_deck.png").convert_alpha()]
                                input_txt = ["A", "B", "X", "Y", "L1", "R1", "START"]
                        running = False
                        calibrate(back_scene)
                        return 0
                    elif e.key == pygame.K_ESCAPE:
                        running = False
                        back_scene()
                        return 0
                    if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                        select -= 1
                    if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                        select += 1
                    select %= 3
        else:
            time_event += 1
        
        if select == 0:
            xPosSelector = WINDOW_SIZE[0]/2-405
        elif select == 1:
            xPosSelector = WINDOW_SIZE[0]/2
        else:
            xPosSelector = WINDOW_SIZE[0]/2+405

        sc.fill((0, 0, 0))

        sc.blit(txt_select_info, txt_select_info.get_rect(center=(WINDOW_SIZE[0]/2, 46)))

        x_input_image = pygame.image.load("Assets/sprite/ui/gamepad_key/x-input.png").convert_alpha()
        d_input_image = pygame.image.load("Assets/sprite/ui/gamepad_key/d-input.png").convert_alpha()
        deck_input_image = pygame.image.load("Assets/sprite/ui/gamepad_key/deck_input.png").convert_alpha()

        x_input_image = pygame.transform.scale(x_input_image, (size[0], size[1]))
        d_input_image = pygame.transform.scale(d_input_image, (size[0], size[1]))
        deck_input_image = pygame.transform.scale(deck_input_image, (size[0], size[1]))

        txt_warning = pygame.font.Font(standart_font, size[2]).render(language_system.txt_lang["GAMEPAD_UNCONECTED"], scene_config.AA_TEXT, (205, 0, 0))

        d_input_txt = pygame.font.Font(standart_font, size[3]).render("Playstation", scene_config.AA_TEXT, (255, 255, 255))
        x_input_txt = pygame.font.Font(standart_font, size[3]).render("Xbox", scene_config.AA_TEXT, (255, 255, 255))
        deck_input_txt = pygame.font.Font(standart_font, size[3]).render("Steam Deck", scene_config.AA_TEXT, (255, 255, 255))

        for i, b in enumerate(size):
            size[i] += 20

        size[0] = max(0, min(47 * 4, size[0])) 
        size[1] = max(0, min(47 * 4, size[1])) 

        size[2] = max(0, min(20, size[2])) 
        size[3] = max(0, min(24, size[3])) 

        sc.blit(x_input_image, x_input_image.get_rect(center=(WINDOW_SIZE[0]/2-405, WINDOW_SIZE[1]/2-90)))
        sc.blit(d_input_image, d_input_image.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2-90)))
        sc.blit(deck_input_image, deck_input_image.get_rect(center=(WINDOW_SIZE[0]/2+405, WINDOW_SIZE[1]/2-90)))

        sc.blit(x_input_txt, x_input_txt.get_rect(center=(WINDOW_SIZE[0]/2-405, WINDOW_SIZE[1]/2+50)))
        sc.blit(d_input_txt, d_input_txt.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2+50)))
        sc.blit(deck_input_txt, deck_input_txt.get_rect(center=(WINDOW_SIZE[0]/2+405, WINDOW_SIZE[1]/2+50)))

        sc.blit(txt_warning, txt_warning.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]-30)))

        sc.blit(selector_img, selector_img.get_rect(center=(xPosSelector, WINDOW_SIZE[1]/2+50)))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)
        
        pygame.display.update()
        clock.tick(FPS)

def calibrate(back_scene):
    global input_button, input_txt, input_sys
    running = True
    FPS = 60

    for i, b in enumerate(input_button):
        input_button[i] = pygame.transform.scale(input_button[i], (15 * 5, 15 * 5))
    
    input_button[6] = pygame.transform.scale(input_button[6], (34 * 4, 11 * 4))
    
    select_step = 0

    txt_warning = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["GAMEPAD_UNCONECTED"], scene_config.AA_TEXT, (205, 0, 0))

    txt_press = pygame.font.Font(standart_font, 31).render(language_system.txt_lang["PRESS_TXT"], scene_config.AA_TEXT, (255, 0, 255))

    time_tap = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.JOYBUTTONDOWN and time_tap >= 1:
                match select_step:
                    case 0:
                        scene_config.control_gamepad["A"] = e.button
                    case 1:
                        scene_config.control_gamepad["B"] = e.button
                    case 2:
                        scene_config.control_gamepad["X"] = e.button
                    case 3:
                        scene_config.control_gamepad["Y"] = e.button
                    case 4:
                        scene_config.control_gamepad["L_BUMPER"] = e.button
                    case 5:
                        scene_config.control_gamepad["R_BUMPER"] = e.button
                    case 6:
                        scene_config.control_gamepad["START"] = e.button
                        config.check_gamepad(input_sys)
                        running = False
                        save_calibrate()
                        back_scene()
                        return 0
                select_step += 1
                time_tap = 0
            elif time_tap < 1:
                time_tap += 1
        
        sc.fill((0, 0, 0))
        sc.blit(txt_press, txt_press.get_rect(center=(WINDOW_SIZE[0]/2, 75)))
        sc.blit(input_button[select_step], input_button[select_step].get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2-10)))
        txt_input = pygame.font.Font(standart_font, 28).render(input_txt[select_step], scene_config.AA_TEXT, (255, 255, 255))
        sc.blit(txt_input, txt_input.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2+100)))
        sc.blit(txt_warning, txt_warning.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]-30)))
        pygame.display.update()
        clock.tick(FPS)
