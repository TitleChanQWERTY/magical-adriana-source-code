import pygame
from group_conf import particle_group

img_splash = {"KILL": pygame.image.load("Assets/sprite/ui/cartoon_splash/1.png").convert_alpha()}


class Splash(pygame.sprite.Sprite):
    def __init__(self, txt, pos):
        super().__init__()
        self.txt = txt
        self.pos = pos
        self.image = img_splash[txt]
        self.rect = self.image.get_rect(center=pos)


        self.add(particle_group)

        self.size = [25 * 8, 23 * 8]

        self.time_live = 0

        self.alpha = 301

    def update(self):
        
        if self.time_live >= 79:
            self.kill()
        elif self.time_live >= 65:
            self.size[0] += 23
            self.size[1] += 21
            self.alpha -= 27
        else:
            self.size[0] -= 19
            self.size[1] -= 17
            self.size[0] = max(22*3, min(25*8, self.size[0]))
            self.size[1] = max(20*3, min(23*8, self.size[1]))
        self.time_live += 1

        self.image = img_splash[self.txt]
        self.image = pygame.transform.scale(self.image, self.size)
        self.image.set_alpha(self.alpha)
        self.rect = self.image.get_rect(center=self.pos)
