import json
import os

import pygame
import config
import language_system
from animator import Animator

import gamepad_calibrate

from scene import scene_config, scene_option


def start():
    running = True
    FPS: int = 60
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    time_end = 0

    txt_load_progres = "LOADING..."

    isError: bool = False

    color_txt = [255, 255, 255]

    logo_sprite = (pygame.image.load("Assets/sprite/ui/logo/anim/1.png").convert_alpha(),
                   pygame.image.load("Assets/sprite/ui/logo/anim/2.png").convert_alpha(),
                   pygame.image.load("Assets/sprite/ui/logo/anim/3.png").convert_alpha())

    anim_logo = Animator(7, logo_sprite)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                pygame.quit()
        config.sc.fill((15, 5, 15))
        pygame.draw.rect(config.sc, (255, 15, 255), (0, 0, 50 * 17, 18 * 17), 5)
        image_logo = anim_logo.update()
        image_logo = pygame.transform.scale(image_logo, (50 * 10, 18 * 10))
        config.sc.blit(image_logo, image_logo.get_rect(center=(50 * 17 / 2, 18 * 17 / 2 - 38)))
        txt_load = pygame.font.Font(config.standart_font, 25).render(txt_load_progres, False, color_txt)
        config.sc.blit(txt_load, txt_load.get_rect(center=(50 * 17 / 2, 18 * 17 / 2 + 111)))
        if not isError:
            if time_end == 30:
                txt_load_progres = "LOAD CONFIG..."
                try:
                    with open("config.json", encoding='utf-8') as lang:
                        config.config_file = json.load(lang)
                    if pygame.joystick.get_count() > 0:
                        gamepad_calibrate.input_sys = config.config_file["input_sys"]
                        config.check_gamepad(gamepad_calibrate.input_sys)
                except FileNotFoundError:
                    pass
                try: 
                    with open("gamepad.json", encoding='utf-8') as gamepad:
                        game = json.load(gamepad)
                        scene_config.control_gamepad = game
                except FileNotFoundError:
                    pass
            elif time_end == 30:
                try:
                    scene_option.isFullscreen = config.config_file["FULLSCREEN"]
                except TypeError:
                    scene_option.isFullscreen = False
            elif time_end == 45:
                txt_load_progres = "LOAD AUDIO..."
                try:
                    scene_config.sfx_volume = config.config_file["sfx_volume"]
                    scene_config.music_volume = config.config_file["music_volume"]
                except TypeError:
                    scene_config.sfx_volume = 0.30
                    scene_config.music_volume = 0.20
            elif time_end == 65:
                txt_load_progres = "LOAD SPRITE..."
                try:
                    scene_config.blood_sprite = (
                        pygame.image.load("Assets/sprite/particle/blood/1.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/particle/blood/2.png").convert_alpha())
                    scene_config.enemy_die_effect_one = (
                        pygame.image.load("Assets/sprite/effect/enemy_die/1/1.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/1/2.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/1/3.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/1/4.png").convert_alpha())
                    scene_config.simple_particle_sprite = (
                        pygame.image.load("Assets/sprite/particle/particle/1.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/particle/particle/2.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/particle/particle/3.png").convert_alpha())
                    scene_config.gore_sprite = (pygame.image.load("Assets/sprite/particle/gore/1.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/particle/gore/2.png").convert_alpha(),
                                                pygame.image.load("Assets/sprite/particle/gore/3.png").convert_alpha())
                    scene_config.aim_image = [pygame.image.load("Assets/sprite/ui/aim_icon/1.png").convert_alpha(),
                                              pygame.image.load("Assets/sprite/ui/aim_icon/2.png").convert_alpha(),
                                              pygame.image.load("Assets/sprite/ui/aim_icon/3.png").convert_alpha(),
                                              pygame.image.load("Assets/sprite/ui/aim_icon/4.png").convert_alpha()]
                    for i in range(4):
                        scene_config.aim_image[i] = pygame.transform.scale(scene_config.aim_image[i], (6 * 2, 6 * 2))
                except FileNotFoundError:
                    isError = True
                try:
                    scene_config.select_aim = config.config_file["select_aim"]
                except TypeError:
                    scene_config.select_aim = 0
                    scene_config.AA_TEXT = False
            elif time_end == 80:
                txt_load_progres = "LOAD TXT..."
                try:
                    language_system.select_lang = config.config_file["LANGUAGE"]
                    scene_config.AA_TEXT = config.config_file["AA_TEXT"]
                except TypeError:
                    scene_config.AA_TEXT = False
                    language_system.select_lang = 0
                language_system.set_language()
            elif time_end == 90:
                txt_load_progres = "FINAL LOAD..."
            elif time_end >= 120:
                running = False
                try:
                    if config.config_file["FULLSCREEN"]:
                        scene_option.isFullscreen = True
                        config.sc = pygame.display.set_mode(config.WINDOW_SIZE,
                                                            pygame.FULLSCREEN | pygame.SCALED | pygame.HWSURFACE | pygame.DOUBLEBUF)
                    else:
                        scene_option.isFullscreen = False
                        config.sc = pygame.display.set_mode(config.WINDOW_SIZE, pygame.DOUBLEBUF | pygame.HWSURFACE)
                except TypeError:
                    scene_option.isFullscreen = False
                    config.sc = pygame.display.set_mode(config.WINDOW_SIZE, pygame.HWSURFACE)   
                pygame.mouse.set_visible(False)
                os.environ['SDL_VIDEO_CENTERED'] = '1'
        else:
            color_txt[0] = 255
            color_txt[1] = 0
            color_txt[2] = 0
            txt_load_progres = "ERROR LOADING!"
            if time_end >= 150:
                running = False
                pygame.quit()
        time_end += 1
        pygame.display.update()
        config.clock.tick(FPS)
