from pygame import mixer
from scene import scene_config

enemy_take_damage = mixer.Sound("Assets/sfx/ENEMY_TAKE_DAMAGE.ogg")

enemy_first_shoot = mixer.Sound("Assets/sfx/ENEMY_SHOOT_FIRST.ogg")
enemy_eye_shoot_laser_1 = mixer.Sound("Assets/sfx/EYE_SHOOT_LAZER_1.ogg")
enemy_eye_shoot_laser_2 = mixer.Sound("Assets/sfx/EYE_SHOOT_LAZER_2.ogg")
enemy_spider_shoot = mixer.Sound("Assets/sfx/SPIDER_SHOOT.ogg")

player_take_damage = (mixer.Sound("Assets/sfx/PLAYER_TAKE_DAMAGE_SECOND.ogg"),
                      mixer.Sound("Assets/sfx/PLAYER_TAKE_DAMAGE.ogg"),
                      mixer.Sound("Assets/sfx/PLAYER_TAKE_DAMAGE_THREE.ogg"))

player_switch_gun = mixer.Sound("Assets/sfx/SWITCH_GUN.ogg")

player_standart_shoot = mixer.Sound("Assets/sfx/PLAYER_STANDART_SHOOT.ogg")
player_pistol_shoot = mixer.Sound("Assets/sfx/PISTOL_PLAYER_SHOOT.ogg")
player_electro_shoot = mixer.Sound("Assets/sfx/PLAYER_ELECTRO_SHOOT.ogg")
player_minigun_shoot = mixer.Sound("Assets/sfx/PLAYER_MINIGUN_SHOOT.ogg")
player_shotgun_shoot = mixer.Sound("Assets/sfx/PLAYER_SHOTGUN_SHOOT.ogg")

cell_weapon = mixer.Sound("Assets/sfx/CELL_WEAPON.ogg")

switch_select_menu = mixer.Sound("Assets/sfx/SWITCH_SELECT_MENU.ogg")

player_take_weapon = mixer.Sound("Assets/sfx/PLAYER_TAKE_WEAPON.ogg")

player_type_txt = mixer.Sound("Assets/sfx/PLAYER_TYPE_TXT.ogg")
player_create_character = mixer.Sound("Assets/sfx/PLAYER_CREATE_CHARACTER.ogg")
player_remove_txt = mixer.Sound("Assets/sfx/PLAYER_REMOVE_TXT.ogg")
player_capslock_txt = mixer.Sound("Assets/sfx/PLAYER_TXT_CAPSLOCK.ogg")
player_capslock_txt_off = mixer.Sound("Assets/sfx/PLAYER_TXT_CAPSLOCK_OFF.ogg")

start_game_menu = mixer.Sound("Assets/sfx/START_GAME_MENU.ogg")

sound_play_mode = mixer.Sound("Assets/sfx/SOUND_PLAY_MODE.ogg")

game_type_txt = mixer.Sound("Assets/sfx/GAME_TYPE_TXT.ogg")

switch_gun_menu = mixer.Sound("Assets/sfx/SWITCH_GUN_MENU.ogg")

boss_die_mecha = mixer.Sound("Assets/sfx/BOSS_DIE_MECHA.ogg")

write_txt_dialog = mixer.Sound("Assets/sfx/WRITE_TXT_DIALOG.ogg")

player_start_lvl = mixer.Sound("Assets/sfx/PLAYER_START_LVL.ogg")

boss_down_fall = mixer.Sound("Assets/sfx/BOSS_DOWN_FALL.ogg")
boss_mecha_walk = mixer.Sound("Assets/sfx/BOSS_MECHA_WALK.ogg")

max_magical_voice = mixer.Sound("Assets/sfx/max_magical.ogg")
chanel_voice = mixer.Channel(5)

chanel_player_shoot = mixer.Channel(1)
chanel_enemy_shoot = mixer.Channel(2)
chanel_enemy_shoot_2 = mixer.Channel(3)


def stop_sound():
    boss_down_fall.stop()
    enemy_spider_shoot.stop()
    player_standart_shoot.stop()
    enemy_eye_shoot_laser_1.stop()
    enemy_eye_shoot_laser_2.stop()
    cell_weapon.stop()
    player_switch_gun.stop()
    switch_select_menu.stop()
    enemy_first_shoot.stop()
    player_shotgun_shoot.stop()
    player_minigun_shoot.stop()
    player_pistol_shoot.stop()
    enemy_take_damage.stop()
    for i in range(len(player_take_damage) - 1):
        player_take_damage[i].stop()
    player_electro_shoot.stop()


def set_volume():
    player_start_lvl.set_volume(scene_config.sfx_volume)
    max_magical_voice.set_volume(scene_config.sfx_volume)
    boss_mecha_walk.set_volume(scene_config.sfx_volume)
    boss_down_fall.set_volume(scene_config.sfx_volume)
    boss_die_mecha.set_volume(scene_config.sfx_volume)
    player_take_weapon.set_volume(scene_config.sfx_volume)
    write_txt_dialog.set_volume(scene_config.sfx_volume)
    switch_gun_menu.set_volume(scene_config.sfx_volume)
    game_type_txt.set_volume(scene_config.sfx_volume)
    sound_play_mode.set_volume(scene_config.sfx_volume)
    player_create_character.set_volume(scene_config.sfx_volume)
    player_capslock_txt_off.set_volume(scene_config.sfx_volume)
    player_capslock_txt.set_volume(scene_config.sfx_volume)
    start_game_menu.set_volume(scene_config.sfx_volume)
    player_remove_txt.set_volume(scene_config.sfx_volume)
    player_type_txt.set_volume(scene_config.sfx_volume)
    enemy_spider_shoot.set_volume(scene_config.sfx_volume)
    player_standart_shoot.set_volume(scene_config.sfx_volume)
    enemy_eye_shoot_laser_1.set_volume(scene_config.sfx_volume)
    enemy_eye_shoot_laser_2.set_volume(scene_config.sfx_volume)
    cell_weapon.set_volume(scene_config.sfx_volume)
    player_switch_gun.set_volume(scene_config.sfx_volume)
    switch_select_menu.set_volume(scene_config.sfx_volume)
    enemy_first_shoot.set_volume(scene_config.sfx_volume)
    player_shotgun_shoot.set_volume(scene_config.sfx_volume)
    player_minigun_shoot.set_volume(scene_config.sfx_volume)
    player_pistol_shoot.set_volume(scene_config.sfx_volume)
    enemy_take_damage.set_volume(scene_config.sfx_volume)
    for i, s in enumerate(player_take_damage):
        player_take_damage[i].set_volume(scene_config.sfx_volume)
    player_electro_shoot.set_volume(scene_config.sfx_volume)
