import pygame
from group_conf import notification_group
from scene import scene_config
from config import sc, standart_font, rank_image

notf_frame = pygame.image.load("Assets/sprite/ui/notf_frame.png").convert_alpha()

notf_icon = [pygame.image.load("Assets/sprite/ui/notf_icon/screenshoot.png").convert_alpha(),
             pygame.image.load("Assets/sprite/ui/notf_icon/warning.png").convert_alpha(),
             pygame.image.load("Assets/sprite/item/specifyc_item/paper.png").convert_alpha()]

notf_icon[0] = pygame.transform.scale(notf_icon[0], (10 * 3, 10 * 3))
notf_icon[1] = pygame.transform.scale(notf_icon[1], (9 * 5, 9 * 5))
notf_icon[2] = pygame.transform.scale(notf_icon[2], (11 * 3, 12 * 3))


def draw():
    notification_group.draw(sc)
    notification_group.update()


class Notification(pygame.sprite.Sprite):
    def __init__(self, txt_notf, type_notf=0):
        super().__init__()
        self.image = notf_frame
        self.image = pygame.transform.scale(self.image, (19 * 10 + 65, 9 * 8 + 1))
        self.rect = self.image.get_rect(center=(1350, 45))

        try:
            self.index = scene_config.yPosNotf.index(len(scene_config.yPosNotf) - 1)
        except ValueError:
            self.index = 0

        for i in scene_config.yPosNotf:
            if self.rect.y >= i:
                self.rect.y = i + 85
        scene_config.yPosNotf.append(self.rect.y)

        self.add(notification_group)

        self.time_hide: int = 0

        self.type_notf = type_notf

        self.txt_notf = pygame.font.Font(standart_font, 32 - len(txt_notf) - 1).render(
            txt_notf, scene_config.AA_TEXT, (255, 255, 255))

        if type_notf == 2:
            self.notf_icon = rank_image[scene_config.p.RANK]
        else:
            try:
                self.notf_icon = notf_icon[type_notf]
            except IndexError:
                self.notf_icon = notf_icon[type_notf-1]

    def update(self):
        sc.blit(self.notf_icon, self.notf_icon.get_rect(center=(self.rect.centerx - 95, self.rect.centery)))
        sc.blit(self.txt_notf, self.txt_notf.get_rect(center=(self.rect.centerx + 30, self.rect.centery)))

        pygame.draw.rect(sc, (255, 255, 255), (self.rect.centerx - 65, self.rect.centery - 29, 1, 60))

        if self.time_hide > 185:
            if self.rect.x < 1220:
                self.rect.x += 9
            else:
                scene_config.yPosNotf.remove(self.rect.y)
                self.kill()
        else:
            if self.rect.x > 940:
                self.rect.x -= 6
        self.time_hide += 1
