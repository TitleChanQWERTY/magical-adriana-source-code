import pygame
from config import sc, clock, WINDOW_SIZE, standart_font, check_gamepad, txt_ok, key_image
from group_conf import fill_screen, particle_group
from scene import scene_config, scene_music_mode, scene_select_mode
from screenshoot import draw_notification, TakeScreenshot
from sfx_colection import switch_select_menu
import language_system
import os
import gamepad_calibrate


def scene():
    running = True
    FPS = 60

    if not os.path.exists("Music"):
        os.mkdir("Music")

    
    music_list = os.listdir("Music")


    txt_music = []

    for m_list in music_list:
        txt_b = pygame.font.Font(standart_font, 19).render(str(m_list), scene_config.AA_TEXT, (200, 200, 200))
        txt_music.append(txt_b)

    
    yPosTXTmusic = 145

    rect_music = []

    for m_txt_list in txt_music:
        rect = m_txt_list.get_rect(center=(WINDOW_SIZE[0]/2, yPosTXTmusic))
        rect_music.append(rect)
        yPosTXTmusic += 51

    select_max = len(music_list) - 1
    
    select = 0
    
    if len(rect_music) >= 1:
        yPosSelect = yPosSelect = rect_music[0].bottom + 5

    select_music_txt = pygame.font.Font(standart_font, 30).render(language_system.txt_lang["SELECT_MUSIC"]+" ->", scene_config.AA_TEXT, (255, 255, 255))

    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["BACK"],
                                                          scene_config.AA_TEXT,
                                                          (255, 95, 255))

    txt_exit_rect = txt_exit.get_rect(center=(90, 667))
    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.JOYHATMOTION:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.value == (0, -1) and select < select_max:
                    switch_select_menu.play()
                    select += 1

                    for i, b in enumerate(rect_music):
                        if select >= 6:  
                            rect_music[i].y -= 25

                    yPosSelect = rect_music[select].bottom + 5
                elif e.value == (0, 1) and select > 0:
                    switch_select_menu.play()
                    select -= 1

                    for i, b in enumerate(rect_music):
                        if select >= 5:  
                            rect_music[i].y += 25

                    yPosSelect = rect_music[select].bottom + 5
            elif e.type == pygame.JOYBUTTONDOWN:
                if e.button == scene_config.control_gamepad["B"]:
                    running = False
                    scene_select_mode.scene()
                    return
                elif e.button == scene_config.control_gamepad["A"] and len(txt_music) >= 1:
                    pygame.mixer.music.load("Music/"+music_list[select])
                    pygame.mixer.music.play(-1)
                    pygame.mixer.music.set_volume(scene_config.music_volume)
                    running = False
                    scene_music_mode.select_music = "Music/"+music_list[select]
                    scene_config.select_mode = 2
                    scene_select_mode.scene_difficulty()
                    return
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                elif e.key == pygame.K_ESCAPE:
                    running = False
                    scene_select_mode.scene()
                    return
                elif e.key == pygame.K_DOWN and select < select_max:
                    switch_select_menu.play()
                    select += 1

                    for i, b in enumerate(rect_music):
                        if select >= 5:  
                            rect_music[i].y -= 25

                    yPosSelect = rect_music[select].bottom + 5
                
                elif e.key == pygame.K_UP and select > 0:
                    switch_select_menu.play()
                    select -= 1

                    for i, b in enumerate(rect_music):
                        if select >= 4:  
                            rect_music[i].y += 25

                    yPosSelect = rect_music[select].bottom + 5
                elif e.key == pygame.K_RETURN and len(txt_music) >= 1:
                    pygame.mixer.music.load("Music/"+music_list[select])
                    pygame.mixer.music.play(-1)
                    pygame.mixer.music.set_volume(scene_config.music_volume)
                    running = False
                    scene_music_mode.select_music = "Music/"+music_list[select]
                    scene_config.select_mode = 2
                    scene_select_mode.scene_difficulty()
                    return


        sc.fill((9, 3, 9))
        particle_group.draw(sc)
        particle_group.update()
        if len(rect_music) >= 1:
            rect_select = pygame.Rect(WINDOW_SIZE[0]/4, yPosSelect, 245, 4)

            pygame.draw.rect(sc, (230, 0, 185), (rect_select.centerx, rect_select.y+2, 370, 4))

            sc.blit(key_image["enter"], key_image["enter"].get_rect(center=(WINDOW_SIZE[0] - 110, 665)))
            sc.blit(txt_ok, txt_ok.get_rect(center=(WINDOW_SIZE[0] - 175, 668)))
        else:
            txt_tuttorial = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["COPY_MUSIC_TUTTORIAL"], scene_config.AA_TEXT, (255, 255, 255))
            sc.blit(txt_tuttorial, txt_tuttorial.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2)))

        for i, b in enumerate(txt_music):
            sc.blit(txt_music[i], rect_music[i])

        pygame.draw.rect(sc, (9, 3, 9), (0, 0, WINDOW_SIZE[0], 95))
        sc.blit(select_music_txt, select_music_txt.get_rect(center=(WINDOW_SIZE[0]/2, 45)))
        pygame.draw.rect(sc, (255, 0, 255), (0, 95, WINDOW_SIZE[0], 2))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
