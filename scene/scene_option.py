import os

import pygame
import config
import language_system
import gamepad_calibrate
from group_conf import particle_group
from scene import scene_config, scene_menu
from screenshoot import TakeScreenshot, draw_notification
from sfx_colection import set_volume, switch_select_menu
from save_progres import save_config

isFullscreen = None


def scene():
    global isFullscreen

    LANG = os.listdir("Language (Dont Remove)")

    running = True
    FPS: int = 60

    txt_exit = pygame.font.Font(config.standart_font, 20).render(language_system.txt_lang["MAIN_MENU"],
                                                                 scene_config.AA_TEXT,
                                                                 (255, 95, 255))
    txt_exit_rect = txt_exit.get_rect(center=(103, 667))

    txt_exit_rect.left = config.key_image["esc"].get_rect(center=(39, 665)).right+8

    txt_fullscreen = pygame.font.Font(config.standart_font, 26).render(
        language_system.txt_lang["FULLSCREEN"] + ": " + str(isFullscreen),
        scene_config.AA_TEXT, (255, 255, 255))
    if not isFullscreen:
        txt_fullscreen.set_alpha(50)
    txt_language = pygame.font.Font(config.standart_font, 26).render(
        language_system.txt_lang["LANGUAGE_NAME"],
        scene_config.AA_TEXT, (255, 255, 255))

    txt_aa = pygame.font.Font(config.standart_font, 26).render(
        language_system.txt_lang["AA_FONT"] + ": " + str(scene_config.AA_TEXT),
        scene_config.AA_TEXT, (255, 255, 255))
    if not scene_config.AA_TEXT:
        txt_aa.set_alpha(50)

    txt_volume_sfx = pygame.font.Font(config.standart_font, 26).render(
        language_system.txt_lang["SFX_VOLUME"] + ": %.2f" % scene_config.sfx_volume,
        scene_config.AA_TEXT, (255, 255, 255))

    txt_volume_music = pygame.font.Font(config.standart_font, 26).render(
        language_system.txt_lang["MUSIC_VOLUME"] + ": %.2f" % scene_config.music_volume,
        scene_config.AA_TEXT, (255, 255, 255))

    txt_aim = pygame.font.Font(config.standart_font, 26).render(language_system.txt_lang["SELECT_AIM"] + ": ",
                                                                scene_config.AA_TEXT, (255, 255, 255))

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (64 * 5, 16 * 5))
    yPosSelector = 200

    select = 0


    txt_gamepad = pygame.font.Font(config.standart_font, 26).render(language_system.txt_lang["SET_GAMEPAD"], scene_config.AA_TEXT, (255, 255, 255))

    rect_left = config.key_image["arrow_left"].get_rect(center=(config.WINDOW_SIZE[0]/2, 199))
    rect_right = config.key_image["arrow_right"].get_rect(center=(config.WINDOW_SIZE[0]/2, 199))

    rect_left.right = txt_language.get_rect(center=(config.WINDOW_SIZE[0] / 2, 200)).left-10
    rect_right.left = txt_language.get_rect(center=(config.WINDOW_SIZE[0] / 2, 200)).right+10

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYHATMOTION:
                config.check_gamepad(gamepad_calibrate.input_sys)
                if e.value == (-1, 0):
                    if select == 1:
                        language_system.select_lang -= 1
                        language_system.select_lang %= len(LANG)
                        language_system.set_language()
                        running = False
                        scene()
                        return 0
                    if select == 3:
                        switch_select_menu.play()
                        scene_config.sfx_volume -= 0.1
                        scene_config.sfx_volume = max(0, min(1, scene_config.sfx_volume))
                        txt_volume_sfx = pygame.font.Font(config.standart_font, 26).render(
                            language_system.txt_lang["SFX_VOLUME"] + ": %.2f" % scene_config.sfx_volume,
                            scene_config.AA_TEXT, (255, 255, 255))
                        set_volume()

                    if select == 4:
                        scene_config.music_volume -= 0.1
                        scene_config.music_volume = max(0, min(1, scene_config.music_volume))
                        txt_volume_music = pygame.font.Font(config.standart_font, 26).render(
                            language_system.txt_lang["MUSIC_VOLUME"] + ": %.2f" % scene_config.music_volume,
                            scene_config.AA_TEXT, (255, 255, 255))
                        pygame.mixer.music.set_volume(scene_config.music_volume)
                    if select == 5:
                        scene_config.select_aim -= 1
                        scene_config.select_aim %= len(scene_config.aim_image)
                if e.value == (1, 0):
                    if select == 1:
                        language_system.select_lang += 1
                        language_system.select_lang %= len(LANG)
                        language_system.set_language()
                        running = False
                        scene()
                        return 0
                    if select == 3:
                        switch_select_menu.play()
                        scene_config.sfx_volume += 0.1
                        scene_config.sfx_volume = max(0, min(1, scene_config.sfx_volume))
                        txt_volume_sfx = pygame.font.Font(config.standart_font, 26).render(
                            language_system.txt_lang["SFX_VOLUME"] + ": %.2f" % scene_config.sfx_volume,
                            scene_config.AA_TEXT, (255, 255, 255))
                        set_volume()

                    if select == 4:
                        scene_config.music_volume += 0.1
                        scene_config.music_volume = max(0, min(1, scene_config.music_volume))
                        txt_volume_music = pygame.font.Font(config.standart_font, 26).render(
                            language_system.txt_lang["MUSIC_VOLUME"] + ": %.2f" % scene_config.music_volume,
                            scene_config.AA_TEXT, (255, 255, 255))
                        pygame.mixer.music.set_volume(scene_config.music_volume)
                    if select == 5:
                        scene_config.select_aim += 1
                        scene_config.select_aim %= len(scene_config.aim_image)
                if e.value == (0, -1):
                    switch_select_menu.play()
                    select += 1
                elif e.value == (0, 1):
                    switch_select_menu.play()
                    select -= 1
                select %= 7
            elif e.type == pygame.JOYBUTTONDOWN:
                config.check_gamepad(gamepad_calibrate.input_sys)
                if e.button == scene_config.control_gamepad["A"]:
                    match select:
                        case 0:
                            if not isFullscreen:
                                config.sc = pygame.display.set_mode(config.WINDOW_SIZE,
                                                                    pygame.FULLSCREEN | pygame.SCALED | pygame.HWSURFACE | pygame.DOUBLEBUF)
                                isFullscreen = True
                            else:
                                config.sc = pygame.display.set_mode(config.WINDOW_SIZE,
                                                                    pygame.SCALED | pygame.DOUBLEBUF)
                                isFullscreen = False
                            os.environ['SDL_VIDEO_CENTERED'] = '1'
                            running = False
                            scene()
                            return 0
                        case 2:
                            scene_config.AA_TEXT = not scene_config.AA_TEXT
                            running = False
                            scene()
                            return 0
                        case 6:
                            running = False
                            gamepad_calibrate.select_input(scene)
                            return
                if e.button == scene_config.control_gamepad["B"]:
                    save_config(isFullscreen)
                    running = False
                    scene_menu.scene()
                    return 0
            if e.type == pygame.KEYDOWN:
                config.check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                elif e.key == pygame.K_ESCAPE or e.key == pygame.K_q:
                    save_config(isFullscreen)
                    running = False
                    scene_menu.scene()
                    return 0

                elif e.key == pygame.K_RETURN or e.key == pygame.K_e:
                    match select:
                        case 0:
                            if not isFullscreen:
                                config.sc = pygame.display.set_mode(config.WINDOW_SIZE,
                                                                    pygame.FULLSCREEN | pygame.SCALED | pygame.HWSURFACE)
                                isFullscreen = True
                            else:
                                config.sc = pygame.display.set_mode(config.WINDOW_SIZE,
                                                                    pygame.SCALED | pygame.HWSURFACE)
                                isFullscreen = False
                            os.environ['SDL_VIDEO_CENTERED'] = '1'
                            running = False
                            scene()
                            return 0
                        case 2:
                            scene_config.AA_TEXT = not scene_config.AA_TEXT
                            running = False
                            scene()
                            return 0
                        case 6:
                            running = False
                            gamepad_calibrate.select_input(scene)
                            return

                if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                    switch_select_menu.play()
                    select += 1
                if e.key == pygame.K_UP or e.key == pygame.K_w:
                    switch_select_menu.play()
                    select -= 1
                select %= 7

                if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                    if select == 1:
                        language_system.select_lang -= 1
                        language_system.select_lang %= len(LANG)
                        language_system.set_language()
                        running = False
                        scene()
                        return 0
                    elif select == 3:
                        switch_select_menu.play()
                        scene_config.sfx_volume -= 0.1
                        scene_config.sfx_volume = max(0, min(1, scene_config.sfx_volume))
                        txt_volume_sfx = pygame.font.Font(config.standart_font, 26).render(
                            language_system.txt_lang["SFX_VOLUME"] + ": %.2f" % scene_config.sfx_volume,
                            scene_config.AA_TEXT, (255, 255, 255))
                        set_volume()

                    elif select == 4:
                        scene_config.music_volume -= 0.1
                        scene_config.music_volume = max(0, min(1, scene_config.music_volume))
                        txt_volume_music = pygame.font.Font(config.standart_font, 26).render(
                            language_system.txt_lang["MUSIC_VOLUME"] + ": %.2f" % scene_config.music_volume,
                            scene_config.AA_TEXT, (255, 255, 255))
                        pygame.mixer.music.set_volume(scene_config.music_volume)
                    elif select == 5:
                        scene_config.select_aim -= 1
                        scene_config.select_aim %= len(scene_config.aim_image)

                if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                    if select == 1:
                        language_system.select_lang += 1
                        language_system.select_lang %= len(LANG)
                        language_system.set_language()
                        running = False
                        scene()
                        return 0
                    if select == 3:
                        switch_select_menu.play()
                        scene_config.sfx_volume += 0.1
                        scene_config.sfx_volume = max(0, min(1, scene_config.sfx_volume))
                        txt_volume_sfx = pygame.font.Font(config.standart_font, 26).render(
                            language_system.txt_lang["SFX_VOLUME"] + ": %.2f" % scene_config.sfx_volume,
                            scene_config.AA_TEXT, (255, 255, 255))
                        set_volume()

                    if select == 4:
                        scene_config.music_volume += 0.1
                        scene_config.music_volume = max(0, min(1, scene_config.music_volume))
                        txt_volume_music = pygame.font.Font(config.standart_font, 26).render(
                            language_system.txt_lang["MUSIC_VOLUME"] + ": %.2f" % scene_config.music_volume,
                            scene_config.AA_TEXT, (255, 255, 255))
                        pygame.mixer.music.set_volume(scene_config.music_volume)
                    if select == 5:
                        scene_config.select_aim += 1
                        scene_config.select_aim %= len(scene_config.aim_image)

        config.sc.fill((9, 3, 9))
        particle_group.draw(config.sc)
        particle_group.update()

        match select:
            case 0:
                yPosSelector = 149
            case 1:
                yPosSelector = 199
                rect_left = config.key_image["arrow_left"].get_rect(center=(config.WINDOW_SIZE[0]/2, 198))
                rect_right = config.key_image["arrow_right"].get_rect(center=(config.WINDOW_SIZE[0]/2, 198))
                selector_img = pygame.transform.scale(selector_img,
                                                      (64 * 5, 16 * 5))
                rect_left.right = txt_language.get_rect(center=(config.WINDOW_SIZE[0] / 2, 200)).left-10
                rect_right.left = txt_language.get_rect(center=(config.WINDOW_SIZE[0] / 2, 200)).right+10
                config.sc.blit(config.key_image["arrow_left"], rect_left)
                config.sc.blit(config.key_image["arrow_right"], rect_right)
            case 2:
                yPosSelector = 249
                selector_img = pygame.transform.scale(selector_img,
                                                      (70 * 6 + len(language_system.txt_lang["AA_FONT"]) + 1, 16 * 5))
            case 3:
                yPosSelector = 299
                selector_img = pygame.transform.scale(selector_img,
                                                      (65 * 6 + len(language_system.txt_lang["SFX_VOLUME"]) + 1, 16 * 5))
            case 4:
                yPosSelector = 349
                selector_img = pygame.transform.scale(selector_img,
                                                      (65 * 6 + len(language_system.txt_lang["MUSIC_VOLUME"]) + 1, 16 * 5))
            case 5:
                selector_img = pygame.transform.scale(selector_img,
                                                      (64 * 5, 16 * 5))
                yPosSelector = 399
                rect_left = config.key_image["arrow_left"].get_rect(center=(config.WINDOW_SIZE[0]/2, 398))
                rect_right = config.key_image["arrow_right"].get_rect(center=(config.WINDOW_SIZE[0]/2, 398))
                rect_left.right = txt_aim.get_rect(center=(config.WINDOW_SIZE[0] / 2, 400)).left-11
                rect_right.left = txt_aim.get_rect(center=(config.WINDOW_SIZE[0] / 2, 400)).right+24
                config.sc.blit(config.key_image["arrow_left"], rect_left)
                config.sc.blit(config.key_image["arrow_right"], rect_right)
            case 6:
                yPosSelector = 449
                selector_img = pygame.transform.scale(selector_img,
                                                      (63 * 6 + len(language_system.txt_lang["SET_GAMEPAD"]) + 1, 16 * 5))

        config.sc.blit(txt_fullscreen, txt_fullscreen.get_rect(center=(config.WINDOW_SIZE[0] / 2, 148)))
        config.sc.blit(txt_language, txt_language.get_rect(center=(config.WINDOW_SIZE[0] / 2, 200)))
        config.sc.blit(txt_aa, txt_aa.get_rect(center=(config.WINDOW_SIZE[0] / 2, 250)))
        config.sc.blit(txt_volume_sfx, txt_volume_sfx.get_rect(center=(config.WINDOW_SIZE[0] / 2, 300)))
        config.sc.blit(txt_volume_music, txt_volume_music.get_rect(center=(config.WINDOW_SIZE[0] / 2, 350)))

        config.sc.blit(txt_aim, txt_aim.get_rect(center=(config.WINDOW_SIZE[0] / 2, 398)))
        aim_rect = scene_config.aim_image[scene_config.select_aim].get_rect(center=(config.WINDOW_SIZE[0] / 2 + 55, 398))
        aim_rect.left = txt_aim.get_rect(center=(config.WINDOW_SIZE[0] / 2, 400)).right + 1
        config.sc.blit(scene_config.aim_image[scene_config.select_aim], aim_rect)
        config.sc.blit(txt_gamepad, txt_gamepad.get_rect(center=(config.WINDOW_SIZE[0] / 2, 450)))

        config.sc.blit(config.key_image["enter"],
                       config.key_image["enter"].get_rect(center=(config.WINDOW_SIZE[0] - 110, 665)))
        config.sc.blit(config.txt_ok, config.txt_ok.get_rect(center=(config.WINDOW_SIZE[0] - 175, 668)))

        config.sc.blit(config.key_image["esc"], config.key_image["esc"].get_rect(center=(39, 665)))
        config.sc.blit(txt_exit, txt_exit_rect)

        config.sc.blit(selector_img, selector_img.get_rect(center=(config.WINDOW_SIZE[0] / 2, yPosSelector)))

        draw_notification()

        pygame.display.update()
        config.clock.tick(FPS)
