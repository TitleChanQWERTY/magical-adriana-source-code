from random import randint

import pygame

from enemy import protator, spider, eye, drone_container, chain
from boss import lancetus
from player import create_player
from group_conf import enemy_group
from boss_splash import BossSplash

screen_shake: int = 0
power_shake: int = 0

aim_image = []

select_aim = 0

AA_TEXT = None

p = create_player.createPlayer()

blood_sprite = None

simple_particle_sprite = None

gore_sprite = None

enemy_die_effect_one = None

enemy_die_effect_two = (pygame.image.load("Assets/sprite/effect/enemy_die/2/1.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/2.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/3.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/4.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/5.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/1.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/2.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/3.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/4.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/enemy_die/2/5.png").convert_alpha())

laser_shoot_effect = (pygame.image.load("Assets/sprite/effect/laser_shoot/1.png").convert_alpha(),
                      pygame.image.load("Assets/sprite/effect/laser_shoot/2.png").convert_alpha(),
                      pygame.image.load("Assets/sprite/effect/laser_shoot/3.png").convert_alpha(),
                      pygame.image.load("Assets/sprite/effect/laser_shoot/4.png").convert_alpha())

menu_particle_image = [pygame.image.load("Assets/sprite/effect/main_menu/1.png").convert_alpha(),
                       pygame.image.load("Assets/sprite/effect/main_menu/2.png").convert_alpha(),
                       pygame.image.load("Assets/sprite/effect/main_menu/3.png").convert_alpha(),
                       pygame.image.load("Assets/sprite/effect/main_menu/4.png").convert_alpha()]

boss_die_effect = (pygame.image.load("Assets/sprite/effect/enemy_die/boss/1.png").convert_alpha(),
                    pygame.image.load("Assets/sprite/effect/enemy_die/boss/2.png").convert_alpha(),
                    pygame.image.load("Assets/sprite/effect/enemy_die/boss/3.png").convert_alpha(),
                    pygame.image.load("Assets/sprite/effect/enemy_die/boss/4.png").convert_alpha(),
                    pygame.image.load("Assets/sprite/effect/enemy_die/boss/5.png").convert_alpha(),
                    pygame.image.load("Assets/sprite/effect/enemy_die/boss/6.png").convert_alpha(),
                    pygame.image.load("Assets/sprite/effect/enemy_die/boss/7.png").convert_alpha())

select_mode = 0

sfx_volume = 0
music_volume = 0


time_played = 0

timeCreateProtator: int = 0
timeCreateSpider: int = 0
timeCreateEye: int = 0
timeCreateDroneContainer: int = 0
timeCreateChain = 0

maxTimeCreateProtator = 155
maxTimeCreateSpider = 195
maxTimeCreateEye = 393
maxTimeCreateDroneContainer = 520
maxTimeCreateDroneChain = 125

difficulty = 0

enemy_bullet_damage = 3
timeDamageMax = 0

time_spawn_bos = 0
time_spawn_bos_max = randint(2000, 2955)
isBoss = False
isBoss2 = False
isBoss3 = False


def set_difficulty():
    global enemy_bullet_damage, timeDamageMax, maxTimeCreateDroneContainer, maxTimeCreateEye, maxTimeCreateSpider, maxTimeCreateProtator, maxTimeCreateDroneChain, time_spawn_bos_max
    time_spawn_bos_max = randint(2005, 2955)
    if difficulty == 0:
        p.max_time_take_damage = 265
        timeDamageMax = 90
        enemy_bullet_damage = 5
        maxTimeCreateProtator = 290
        maxTimeCreateSpider = 450
        maxTimeCreateEye = 781
        maxTimeCreateDroneContainer = 866
        maxTimeCreateDroneChain = 311
        return
    elif difficulty == 1:
        p.max_time_take_damage = 520
        timeDamageMax = 75
        enemy_bullet_damage = 7
        maxTimeCreateProtator = 214
        maxTimeCreateSpider = 249
        maxTimeCreateEye = 600
        maxTimeCreateDroneContainer = 649
        maxTimeCreateDroneChain = 285
        return
    elif difficulty == 2:
        p.max_time_take_damage = 765
        timeDamageMax = 59
        enemy_bullet_damage = 11
        maxTimeCreateProtator = 150
        maxTimeCreateSpider = 214
        maxTimeCreateEye = 325
        maxTimeCreateDroneContainer = 485
        maxTimeCreateDroneChain = 245
        return

def spawn_bos():
    global time_spawn_bos, isBoss, isBoss2, isBoss3
    if time_spawn_bos >= time_spawn_bos_max-87:
        isBoss = True
    if time_spawn_bos >= time_spawn_bos_max and len(enemy_group) <= 4:
        if not isBoss3:
            BossSplash()
            isBoss3 = True
    if time_spawn_bos >= time_spawn_bos_max and len(enemy_group) < 2:
        if not isBoss2:
            lancetus.Lancetus()
            isBoss2 = True
    else:
        time_spawn_bos += 1


def spawnEnemy():
    global timeCreateProtator, timeCreateSpider, timeCreateEye, timeCreateDroneContainer, timeCreateChain, time_spawn_bos, isBoss
    if not isBoss:
        if timeCreateProtator >= maxTimeCreateProtator:
            protator.Protator(randint(392, 800), -25)
            timeCreateProtator = 0
        elif timeCreateSpider >= maxTimeCreateSpider and p.BOSS_KILL_COUNT >= 1:
            spider.Spider(randint(373, 821), -35)
            timeCreateSpider = 0
        elif timeCreateEye >= maxTimeCreateEye:
            eye.Eye(randint(385, 830), randint(130, 455))
            timeCreateEye = 0
        elif timeCreateDroneContainer > maxTimeCreateDroneContainer:
            drone_container.DroneContainer(randint(375, 820), -30)
            timeCreateDroneContainer = 0
        elif timeCreateChain >= maxTimeCreateDroneChain and p.BOSS_KILL_COUNT >= 1:
            if difficulty >= 2:
                spawn = randint(1, 3)
            elif difficulty == 1:
                spawn = randint(1, 2)
            else:
                spawn = 1
            for i in range(spawn):
                chain.Chain()
            timeCreateChain = 0
        timeCreateProtator += 1
        timeCreateSpider += 1
        timeCreateEye += 1
        timeCreateDroneContainer += 1
        timeCreateChain += 1


yPosNotf = []


control_type = 0

control_keyboard = {'move_up': pygame.K_w,
                    'move_down': pygame.K_s,
                    'move_left': pygame.K_a,
                    'move_right': pygame.K_d,
                    'shoot': pygame.K_k,
                    'next_weapon': pygame.K_l,
                    'prev_weapon': pygame.K_j,
                    'shoot_bomb': pygame.K_i,
                    'dash': pygame.K_SPACE}


control_gamepad = {"A": 0,
                   "B": 1,
                   "X": 2,
                   "Y": 3,
                   "L_BUMPER": 4,
                   "R_BUMPER": 5,
                   "START": 7}
