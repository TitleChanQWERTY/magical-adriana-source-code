import pygame

import language_system
from config import sc, clock, WINDOW_SIZE, rank_image, standart_font, key_image, rank_max_exp, check_gamepad
import gamepad_calibrate
from scene import scene_menu, scene_config
from group_conf import particle_group
from progres_bar import ProgressBar
from screenshoot import draw_notification, TakeScreenshot


def scene():
    running: bool = True
    FPS: int = 60

    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["MAIN_MENU"],
                                                          scene_config.AA_TEXT,
                                                          (255, 95, 255))
    txt_exit_rect = txt_exit.get_rect(center=(103, 667))

    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    txt_rank = pygame.font.Font(standart_font, 19).render(language_system.txt_lang["RANK"]+":",
                                                          scene_config.AA_TEXT, (255, 0, 215))

    if scene_config.p.RANK < 6:
        bar_rank = ProgressBar(WINDOW_SIZE[0] / 2 - rank_max_exp[scene_config.p.RANK+1], 150,
                            (rank_max_exp[scene_config.p.RANK+1], 17), (255, 0, 215))
    
    txt_max = pygame.font.Font(standart_font, 25).render("MAX", scene_config.AA_TEXT, (255, 200, 255))

    txt_exp = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["EXP"] + ":",
                                                         scene_config.AA_TEXT, (255, 255, 255))

    txt_hiscore = pygame.font.Font(standart_font, 25).render(
        language_system.txt_lang["HISCORE"] + ": " + str(scene_config.p.HISCORE), scene_config.AA_TEXT, (255, 255, 0))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.button == scene_config.control_gamepad["B"]:
                    running = False
                    scene_menu.scene()
                    return 0
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                elif e.key == pygame.K_ESCAPE or e.key == pygame.K_q:
                    running = False
                    scene_menu.scene()
                    return 0
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1

        sc.fill((9, 3, 9))
        particle_group.draw(sc)
        particle_group.update()

        sc.blit(txt_rank, txt_rank.get_rect(center=(WINDOW_SIZE[0] / 2-70, 105)))

        if scene_config.p.RANK < 6:
            bar_rank.update(scene_config.p.EXP-1, sc)
            sc.blit(txt_exp, txt_exp.get_rect(center=(bar_rank.rect.centerx - 25, bar_rank.rect.centery + 8)))
        else:
            sc.blit(txt_max, txt_max.get_rect(center=(WINDOW_SIZE[0]/2-1, 162)))

        sc.blit(txt_hiscore, txt_hiscore.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 95)))

        txt_time_played = pygame.font.Font(standart_font, 24).render(
            language_system.txt_lang["TIME_PLAYED"] + ": " + str(scene_config.time_played) + "m", scene_config.AA_TEXT,
            (255, 255, 255))
        sc.blit(txt_time_played, txt_time_played.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 25)))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)

        sc.blit(rank_image[scene_config.p.RANK],
                rank_image[scene_config.p.RANK].get_rect(center=(WINDOW_SIZE[0] / 2, 100)))

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
