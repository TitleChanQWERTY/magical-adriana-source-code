import pygame

import language_system
from config import sc, clock, WINDOW_SIZE, rank_image, standart_font, key_image, rank_max_exp, check_gamepad
from scene import scene_menu, scene_config, scene_final_credits, scene_paper_view
import gamepad_calibrate
from group_conf import particle_group, fill_screen
from screen_fill import ScreenFill
from sfx_colection import switch_select_menu
from screenshoot import draw_notification, TakeScreenshot


def scene():
    running: bool = True
    FPS: int = 60

    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["MAIN_MENU"],
                                                          scene_config.AA_TEXT,
                                                          (255, 95, 255))
    txt_exit_rect = txt_exit.get_rect(center=(103, 667))

    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (64 * 5, 16 * 5))
    yPosSelector = 150

    txt_credits = pygame.font.Font(standart_font, 30).render(language_system.txt_lang["EXTRA_CREDITS"], scene_config.AA_TEXT, (255, 255, 255))
    txt_documents = pygame.font.Font(standart_font, 30).render(language_system.txt_lang["DOCUMENTS_MENU"], scene_config.AA_TEXT, (255, 255, 255))

    isEnd = False

    time_end = 0

    select = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if not isEnd:
                if e.type == pygame.JOYHATMOTION:
                    check_gamepad(gamepad_calibrate.input_sys)
                    if e.value == (0, -1):
                        switch_select_menu.play()
                        select += 1
                    elif e.value == (0, 1):
                        switch_select_menu.play()
                        select -= 1
                    select %= 2
                if e.type == pygame.JOYBUTTONDOWN:
                    check_gamepad(gamepad_calibrate.input_sys)
                    if e.button == scene_config.control_gamepad["A"]:
                        match select:
                            case 0:
                                pygame.mixer.music.stop()
                                isEnd = True
                                ScreenFill(1, 10, (0, 0, 0), 50)
                            case 1:
                                running = False
                                scene_paper_view.sceme()
                                return 0
                    elif e.button == scene_config.control_gamepad["B"]:
                        running = False
                        scene_menu.scene()
                        return 0
                elif e.type == pygame.KEYDOWN:
                    check_gamepad(gamepad_calibrate.input_sys, True)
                    if e.key == pygame.K_F6:
                        TakeScreenshot()
                    elif e.key == pygame.K_DOWN:
                        switch_select_menu.play()
                        select += 1
                    elif e.key == pygame.K_UP:
                        switch_select_menu.play()
                        select -= 1
                    elif e.key == pygame.K_RETURN:
                        match select:
                            case 0:
                                pygame.mixer.music.stop()
                                isEnd = True
                                ScreenFill(1, 10, (0, 0, 0), 50)
                            case 1:
                                running = False
                                scene_paper_view.sceme()
                                return 0
                    elif e.key == pygame.K_ESCAPE:
                        running = False
                        scene_menu.scene()
                        return 0
                    select %= 2
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1

        sc.fill((9, 3, 9))
        particle_group.draw(sc)
        particle_group.update()

        match select:
            case 0:
                yPosSelector = 150
            case 1:
                yPosSelector = 200

        sc.blit(selector_img, selector_img.get_rect(center=(WINDOW_SIZE[0]/2, yPosSelector)))

        sc.blit(txt_credits, txt_credits.get_rect(center=(WINDOW_SIZE[0]/2, 150)))
        sc.blit(txt_documents, txt_documents.get_rect(center=(WINDOW_SIZE[0]/2, 200)))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)

        if isEnd:
            if time_end >= 51:
                running = False
                scene_final_credits.scene_dialog()
                for i in fill_screen:
                    i.kill()
                return
            time_end += 1

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
