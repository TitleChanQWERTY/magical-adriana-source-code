from random import randint

import pygame
from config import sc, clock, WINDOW_SIZE, standart_font, key_image, txt_ok, rank_image, check_gamepad
from group_conf import particle_group
from player.weapon import Standart_weapon, MagicPistol, MagicMinigun, MagicShotgun, MagicTrident
from scene import scene_config
from animator import Animator
from screenshoot import draw_notification, TakeScreenshot
from sfx_colection import switch_select_menu, cell_weapon, switch_gun_menu
from particle import PhysicalParticle
from item.money import image_money
import language_system
import gamepad_calibrate

price_upgrade = (45, 35, 85)

scene_exit_w = None

def scene(scene_exit):
    global scene_exit_w
    running: bool = True
    FPS: int = 60

    scene_exit_w = scene_exit

    weapon_img = []

    for weapon in scene_config.p.weapon:
        if isinstance(weapon, Standart_weapon):
            img = pygame.image.load("Assets/sprite/item/weapon/standart.png").convert_alpha()
            weapon_img.append(pygame.transform.scale(img, (6 * 7, 8 * 7)))
        if isinstance(weapon, MagicPistol):
            img = pygame.image.load("Assets/sprite/item/weapon/pistiol.png").convert_alpha()
            img = pygame.transform.scale(img, (8 * 7, 7 * 7))
            weapon_img.append(img)
        if isinstance(weapon, MagicShotgun):
            img = pygame.image.load("Assets/sprite/item/weapon/shutgun.png").convert_alpha()
            img = pygame.transform.scale(img, (19 * 6, 8 * 6))
            weapon_img.append(img)
        if isinstance(weapon, MagicMinigun):
            img = pygame.image.load("Assets/sprite/item/weapon/minigun.png").convert_alpha()
            img = pygame.transform.scale(img, (35 * 4, 12 * 4))
            weapon_img.append(img)
        if isinstance(weapon, MagicTrident):
            img = pygame.image.load("Assets/sprite/item/weapon/electro_trident.png").convert_alpha()
            img = pygame.transform.scale(img, (12 * 5, 12 * 5))
            weapon_img.append(img)

    select: int = 0

    select_upgrade: int = 0

    selector_img = (pygame.image.load("Assets/sprite/ui/selector_rect_anim/1.png").convert_alpha(),
                    pygame.image.load("Assets/sprite/ui/selector_rect_anim/2.png").convert_alpha(),
                    pygame.image.load("Assets/sprite/ui/selector_rect_anim/3.png").convert_alpha())

    anim_selector = Animator(11, selector_img)

    AimImg = pygame.image.load("Assets/sprite/weapon_upgrade_set/1.png").convert_alpha()
    AimImg = pygame.transform.scale(AimImg, (11 * 5, 13 * 5))

    StabilizationImg = pygame.image.load("Assets/sprite/weapon_upgrade_set/2.png").convert_alpha()
    StabilizationImg = pygame.transform.scale(StabilizationImg, (8 * 6, 7 * 6))

    BombShootImg = pygame.image.load("Assets/sprite/weapon_upgrade_set/3.png").convert_alpha()
    BombShootImg = pygame.transform.scale(BombShootImg, (11 * 5, 11 * 5))

    yPosSelector = WINDOW_SIZE[1] // 2
    xPosSelector = WINDOW_SIZE[0] // 2

    weaponMenuTXT = [pygame.font.Font(standart_font, 30).render(language_system.txt_lang["WEAPON_MENU"],
                                                                scene_config.AA_TEXT,
                                                                (255, 255, 255)),
                     pygame.font.Font(standart_font, 25).render(language_system.txt_lang["PRICE"] + ":",
                                                                scene_config.AA_TEXT,
                                                                (255, 255, 255)),
                     pygame.font.Font(standart_font, 10).render(language_system.txt_lang["UPGRADE_AIM"],
                                                                scene_config.AA_TEXT,
                                                                (255, 255, 255)),
                     pygame.font.Font(standart_font, 18).render(language_system.txt_lang["INFO_AIM"],
                                                                scene_config.AA_TEXT,
                                                                (255, 255, 255)),
                     pygame.font.Font(standart_font, 18).render(language_system.txt_lang["INFO_STABILIZ"],
                                                                scene_config.AA_TEXT,
                                                                (255, 255, 255)),
                     pygame.font.Font(standart_font, 18).render(language_system.txt_lang["INFO_BOMB"],
                                                                scene_config.AA_TEXT,
                                                                (255, 255, 255)),
                     pygame.font.Font(standart_font, 39).render(language_system.txt_lang["OFF_WEAPON"],
                                                                scene_config.AA_TEXT,
                                                                (255, 95, 255)),
                     pygame.font.Font(standart_font, 20).render(language_system.txt_lang["MAIN_MENU"],
                                                                scene_config.AA_TEXT,
                                                                (255, 95, 255)),
                     pygame.font.Font(standart_font, 20).render(language_system.txt_lang["CELL"],
                                                                scene_config.AA_TEXT,
                                                                (255, 95, 255)),
                     pygame.font.Font(standart_font, 52).render(language_system.txt_lang["RANK_REQUEST"]+":",
                                                                scene_config.AA_TEXT,
                                                                (255, 0, 100)),
                     pygame.font.Font(standart_font, 21).render(language_system.txt_lang["RANK_REQUEST"]+":",
                                                                scene_config.AA_TEXT,
                                                                (255, 0, 100))
                     ]

    key_j = key_image["j"]
    key_k = key_image["l"]

    arrow_image_1 = pygame.image.load("Assets/sprite/ui/arrow.png").convert_alpha()
    arrow_image_2 = pygame.image.load("Assets/sprite/ui/arrow.png").convert_alpha()

    arrow_image_1 = pygame.transform.scale(arrow_image_1, (11 * 4, 8 * 4))
    arrow_image_2 = pygame.transform.scale(arrow_image_2, (11 * 4, 8 * 4))
    arrow_image_2 = pygame.transform.flip(arrow_image_2, True, False)

    frame_arrow_right = 0
    frame_arrow_left = 0

    x_arrow_right = WINDOW_SIZE[0] // 2 + 200
    x_arrow_left = WINDOW_SIZE[0] // 2 - 200

    weaponMenuTXT[6] = pygame.transform.rotate(weaponMenuTXT[6], -27)

    txt_exit_rect = weaponMenuTXT[7].get_rect(center=(90, 667))
    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    if select_upgrade < 1 and len(weapon_img) - 1 > 0:
        key_k.set_alpha(300)
        key_j.set_alpha(300)
        arrow_image_1.set_alpha(300)
        arrow_image_2.set_alpha(300)
    else:
        key_k.set_alpha(50)
        key_j.set_alpha(50)
        arrow_image_1.set_alpha(50)
        arrow_image_2.set_alpha(50)

    remove_txt_rect = weaponMenuTXT[8].get_rect(
                            center=(WINDOW_SIZE[0] - 344 + len(language_system.txt_lang["CELL"]) - 1, 667))
    
    remove_txt_rect.right = key_image["backspace"].get_rect(center=(WINDOW_SIZE[0] - 251, 665)).left - 3

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1
            if e.type == pygame.JOYHATMOTION:
                check_gamepad(gamepad_calibrate.input_sys)
                key_j = key_image["j"]
                key_k = key_image["l"]
                if select_upgrade < 1 and len(weapon_img) - 1 > 0:
                    key_k.set_alpha(300)
                    key_j.set_alpha(300)
                    arrow_image_1.set_alpha(300)
                    arrow_image_2.set_alpha(300)
                else:
                    key_k.set_alpha(50)
                    key_j.set_alpha(50)
                    arrow_image_1.set_alpha(50)
                    arrow_image_2.set_alpha(50)
                if scene_config.p.RANK >= 1:
                    if e.value == (-1, 0):
                        switch_select_menu.play()
                        select_upgrade -= 1
                    if e.value == (1, 0):
                        switch_select_menu.play()
                        select_upgrade += 1
                    if e.value == (0, 1):
                        switch_select_menu.play()
                        select_upgrade = 0
                    if scene_config.p.RANK >= 1:
                        if e.value == (0, -1):
                            switch_select_menu.play()
                            select_upgrade = 1
                select_upgrade %= 4
            if e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                key_j = key_image["j"]
                key_k = key_image["l"]
                if select_upgrade < 1 and len(weapon_img) - 1 > 0:
                    key_k.set_alpha(300)
                    key_j.set_alpha(300)
                    arrow_image_1.set_alpha(300)
                    arrow_image_2.set_alpha(300)
                else:
                    key_k.set_alpha(50)
                    key_j.set_alpha(50)
                    arrow_image_1.set_alpha(50)
                    arrow_image_2.set_alpha(50)
                if e.button == scene_config.control_gamepad["A"]:
                    match select_upgrade:
                        case 1:
                            if scene_config.p.MONEY >= price_upgrade[0] and not scene_config.p.weapon[select].isAim:
                                scene_config.p.MONEY -= price_upgrade[0]
                                scene_config.p.weapon[select].isAim = True
                        case 2:
                            if scene_config.p.MONEY >= price_upgrade[1] and not scene_config.p.weapon[select].isStabilization:
                                scene_config.p.MONEY -= price_upgrade[1]
                                scene_config.p.weapon[select].isStabilization = True
                        case 3:
                            if scene_config.p.MONEY >= price_upgrade[2] and not scene_config.p.weapon[select].isBombShoot:
                                scene_config.p.MONEY -= price_upgrade[2]
                                scene_config.p.weapon[select].isBombShoot = True
                            elif scene_config.p.MONEY >= price_upgrade[2] and scene_config.p.weapon[select].bombCount < 10 and scene_config.p.weapon[select].isBombShoot:
                                scene_config.p.MONEY -= price_upgrade[2]
                                scene_config.p.weapon[select].bombCount += 1
                if select > 0 and select_upgrade == 0:
                    if scene_config.p.RANK >= 2:
                        if e.button == scene_config.control_gamepad["A"]:
                            if scene_config.p.weapon[select].isOn:
                                scene_config.p.weapon[select].isOn = False
                                weapon_img[select].set_alpha(90)
                            else:
                                scene_config.p.weapon[select].isOn = True
                                weapon_img[select].set_alpha(300)
                        if e.button == scene_config.control_gamepad["X"]:
                            cell_weapon.play()
                            for i in range(5):
                                PhysicalParticle(WINDOW_SIZE[0] / 2 + randint(-55, 55),
                                                 WINDOW_SIZE[1] / 2 - randint(35, 65),
                                                 -randint(1, 10), image_money[0], (12 * 2, 8 * 2))
                            scene_config.p.weapon.pop(select)
                            scene_config.p.MONEY += randint(15, 55)
                            running = False
                            scene(scene_exit_w)
                if e.button == scene_config.control_gamepad["B"]:
                    running = False
                    scene_exit()
                    return 
                if select_upgrade == 0:
                    if len(weapon_img) - 1 > 0:
                        if e.button == scene_config.control_gamepad["L_BUMPER"] and frame_arrow_left <= 1:
                            switch_gun_menu.play()
                            frame_arrow_left = 8
                            x_arrow_left = WINDOW_SIZE[0] // 2 - 200
                            select -= 1
                        if e.button == scene_config.control_gamepad["R_BUMPER"] and frame_arrow_right <= 1:
                            switch_gun_menu.play()
                            frame_arrow_right = 8
                            x_arrow_right = WINDOW_SIZE[0] // 2 + 200
                            select += 1
                        select %= len(weapon_img)
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                key_j = key_image["j"]
                key_k = key_image["l"]
                if select_upgrade < 1 and len(weapon_img) - 1 > 0:
                    key_k.set_alpha(300)
                    key_j.set_alpha(300)
                    arrow_image_1.set_alpha(300)
                    arrow_image_2.set_alpha(300)
                else:
                    key_k.set_alpha(50)
                    key_j.set_alpha(50)
                    arrow_image_1.set_alpha(50)
                    arrow_image_2.set_alpha(50)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if e.key == pygame.K_RETURN:
                    match select_upgrade:
                        case 1:
                            if scene_config.p.MONEY >= price_upgrade[0] and not scene_config.p.weapon[select].isAim:
                                scene_config.p.MONEY -= price_upgrade[0]
                                scene_config.p.weapon[select].isAim = True
                        case 2:
                            if scene_config.p.MONEY >= price_upgrade[1] and not scene_config.p.weapon[select].isStabilization:
                                scene_config.p.MONEY -= price_upgrade[1]
                                scene_config.p.weapon[select].isStabilization = True
                        case 3:
                            if scene_config.p.MONEY >= price_upgrade[2] and not scene_config.p.weapon[select].isBombShoot:
                                scene_config.p.MONEY -= price_upgrade[2]
                                scene_config.p.weapon[select].isBombShoot = True
                            elif scene_config.p.MONEY >= price_upgrade[2] and scene_config.p.weapon[select].bombCount < 10 and scene_config.p.weapon[select].isBombShoot:
                                scene_config.p.MONEY -= price_upgrade[2]
                                scene_config.p.weapon[select].bombCount += 1

                if select > 0 and select_upgrade == 0:
                    if scene_config.p.RANK >= 2:
                        if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                            if scene_config.p.weapon[select].isOn:
                                scene_config.p.weapon[select].isOn = False
                                weapon_img[select].set_alpha(90)
                            else:
                                scene_config.p.weapon[select].isOn = True
                                weapon_img[select].set_alpha(300)
                        if e.key == pygame.K_BACKSPACE:
                            cell_weapon.play()
                            for i in range(5):
                                PhysicalParticle(WINDOW_SIZE[0] / 2 + randint(-55, 55),
                                                 WINDOW_SIZE[1] / 2 - randint(35, 65),
                                                 -randint(1, 10), image_money[0], (12 * 2, 8 * 2))
                            scene_config.p.weapon.pop(select)
                            scene_config.p.MONEY += randint(15, 55)
                            running = False
                            scene(scene_exit_w)
                if scene_config.p.RANK >= 1:
                    if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                        switch_select_menu.play()
                        select_upgrade -= 1
                    if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                        switch_select_menu.play()
                        select_upgrade += 1
                    if e.key == pygame.K_UP or e.key == pygame.K_w:
                        switch_select_menu.play()
                        select_upgrade = 0
                if select_upgrade == 0:
                    if scene_config.p.RANK >= 1:
                        if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                            switch_select_menu.play()
                            select_upgrade = 1
                    if len(weapon_img) - 1 > 0:
                        if e.key == pygame.K_j and frame_arrow_left <= 1:
                            switch_gun_menu.play()
                            frame_arrow_left = 8
                            x_arrow_left = WINDOW_SIZE[0] // 2 - 200
                            select -= 1
                        if e.key == pygame.K_l and frame_arrow_right <= 1:
                            switch_gun_menu.play()
                            frame_arrow_right = 8
                            x_arrow_right = WINDOW_SIZE[0] // 2 + 200
                            select += 1
                        select %= len(weapon_img)
                if e.key == pygame.K_ESCAPE:
                    running = False
                    scene_exit()
                    return 
                select_upgrade %= 4

        sc.fill((9, 3, 9))
        particle_group.draw(sc)
        particle_group.update()

        try:
            txt_count_weapon = pygame.font.Font(standart_font, 26).render(str(select+1)+"/"+str(len(scene_config.p.weapon)), scene_config.AA_TEXT, (255, 255, 255))
            sc.blit(txt_count_weapon, txt_count_weapon.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 75)))
        except IndexError:
            pass

        match select_upgrade:
            case 0:
                yPosSelector = WINDOW_SIZE[1] // 2
                xPosSelector = WINDOW_SIZE[0] // 2
            case 1:
                if scene_config.p.MONEY < price_upgrade[0]:
                    color_txt_price = (255, 0, 0)
                else:
                    color_txt_price = (255, 255, 255)
                weaponMenuTXT[1] = pygame.font.Font(standart_font, 30).render(
                    language_system.txt_lang["PRICE"] + ": " + str(price_upgrade[0]),
                    scene_config.AA_TEXT,
                    color_txt_price)
                yPosSelector = WINDOW_SIZE[1] // 2 + 190
                xPosSelector = WINDOW_SIZE[0] // 2 - 130
                sc.blit(weaponMenuTXT[3], weaponMenuTXT[3].get_rect(center=(WINDOW_SIZE[0] // 2, 640)))
            case 2:
                if scene_config.p.MONEY < price_upgrade[1]:
                    color_txt_price = (255, 0, 0)
                else:
                    color_txt_price = (255, 255, 255)
                weaponMenuTXT[1] = pygame.font.Font(standart_font, 30).render(
                    language_system.txt_lang["PRICE"] + ": " + str(price_upgrade[1]),
                    scene_config.AA_TEXT,
                    color_txt_price)
                yPosSelector = WINDOW_SIZE[1] // 2 + 190
                xPosSelector = WINDOW_SIZE[0] // 2
                sc.blit(weaponMenuTXT[4], weaponMenuTXT[4].get_rect(center=(WINDOW_SIZE[0] // 2, 640)))
            case 3:
                if scene_config.p.MONEY < price_upgrade[2]:
                    color_txt_price = (255, 0, 0)
                else:
                    color_txt_price = (255, 255, 255)
                weaponMenuTXT[1] = pygame.font.Font(standart_font, 30).render(
                    language_system.txt_lang["PRICE"] + ": " + str(price_upgrade[2]),
                    scene_config.AA_TEXT,
                    color_txt_price)
                yPosSelector = WINDOW_SIZE[1] // 2 + 190
                xPosSelector = WINDOW_SIZE[0] // 2 + 130
                sc.blit(weaponMenuTXT[5], weaponMenuTXT[5].get_rect(center=(WINDOW_SIZE[0] // 2, 640)))

        try:
            if select > 0 and not scene_config.p.weapon[select].isOn:
                weapon_img[select].set_alpha(95)
        except IndexError:
            pass

        sc.blit(weapon_img[select], weapon_img[select].get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 5)))
        selector_img_full = anim_selector.update()
        selector_img_full = pygame.transform.scale(selector_img_full, (16 * 8, 16 * 8))
        sc.blit(selector_img_full, selector_img_full.get_rect(center=(xPosSelector, yPosSelector)))

        try:
            if not scene_config.p.weapon[select].isAim:
                AimImg.set_alpha(30)
            else:
                AimImg.set_alpha(300)
        except IndexError:
            pass

        try:
            if not scene_config.p.weapon[select].isStabilization:
                StabilizationImg.set_alpha(30)
            else:
                StabilizationImg.set_alpha(300)
        except IndexError:
            pass

        try:
            if not scene_config.p.weapon[select].isBombShoot:
                BombShootImg.set_alpha(30)
            else:
                BombShootImg.set_alpha(300)
        except IndexError:
            pass

        sc.blit(AimImg, AimImg.get_rect(center=(WINDOW_SIZE[0] // 2 - 130, WINDOW_SIZE[1] // 2 + 190)))
        sc.blit(StabilizationImg, StabilizationImg.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 + 190)))
        sc.blit(BombShootImg, BombShootImg.get_rect(center=(WINDOW_SIZE[0] // 2 + 130, WINDOW_SIZE[1] // 2 + 190)))

        try:
            txt_bomb = pygame.font.Font(standart_font, 24).render(str(scene_config.p.weapon[select].bombCount)+"/10", scene_config.AA_TEXT, (255, 255, 255))
            txt_bomb_rect = txt_bomb.get_rect(center=(WINDOW_SIZE[0] // 2 + 130, WINDOW_SIZE[1] // 2 + 189))
            txt_bomb_rect.left = BombShootImg.get_rect(center=(WINDOW_SIZE[0] // 2 + 130, WINDOW_SIZE[1] // 2 + 190)).right + 9
            if scene_config.p.weapon[select].isBombShoot:
                sc.blit(txt_bomb, txt_bomb_rect)
        except IndexError:
            pass

        sc.blit(weaponMenuTXT[0], weaponMenuTXT[0].get_rect(center=(WINDOW_SIZE[0] // 2, 75)))

        if select_upgrade >= 1:
            sc.blit(weaponMenuTXT[1],
                    weaponMenuTXT[1].get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 + 112)))

        sc.blit(key_k, key_k.get_rect(center=(WINDOW_SIZE[0] // 2 + 125, WINDOW_SIZE[1] // 2)))
        sc.blit(key_j, key_j.get_rect(center=(WINDOW_SIZE[0] // 2 - 125, WINDOW_SIZE[1] // 2)))

        if frame_arrow_right > 0:
            frame_arrow_right -= 1
            x_arrow_right += 1.5
        else:
            x_arrow_right = WINDOW_SIZE[0] // 2 + 200

        if frame_arrow_left > 0:
            frame_arrow_left -= 1
            x_arrow_left -= 1.5
        else:
            x_arrow_left = WINDOW_SIZE[0] // 2 - 200

        if select_upgrade < 1 and len(weapon_img) - 1 > 0:
            key_k.set_alpha(300)
            key_j.set_alpha(300)
            arrow_image_1.set_alpha(300)
            arrow_image_2.set_alpha(300)
        else:
            key_k.set_alpha(50)
            key_j.set_alpha(50)
            arrow_image_1.set_alpha(50)
            arrow_image_2.set_alpha(50)

        sc.blit(arrow_image_1, arrow_image_1.get_rect(center=(x_arrow_right, WINDOW_SIZE[1] // 2)))
        sc.blit(arrow_image_2, arrow_image_2.get_rect(center=(x_arrow_left - 1, WINDOW_SIZE[1] // 2)))

        try:
            if not scene_config.p.weapon[select].isOn:
                sc.blit(weaponMenuTXT[6], weaponMenuTXT[6].get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
        except IndexError:
            pass

        money_txt_count = pygame.font.Font(standart_font, 24).render(
            language_system.txt_lang["MONEY"] + ": " + str(scene_config.p.MONEY), scene_config.AA_TEXT,
            (0, 255, 0))

        sc.blit(money_txt_count, money_txt_count.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 156)))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(weaponMenuTXT[7], txt_exit_rect)

        if scene_config.p.RANK < 1:
            sc.blit(weaponMenuTXT[9],
                    weaponMenuTXT[9].get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 + 195)))
            sc.blit(rank_image[1],
                    rank_image[1].get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 + 250)))

        if scene_config.p.RANK < 2:
            sc.blit(weaponMenuTXT[10],
                    weaponMenuTXT[10].get_rect(center=(WINDOW_SIZE[0] - 125 - len(language_system.txt_lang["RANK_REQUEST"]) - 1, 625)))
            sc.blit(rank_image[2],
                    rank_image[2].get_rect(center=(WINDOW_SIZE[0] - 135, 651)))

        else:
            if select > 0 and select_upgrade == 0:
                sc.blit(key_image["backspace"], key_image["backspace"].get_rect(center=(WINDOW_SIZE[0] - 251, 665)))
                sc.blit(weaponMenuTXT[8],
                        remove_txt_rect)
            sc.blit(key_image["enter"], key_image["enter"].get_rect(center=(WINDOW_SIZE[0] - 110, 665)))
            sc.blit(txt_ok, txt_ok.get_rect(center=(WINDOW_SIZE[0] - 175, 668)))

        draw_notification()
        pygame.display.update()
        clock.tick(FPS)
