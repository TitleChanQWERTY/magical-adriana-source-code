import os
import shutil
from random import randint

import pygame
from config import sc, clock, WINDOW_SIZE, standart_font
from group_conf import fill_screen, particle_group
from scene import scene_config, scene_tutorial, scene_player_profile, scene_select_mode
import language_system
from sfx_colection import game_type_txt, player_take_damage, write_txt_dialog, player_start_lvl
from screen_fill import ScreenFill
from screenshoot import draw_notification
from particle import PhysicalParticle
from collider import player_blood


def scene_start_dialog():
    pygame.mixer.music.stop()
    for particle in particle_group:
        particle.kill()
    running: bool = True
    FPS: int = 60

    subtitles_txt = [language_system.txt_lang["TXT_START_GAME"], language_system.txt_lang["TXT_START_GAME_NAME"],
                     language_system.txt_lang["TXT_START_GAME_AGE"], language_system.txt_lang["TXT_START_GAME_NEXT"],
                     language_system.txt_lang["TXT_START_GAME_FINAL"]]

    select_txt = 0

    index_txt = 0

    font = pygame.font.Font(standart_font, 17)
    text_lines = []

    words = subtitles_txt[0].split(" ")
    current_line = words[0]

    for word in words[1:]:
        rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
        if rendered_word.get_width() <= sc.get_width():
            current_line += " " + word
        else:
            text_lines.append(current_line)
            current_line = word
    text_lines.append(current_line)

    time_index = 0

    isKey = False

    next_image = pygame.image.load("Assets/sprite/ui/txt_dialog/next.png").convert_alpha()
    next_image = pygame.transform.scale(next_image, (7 * 4, 5 * 4))

    frame_anim_next = 0
    yPosNext = 660

    time_start = 0
    time_start_anim = 0

    color_back = [0, 0, 0]

    power_shake = 0

    time_next = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYBUTTONDOWN and isKey:
                time_index = 0
                isKey = False
                select_txt += 1
                if select_txt == 5:
                    ScreenFill(1, 5, (0, 0, 0), 100)
                if select_txt <= 4:
                    text_lines.clear()
                    index_txt = 0

                    text_lines = []

                    words = subtitles_txt[select_txt].split(" ")
                    current_line = words[0]

                    for word in words[1:]:
                        rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
                        if rendered_word.get_width() <= sc.get_width():
                            current_line += " " + word
                        else:
                            text_lines.append(current_line)
                            current_line = word
                    text_lines.append(current_line)
            elif e.type == pygame.KEYDOWN and isKey:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                else:
                    isKey = False
                    time_index = 0
                    select_txt += 1
                    if select_txt == 5:
                        ScreenFill(1, 5, (0, 0, 0), 100)
                    if select_txt <= 4:
                        text_lines.clear()
                        index_txt = 0

                        text_lines = []

                        words = subtitles_txt[select_txt].split(" ")
                        current_line = words[0]

                        for word in words[1:]:
                            rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
                            if rendered_word.get_width() <= sc.get_width():
                                current_line += " " + word
                            else:
                                text_lines.append(current_line)
                                current_line = word
                        text_lines.append(current_line)

        sc.fill((0, 0, 0))

        if time_start >= 465:
            for i, line in enumerate(text_lines):
                if select_txt == 1:
                    txt_name = line + ": " + str(scene_config.p.NAME)
                    rendered_line = font.render(txt_name[:index_txt], scene_config.AA_TEXT,
                                                (255, 255, 255))
                    if time_index > 1:
                        if index_txt < len(txt_name):
                            index_txt += 1
                            write_txt_dialog.play()
                        else:
                            isKey = True
                        time_index = 0
                    time_index += 1
                elif select_txt == 2:
                    txt_age = line + ": " + str(scene_config.p.AGE)
                    rendered_line = font.render(txt_age[:index_txt], scene_config.AA_TEXT,
                                                (255, 255, 255))
                    if time_index > 1:
                        if index_txt < len(txt_age):
                            index_txt += 1
                            write_txt_dialog.play()
                        else:
                            isKey = True
                        time_index = 0
                    time_index += 1
                else:
                    rendered_line = font.render(line[:index_txt], scene_config.AA_TEXT, (255, 255, 255))
                    if time_index > 1:
                        if index_txt <= len(line):
                            index_txt += 1
                            write_txt_dialog.play()
                        else:
                            isKey = True
                        time_index = 0
                    time_index += 1
                sc.blit(rendered_line, rendered_line.get_rect(
                    center=(WINDOW_SIZE[0] / 2, 590 + i * rendered_line.get_height())))

            if isKey:
                if frame_anim_next == 0:
                    yPosNext = 685
                if frame_anim_next < 25:
                    yPosNext -= 1
                    frame_anim_next += 1
                else:
                    frame_anim_next = 0
                sc.blit(next_image, next_image.get_rect(center=(WINDOW_SIZE[0] / 2, yPosNext)))
            if select_txt >= 5:
                if time_next >= 95:
                    running = False
                    scene_1()
                    return 0
                else:
                    time_next += 1

        elif time_start == 200:
            for i in range(700):
                size = randint(9, 17)
                PhysicalParticle(WINDOW_SIZE[0]/2+randint(-636, 636), randint(-15, 75), -randint(10, 41), player_blood[randint(0, 1)], (size, size))
            time_start += 1
        elif time_start == 203:
            player_start_lvl.play()
            ScreenFill(256, -5, (255, 0, 255), 75)
            time_start += 1
        else:
            if time_start < 200:
                screen_shake = [0, 0]
                if time_start_anim >= 75:
                    power_shake += 1
                if time_start_anim >= 90:
                    color_back[0] += 1
                    color_back[2] += 1
                    screen_shake[0] = randint(-power_shake, power_shake)
                    screen_shake[1] = randint(-power_shake, power_shake)
                    pygame.draw.rect(sc, color_back,
                                     (screen_shake[0], screen_shake[1], WINDOW_SIZE[0] + 35, WINDOW_SIZE[1] + 35))
                else:
                    time_start_anim += 1
            time_start += 1

        particle_group.draw(sc)
        particle_group.update()

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()
        pygame.display.update()
        clock.tick(FPS)


def scene_1():
    running: bool = True
    FPS: int = 60

    accept_txt = language_system.txt_lang["TXT_ACCEPT_RULE"]

    index_txt = 0

    select = 0

    txt_select_yes = pygame.font.Font(standart_font, 22).render(language_system.txt_lang["TXT_YES"],
                                                                scene_config.AA_TEXT, (15, 255, 15))
    txt_select_no = pygame.font.Font(standart_font, 22).render(language_system.txt_lang["TXT_NO"],
                                                               scene_config.AA_TEXT, (255, 15, 15))

    time_index = 0

    isNext = False

    time_next = 0

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (55 * 5, 15 * 5))
    xPosSelector = 150

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if index_txt >= len(accept_txt):
                if e.type == pygame.JOYHATMOTION:
                    if e.value == (-1, 0):
                        select -= 1
                    elif e.value == (1, 0):
                        select += 1
                    select %= 2
                if e.type == pygame.JOYBUTTONDOWN and not isNext:
                    if e.button == scene_config.control_gamepad["A"]:
                        if select == 0:
                            isNext = True
                            ScreenFill(1, 5, (0, 0, 0), 100)
                        else:
                            running = False
                            kill_scene()
                            return 0
                if e.type == pygame.KEYDOWN and not isNext:
                    if e.key == pygame.K_RETURN:
                        if select == 0:
                            isNext = True
                            ScreenFill(1, 5, (0, 0, 0), 100)
                        else:
                            running = False
                            kill_scene()
                            return 0
                    if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                        select -= 1
                    if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                        select += 1
                    select %= 2

        sc.fill((0, 0, 0))
        if index_txt < len(accept_txt):
            if time_index >= 4:
                game_type_txt.play()
                index_txt += 1
                time_index = 0
            else:
                time_index += 1
        else:
            if select == 0:
                xPosSelector = WINDOW_SIZE[0] / 2 - 150
            else:
                xPosSelector = WINDOW_SIZE[0] / 2 + 150
            sc.blit(selector_img, selector_img.get_rect(center=(xPosSelector, WINDOW_SIZE[1] / 2 + 95)))
            sc.blit(txt_select_yes, txt_select_yes.get_rect(center=(WINDOW_SIZE[0] / 2 - 150, WINDOW_SIZE[1] / 2 + 95)))
            if select == 0:
                pos_no = (randint(-1, 1), randint(-1, 1))
            else:
                pos_no = (randint(-5, 5), randint(-5, 5))
            sc.blit(txt_select_no, txt_select_no.get_rect(
                center=(WINDOW_SIZE[0] / 2 + 150 + pos_no[0], WINDOW_SIZE[1] / 2 + 95 + pos_no[1])))
        txt = pygame.font.Font(standart_font, 27).render(accept_txt[:index_txt], scene_config.AA_TEXT, (255, 0, 245))
        sc.blit(txt, txt.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 5)))
        if isNext:
            if time_next >= 95:
                running = False
                scene_select_mode.scene_show_control(scene_tutorial.scene)
                return 0
            else:
                time_next += 1

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()
        pygame.display.update()
        clock.tick(FPS)


def kill_scene():
    running: bool = True
    FPS: int = 60

    ScreenFill(205, -5, (255, 5, 5), 50)

    txt_kill = pygame.font.Font(standart_font, 45 - len(str(scene_config.p.NAME))).render(
        "\"" + str(scene_config.p.NAME) + "\"" + " " +
        language_system.txt_lang["KILL_MESSAGE_ANGLE"],
        scene_config.AA_TEXT, (255, 0, 0))

    time_next = 0

    profile_file = os.listdir("Profile")
    shutil.rmtree("Profile/" + profile_file[scene_player_profile.select_profile_num])

    player_take_damage[1].play()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        sc.fill((20, 0, 0))

        sc.blit(txt_kill, txt_kill.get_rect(
            center=(WINDOW_SIZE[0] / 2 + randint(-10, 10), WINDOW_SIZE[1] / 2 + randint(-10, 10))))

        if time_next == 250:
            ScreenFill(1, 5, (0, 0, 0), 100)
        if time_next >= 350:
            running = False
            return 0
        time_next += 1

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()
        pygame.display.update()
        clock.tick(FPS)
