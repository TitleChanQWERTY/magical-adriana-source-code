import pygame
import config
from config import sc, clock, WINDOW_SIZE, standart_font, key_image, txt_ok, check_gamepad
from group_conf import particle_group, fill_screen
import language_system
import gamepad_calibrate
from scene import scene_config, scene_menu, scene_game, scene_select_music, scene_music_mode, scene_music_select
from story import starting_scene
from screen_fill import ScreenFill
from random import randint
from screenshoot import TakeScreenshot, draw_notification
from sfx_colection import sound_play_mode, switch_select_menu, game_type_txt


def scene():
    running: bool = True
    FPS: int = 60

    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["MAIN_MENU"],
                                                          scene_config.AA_TEXT,
                                                          (255, 95, 255))
    
    txt_exit_rect = txt_exit.get_rect(center=(103, 667))
    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    mode_image = [pygame.image.load("Assets/sprite/ui/game_mode_icon/1.png").convert_alpha(),
                  pygame.image.load("Assets/sprite/ui/game_mode_icon/not_detect.png").convert_alpha(),
                  pygame.image.load("Assets/sprite/ui/game_mode_icon/3.png").convert_alpha()]

    for i in range(3):
        mode_image[i] = pygame.transform.scale(mode_image[i], (37 * 5, 55 * 5))

    mode_image[1].set_alpha(110)

    txt_record = pygame.font.Font(standart_font, 18).render(language_system.txt_lang["RECORD_MODE"],
                                                            scene_config.AA_TEXT, (255, 255, 255))

    txt_story = pygame.font.Font(standart_font, 18).render(language_system.txt_lang["IN_BUILD"],
                                                           scene_config.AA_TEXT, (255, 0, 0))

    txt_in_music = pygame.font.Font(standart_font, 18).render(language_system.txt_lang["MUSIC_MODE"],
                                                                scene_config.AA_TEXT, (255, 255, 255))

    selector_img = pygame.image.load("Assets/sprite/ui/selector2.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (43 * 5, 61 * 5))
    xPosSelector = WINDOW_SIZE[0] / 2

    txt_select_mode = pygame.font.Font(standart_font, 35).render(language_system.txt_lang["SELECT_MODE"],
                                                                 scene_config.AA_TEXT, (255, 255, 255))

    select = 1

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1
            if e.type == pygame.JOYHATMOTION:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.value == (-1, 0):
                    select += 1
                    switch_select_menu.play()
                    if select > 2:
                        select = 1
                elif e.value == (1, 0):
                    select -= 1
                    switch_select_menu.play()
                    if select < 1:
                        select = 2
            elif e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.button == scene_config.control_gamepad["A"]:
                    scene_config.select_mode = select
                    running = False
                    if select != 2:
                        scene_difficulty()
                    else:
                        scene_music_select.scene()
                    return 0
                elif e.button == scene_config.control_gamepad["B"]:
                    running = False
                    scene_menu.scene()
                    return 0
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if e.key == pygame.K_LEFT:
                    select -= 1
                    switch_select_menu.play()
                    if select < 1:
                        select = 2
                if e.key == pygame.K_RIGHT:
                    select += 1
                    switch_select_menu.play()
                    if select > 2:
                        select = 1
                if e.key == pygame.K_ESCAPE or e.key == pygame.K_q:
                    running = False
                    scene_menu.scene()
                    return 0
                if e.key == pygame.K_RETURN:
                    scene_config.select_mode = select
                    running = False
                    if select != 2:
                        scene_difficulty()
                    else:
                        scene_music_select.scene()
                    return 0
        sc.fill((9, 3, 9))
        particle_group.draw(sc)
        particle_group.update()
        sc.blit(mode_image[0], mode_image[0].get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2)))
        sc.blit(mode_image[1], mode_image[1].get_rect(center=(WINDOW_SIZE[0] / 2 - 325, WINDOW_SIZE[1] / 2)))
        sc.blit(mode_image[2], mode_image[2].get_rect(center=(WINDOW_SIZE[0] / 2 + 325, WINDOW_SIZE[1] / 2)))

        match select:
            case 0:
                xPosSelector = WINDOW_SIZE[0] / 2 - 325
            case 1:
                xPosSelector = WINDOW_SIZE[0] / 2
            case 2:
                xPosSelector = WINDOW_SIZE[0] / 2 + 325
        
        select = max(1, min(2, select))

        sc.blit(txt_record, txt_record.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 110)))

        sc.blit(txt_story, txt_story.get_rect(center=(WINDOW_SIZE[0] / 2 - 325, WINDOW_SIZE[1] / 2 + 110)))
        sc.blit(txt_in_music, txt_in_music.get_rect(center=(WINDOW_SIZE[0] / 2 + 325, WINDOW_SIZE[1] / 2 + 110)))

        sc.blit(txt_select_mode, txt_select_mode.get_rect(center=(WINDOW_SIZE[0] / 2, 75)))

        sc.blit(selector_img, selector_img.get_rect(center=(xPosSelector, WINDOW_SIZE[1] / 2 + 1)))

        sc.blit(key_image["enter"], key_image["enter"].get_rect(center=(WINDOW_SIZE[0] - 110, 665)))
        sc.blit(txt_ok, txt_ok.get_rect(center=(WINDOW_SIZE[0] - 175, 668)))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()
        pygame.display.update()
        clock.tick(FPS)


def scene_difficulty():
    running: bool = True
    FPS: int = 60

    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["BACK"],
                                                          scene_config.AA_TEXT,
                                                          (255, 95, 255))
    
    txt_exit_rect = txt_exit.get_rect(center=(90, 667))
    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    difficulty_image = [pygame.image.load("Assets/sprite/ui/difficulty_icon/1.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/ui/difficulty_icon/2.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/ui/difficulty_icon/3.png").convert_alpha()]

    for i in range(3):
        difficulty_image[i] = pygame.transform.scale(difficulty_image[i], (57 * 4, 18 * 4))

    isKey = False

    frame_switch = 0

    select = 0

    selector_img = pygame.image.load("Assets/sprite/ui/selector3.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (62 * 4, 22 * 4))
    xPosSelector = WINDOW_SIZE[0] / 2 - 300

    txt_easy = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["EASY_DIFFICULTY"],
                                                          scene_config.AA_TEXT,
                                                          (0, 255, 0))
    txt_normal = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["NORMAL_DIFFICULTY"],
                                                            scene_config.AA_TEXT,
                                                            (255, 255, 0))
    txt_hard = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["HARD_DIFFICULTY"],
                                                          scene_config.AA_TEXT,
                                                          (255, 0, 0))

    txt_select_difficulty = pygame.font.Font(standart_font, 35).render(language_system.txt_lang["SELECT_DIFFICULTY"],
                                                                       scene_config.AA_TEXT, (255, 255, 255))

    music_volume = scene_config.music_volume

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYHATMOTION:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.value == (1, 0):
                    switch_select_menu.play()
                    select += 1
                elif e.value == (-1, 0):
                    switch_select_menu.play()
                    select -= 1
                select %= 3
            elif e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if not isKey:
                    if e.button == scene_config.control_gamepad["A"]:
                        if scene_config.select_mode != 1:
                            ScreenFill(1, 10, (0, 0, 0), 50)
                        scene_config.difficulty = select
                        scene_config.set_difficulty()
                        isKey = True
                    elif e.button == scene_config.control_gamepad["B"]:
                        running = False
                        scene()
                        return 0
            if e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                elif not isKey:
                    if e.key == pygame.K_ESCAPE or e.key == pygame.K_q:
                        running = False
                        scene()
                        return 0
                    if e.key == pygame.K_RETURN or e.key == pygame.K_e:
                        if scene_config.select_mode != 1:
                            ScreenFill(1, 10, (0, 0, 0), 50)
                        scene_config.difficulty = select
                        scene_config.set_difficulty()
                        isKey = True
                    if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                        switch_select_menu.play()
                        select -= 1
                    if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                        switch_select_menu.play()
                        select += 1
                    select %= 3
        sc.fill((9, 3, 9))
        particle_group.draw(sc)
        particle_group.update()

        match select:
            case 0:
                xPosSelector = WINDOW_SIZE[0] / 2 - 300
            case 1:
                xPosSelector = WINDOW_SIZE[0] / 2
            case 2:
                xPosSelector = WINDOW_SIZE[0] / 2 + 300

        sc.blit(difficulty_image[0],
                difficulty_image[0].get_rect(center=(WINDOW_SIZE[0] / 2 - 300, WINDOW_SIZE[1] / 2)))
        sc.blit(difficulty_image[1],
                difficulty_image[1].get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2)))
        sc.blit(difficulty_image[2],
                difficulty_image[2].get_rect(center=(WINDOW_SIZE[0] / 2 + 300, WINDOW_SIZE[1] / 2)))

        sc.blit(txt_easy, txt_easy.get_rect(center=(WINDOW_SIZE[0] / 2 - 320, WINDOW_SIZE[1] / 2)))
        sc.blit(txt_normal, txt_normal.get_rect(center=(WINDOW_SIZE[0] / 2 + 10, WINDOW_SIZE[1] / 2)))
        sc.blit(txt_hard, txt_hard.get_rect(center=(WINDOW_SIZE[0] / 2 + 310, WINDOW_SIZE[1] / 2)))

        sc.blit(txt_select_difficulty, txt_select_difficulty.get_rect(center=(WINDOW_SIZE[0] / 2, 75)))

        sc.blit(selector_img, selector_img.get_rect(center=(xPosSelector, WINDOW_SIZE[1] / 2)))

        if frame_switch >= 45:
            running = False
            scene_show_control()
            return 0
        if isKey:
            if scene_config.select_mode == 1:
                running = False
                scene_select_music.scene_select_music_record()
                return 0
            else:
                music_volume -= 0.03
                music_volume = max(music_volume, 0.0)
                pygame.mixer.music.set_volume(music_volume)
                frame_switch += 1

        sc.blit(key_image["enter"], key_image["enter"].get_rect(center=(WINDOW_SIZE[0] - 110, 665)))
        sc.blit(txt_ok, txt_ok.get_rect(center=(WINDOW_SIZE[0] - 175, 668)))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)


def scene_switch():
    running: bool = True
    FPS: int = 60

    txt_mode = (language_system.txt_lang["STORY_MODE"], language_system.txt_lang["RECORD_MODE"], language_system.txt_lang["MUSIC_MODE"])

    size_txt = 20

    index_txt = 0

    time_write_txt = 0

    frame_scene = 0

    frame_rect = pygame.Rect(WINDOW_SIZE[0] / 2 - 20, WINDOW_SIZE[1] / 2 - 9, 45, 25)

    size_x_rect = 65

    back_fill = [0, 0, 0]

    color_txt = [255, 255, 255]

    for fill in fill_screen:
        fill.kill()

    if scene_config.select_mode == 0:
        color_rect = (255, 0, 245)
    elif scene_config.select_mode == 1:
        color_rect = (255, 255, 0)
    else:
        color_rect = (0, 195, 235)
    
    if pygame.joystick.get_count() > 0:
        if gamepad_calibrate.input_sys == 0 or gamepad_calibrate.input_sys == 2:
            txt_tips = (language_system.txt_lang["TIPS_1_2"], language_system.txt_lang["TIPS_3"], 
                language_system.txt_lang["TIPS_2"], language_system.txt_lang["TIPS_4"],
                language_system.txt_lang["TIPS_5"], language_system.txt_lang["TIPS_6"],
                language_system.txt_lang["TIPS_7"])
        elif gamepad_calibrate.input_sys == 1:
            txt_tips = (language_system.txt_lang["TIPS_1_3"], language_system.txt_lang["TIPS_3"], 
                language_system.txt_lang["TIPS_2"], language_system.txt_lang["TIPS_4"],
                language_system.txt_lang["TIPS_5"], language_system.txt_lang["TIPS_6"],
                language_system.txt_lang["TIPS_7"])
    else:
        txt_tips = (language_system.txt_lang["TIPS_1"], language_system.txt_lang["TIPS_3"], 
                    language_system.txt_lang["TIPS_2"], language_system.txt_lang["TIPS_4"],
                    language_system.txt_lang["TIPS_5"], language_system.txt_lang["TIPS_6"],
                    language_system.txt_lang["TIPS_7"])

    txt_draw_tips = pygame.font.Font(standart_font, 19).render(txt_tips[randint(0, len(txt_tips)-1)], scene_config.AA_TEXT, (255, 255, 255))

    alpha_txt_draw_tips = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()

        sc.fill(back_fill)
        if frame_scene == 255:
            ScreenFill(1, 9, (0, 0, 0), 90)
        if frame_scene >= 340:
            running = False
            match scene_config.select_mode:
                case 0:
                    starting_scene.scene()
                case 1:
                    scene_game.scene()
                case 2:
                    scene_music_mode.scene()

            return 0
        if frame_scene == 156:
            ScreenFill(275, -10, color_rect, 85)
        if frame_scene == 157:
            sound_play_mode.play()
        if frame_scene >= 156:
            color_txt[0] = 0
            color_txt[1] = 0
            color_txt[2] = 0

            back_fill[0] = 5
            back_fill[1] = 2
            back_fill[2] = 5
            size_txt = 22
            size_x_rect += 4
            pygame.draw.rect(sc, color_rect, (frame_rect.centerx, frame_rect.centery - 35, size_x_rect, 55))
            pygame.draw.rect(sc, color_rect,
                             (frame_rect.centerx - size_x_rect, frame_rect.centery - 35, size_x_rect, 55))
        frame_scene += 1
        if frame_scene >= 46:
            if index_txt < len(txt_mode[scene_config.select_mode]):
                if time_write_txt >= 6:
                    game_type_txt.play()
                    index_txt += 1
                    time_write_txt = 0
                else:
                    time_write_txt += 1
            txt_show = pygame.font.Font(standart_font, size_txt).render(txt_mode[scene_config.select_mode][:index_txt],
                                                                        scene_config.AA_TEXT, color_txt)
            sc.blit(txt_show, txt_show.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2-1)))

        alpha_txt_draw_tips += 5
        txt_draw_tips.set_alpha(alpha_txt_draw_tips)
        sc.blit(txt_draw_tips, txt_draw_tips.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]-90)))

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)

def scene_show_control(scene_next=scene_switch):
    pygame.mixer.music.stop()
    running: bool = True
    FPS: int = 60

    if pygame.joystick.get_count() > 0 and config.input_type_s == 1:
        layout_image = pygame.image.load("Assets/sprite/ui/gamepad_key/how_play_d.png").convert_alpha()
        layout_image = pygame.transform.scale(layout_image, (88 * 5, 57 * 5))

        press_any_key = pygame.font.Font(standart_font, 23).render(language_system.txt_lang["PRESS_ANY_KEY"],
                                                                scene_config.AA_TEXT, (255, 255, 255))

        txt_wasd = pygame.font.Font(standart_font, 18).render("LEFT_STICK: " + language_system.txt_lang["MOVE"],
                                                            scene_config.AA_TEXT, (255, 255, 255))
        txt_shoot = pygame.font.Font(standart_font, 18).render("\"O\": " + language_system.txt_lang["SHOOT"],
                                                            scene_config.AA_TEXT, (255, 255, 255))
        txt_next_weapon = pygame.font.Font(standart_font, 18).render("\"R1\": " + language_system.txt_lang["NEXT_WEAPON"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_prev_weapon = pygame.font.Font(standart_font, 18).render("\"L1\": " + language_system.txt_lang["PREVIEW_WEAPON"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_shoot_bomb = pygame.font.Font(standart_font, 18).render("\"Triangle\": " + language_system.txt_lang["SHOOT_BOMB"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_dash = pygame.font.Font(standart_font, 18).render("\"Square\": Dash",
                                                                    scene_config.AA_TEXT, (255, 255, 255))

        txt_control = pygame.font.Font(standart_font, 29).render(language_system.txt_lang["CONTROL"], scene_config.AA_TEXT,
                                                                (255, 255, 255))
    elif pygame.joystick.get_count():
        layout_image = pygame.image.load("Assets/sprite/ui/gamepad_key/how_play.png").convert_alpha()
        layout_image = pygame.transform.scale(layout_image, (88 * 5, 57 * 5))

        press_any_key = pygame.font.Font(standart_font, 23).render(language_system.txt_lang["PRESS_ANY_KEY"],
                                                                scene_config.AA_TEXT, (255, 255, 255))

        txt_wasd = pygame.font.Font(standart_font, 18).render("LEFT_STICK: " + language_system.txt_lang["MOVE"],
                                                            scene_config.AA_TEXT, (255, 255, 255))
        txt_shoot = pygame.font.Font(standart_font, 18).render("\"B\": " + language_system.txt_lang["SHOOT"],
                                                            scene_config.AA_TEXT, (255, 255, 255))
        txt_next_weapon = pygame.font.Font(standart_font, 18).render("\"RB\": " + language_system.txt_lang["NEXT_WEAPON"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_prev_weapon = pygame.font.Font(standart_font, 18).render("\"LB\": " + language_system.txt_lang["PREVIEW_WEAPON"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_shoot_bomb = pygame.font.Font(standart_font, 18).render("\"Y\": " + language_system.txt_lang["SHOOT_BOMB"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_dash = pygame.font.Font(standart_font, 18).render("\"X\": Dash",
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_control = pygame.font.Font(standart_font, 29).render(language_system.txt_lang["CONTROL"], scene_config.AA_TEXT,
                                                                (255, 255, 255))
    else:
        layout_image = pygame.image.load("Assets/sprite/ui/keycaps/key_layout.png").convert_alpha()
        layout_image = pygame.transform.scale(layout_image, (185 * 3, 71 * 3))

        press_any_key = pygame.font.Font(standart_font, 23).render(language_system.txt_lang["PRESS_ANY_KEY"],
                                                                scene_config.AA_TEXT, (255, 255, 255))

        txt_wasd = pygame.font.Font(standart_font, 18).render("WASD: " + language_system.txt_lang["MOVE"],
                                                            scene_config.AA_TEXT, (255, 255, 255))
        txt_shoot = pygame.font.Font(standart_font, 18).render("K: " + language_system.txt_lang["SHOOT"],
                                                            scene_config.AA_TEXT, (255, 255, 255))
        txt_next_weapon = pygame.font.Font(standart_font, 18).render("L: " + language_system.txt_lang["NEXT_WEAPON"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_prev_weapon = pygame.font.Font(standart_font, 18).render("J: " + language_system.txt_lang["PREVIEW_WEAPON"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_shoot_bomb = pygame.font.Font(standart_font, 18).render("I: " + language_system.txt_lang["SHOOT_BOMB"],
                                                                    scene_config.AA_TEXT, (255, 255, 255))
        txt_dash = pygame.font.Font(standart_font, 18).render("\"Space\": Dash",
                                                                    scene_config.AA_TEXT, (255, 255, 255))

        txt_control = pygame.font.Font(standart_font, 29).render(language_system.txt_lang["CONTROL"], scene_config.AA_TEXT,
                                                                (255, 255, 255))
        

    alpha_any_key = 300

    frame_alpha = 0

    isKey = False

    frame_switch = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if not isKey:
                    ScreenFill(1, 10, (0, 0, 0), 100)
                    isKey = True
            if e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if not isKey and e.key != pygame.K_F6:
                    ScreenFill(1, 10, (0, 0, 0), 100)
                    isKey = True

        sc.fill((0, 0, 0))
        sc.blit(txt_control, txt_control.get_rect(center=(WINDOW_SIZE[0] / 2, 75)))
        if frame_switch >= 45:
            running = False
            scene_next()
            return 0
        if isKey:
            frame_switch += 1

        if frame_alpha < 50:
            alpha_any_key -= 5
        else:
            frame_alpha = 0
            alpha_any_key = 300
        frame_alpha += 1

        press_any_key.set_alpha(alpha_any_key)

        sc.blit(layout_image, layout_image.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2)))
        sc.blit(press_any_key, press_any_key.get_rect(center=(WINDOW_SIZE[0] / 2, 630)))

        sc.blit(txt_wasd, txt_wasd.get_rect(center=(145, WINDOW_SIZE[1] / 2 - 40)))
        sc.blit(txt_shoot, txt_shoot.get_rect(center=(145, WINDOW_SIZE[1] / 2 - 10)))
        sc.blit(txt_next_weapon, txt_next_weapon.get_rect(center=(145, WINDOW_SIZE[1] / 2 + 20)))
        sc.blit(txt_prev_weapon, txt_prev_weapon.get_rect(center=(145, WINDOW_SIZE[1] / 2 + 40)))
        sc.blit(txt_shoot_bomb, txt_shoot_bomb.get_rect(center=(165, WINDOW_SIZE[1] / 2 + 110)))
        sc.blit(txt_dash, txt_dash.get_rect(center=(144, WINDOW_SIZE[1] / 2 + 70)))

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)



