import pygame
from config import sc, clock, standart_font, WINDOW_SIZE
from scene import scene_config, scene_menu
import language_system


def scene_dialog():
    running = True
    FPS = 60


    txt = (language_system.txt_lang["END_SCENE_1"], language_system.txt_lang["END_SCENE_2"],
            language_system.txt_lang["END_SCENE_3"], language_system.txt_lang["END_SCENE_4"],
            language_system.txt_lang["END_SCENE_5"], language_system.txt_lang["END_SCENE_6"],
            language_system.txt_lang["END_SCENE_7"], language_system.txt_lang["END_SCENE_8"],
            language_system.txt_lang["END_SCENE_9"])

    len_txt = 0

    select = 0

    time_next = 0
    time_next_max = 180 + len(txt[select])

    isEnd = False
    alpha = 300

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        
        sc.fill((0, 0, 0))
        if not isEnd:
            if time_next >= time_next_max:
                if select >= 8:
                    isEnd = True
                    select = 8
                else:
                    select += 1
                    len_txt = 0
                    time_next = 0
                    time_next_max = 180 + len(txt[select-1])
            elif len_txt <= len(txt[select]):
                len_txt += 1
            time_next += 1
        else:
            alpha -= 4
            if alpha <= 5:
                running = False
                creation()
                return 
        txt_draw = pygame.font.Font(standart_font, 25).render(txt[select][:len_txt], scene_config, (255, 255, 255))
        txt_draw.set_alpha(alpha)
        sc.blit(txt_draw, txt_draw.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2)))
        pygame.display.update()
        clock.tick(FPS)


def creation():
    running = True
    FPS = 60

    txt_type = (language_system.txt_lang["CREDITS_PROGRAMING"], language_system.txt_lang["CREDITS_ART"],
    language_system.txt_lang["CREDITS_SFX"], language_system.txt_lang["CREDITS_MUSIC"],
    language_system.txt_lang["CREDITS_TOOLS"], language_system.txt_lang["CREDITS_SPECIAL"])

    credits_txt = (language_system.txt_lang["AUTOR_NAME"], language_system.txt_lang["AUTOR_NAME"],
    language_system.txt_lang["AUTOR_NAME"], language_system.txt_lang["AUTOR_NAME"],
    "Arch Linux, Pygame, Ocenaudio, LMMS, Aseprite", "Darkinjection, ANT_Dev, Screep, MrHaos, BodaMat", language_system.txt_lang["CREDITS_YOU"])

    select = 0
    len_txt = 0

    len_txt_credit = 0

    time_next = 0

    time_start = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        
        sc.fill((0, 0, 0))
        if time_start == 150:
            pygame.mixer.music.load("Assets/ost/end magical adriana.mp3")
            pygame.mixer.music.play(-1)
            pygame.mixer.music.set_volume(scene_config.music_volume)
            time_start += 1
        if time_start >= 150:
            if time_next >= 260:
                time_next = 0
                select += 1
                len_txt = 0
                len_txt_credit = 0
            time_next += 1
            if select >= len(txt_type):
                running = False
                and_you()
                return
            if select < len(txt_type) and len_txt < len(txt_type[select]):
                len_txt += 1
            if len_txt_credit <= len(credits_txt[select]):
                len_txt_credit += 1
            if select < len(txt_type):
                txt_draw_type = pygame.font.Font(standart_font, 25).render(txt_type[select][:len_txt], scene_config.AA_TEXT, (255, 255, 255))
            txt_draw_credit = pygame.font.Font(standart_font, 30).render(credits_txt[select][:len_txt_credit], scene_config.AA_TEXT, (255, 0, 225))
            sc.blit(txt_draw_type, txt_draw_type.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2-50)))
            sc.blit(txt_draw_credit, txt_draw_credit.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2)))
        else:
            time_start += 1
        pygame.display.update()
        clock.tick(FPS)

def and_you():
    running = True
    FPS = 60

    select = 0
    len_txt_credit = 0

    time_end = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        
        sc.fill((0, 0, 0))
        time_end += 1
        if time_end <= 155:
            if len_txt_credit <= len(language_system.txt_lang["CREDITS_YOU"]):
                len_txt_credit += 1
        if time_end >= 155:
            if len_txt_credit > 0:
                len_txt_credit -= 1
        if time_end >= 245:
            running = False
            next()
            return
        txt_draw_credit = pygame.font.Font(standart_font, 30).render(language_system.txt_lang["CREDITS_YOU"][:len_txt_credit], scene_config.AA_TEXT, (255, 0, 225))
        sc.blit(txt_draw_credit, txt_draw_credit.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2)))
        pygame.display.update()
        clock.tick(FPS)

def next():
    pygame.mixer.music.stop()
    running = True
    FPS = 60

    time_next = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        
        sc.fill((0, 0, 0))
        if time_next >= 45:
            running = False
            scene_menu.scene()
            return
        time_next += 1
        clock.tick(FPS)
