import pygame
from config import sc, clock, WINDOW_SIZE, standart_font
from scene import scene_config, scene_player_profile, scene_start_dialog
from animator import Animator
from screenshoot import draw_notification, TakeScreenshot
import language_system

def start_warning():
    running = True
    FPS = 60

    font = pygame.font.Font(standart_font, 23)
    text_lines = []

    words = language_system.txt_lang["WARNING_INFO"].split(" ")
    current_line = words[0]

    for word in words[1:]:
        rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
        if rendered_word.get_width() < sc.get_width() - 360:
            current_line += " " + word
        else:
            text_lines.append(current_line)
            current_line = word
    text_lines.append(current_line)

    yPosTXT = WINDOW_SIZE[1]/2

    warning_txt = pygame.font.Font(standart_font, 35).render(language_system.txt_lang["WARNING_TXT"], scene_config.AA_TEXT, (195, 0, 0))
    
    time_next = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
        
        sc.fill((0, 0, 0))
        if time_next >= 215:
            running = False
            start_stat_team()
            return
        time_next += 1
        sc.blit(warning_txt, warning_txt.get_rect(center=(WINDOW_SIZE[0]/2, 200)))
        for i, line in enumerate(text_lines):
            rendered_line = font.render(line, scene_config.AA_TEXT, (255, 255, 255))
            sc.blit(rendered_line, rendered_line.get_rect(
                center=(sc.get_width()/2, yPosTXT)))
            yPosTXT += 22
        yPosTXT = WINDOW_SIZE[1]/2

        draw_notification()
        
        pygame.display.update()
        clock.tick(FPS)

def start_stat_team():
    running = True
    FPS = 60

    stat_team_icon = pygame.image.load("Assets/sprite/ui/logo_studio.png").convert_alpha()
    stat_team_icon = pygame.transform.scale(stat_team_icon, (59 * 7, 19 * 7))

    alpha = 0

    time_alpha = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()

        sc.fill((0, 0, 0))
        if time_alpha < 105:
            alpha += 4
        if time_alpha > 105:
            alpha -= 5
        if time_alpha > 290:
            running = False
            start_ihrovary()
            return 0
        time_alpha += 1
        stat_team_icon.set_alpha(alpha)
        sc.blit(stat_team_icon, stat_team_icon.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2)))

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)


def start_ihrovary():
    running = True
    FPS = 60

    ihovary_sprite = (pygame.image.load("Assets/sprite/ui/start_screen/ihrovary/1.png").convert_alpha(),
                      pygame.image.load("Assets/sprite/ui/start_screen/ihrovary/2.png").convert_alpha(),
                      pygame.image.load("Assets/sprite/ui/start_screen/ihrovary/3.png").convert_alpha(),
                      pygame.image.load("Assets/sprite/ui/start_screen/ihrovary/2.png").convert_alpha())

    alpha = 0
    alpha_logo = 0

    Anim = Animator(12, ihovary_sprite)

    sfx = pygame.mixer.Sound("Assets/sfx/start_sfx/IHROVARY.ogg")

    txt_special_thanks = (pygame.font.Font(standart_font, 35).render("SPECIAL THANKS IHROVARY", scene_config.AA_TEXT, (95, 205, 235)),
                          pygame.font.Font(standart_font, 35).render("ОСОБЛИВА ПОДЯКА ІГРОВАРАМ", scene_config.AA_TEXT, (255, 255, 0)))

    time_alpha = 0

    sfx.play()

    volume_sfx = scene_config.sfx_volume
    sfx.set_volume(volume_sfx)

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()

        sc.fill((0, 0, 0))
        image = Anim.update()
        image = pygame.transform.scale(image, (138 * 2, 138 * 2))
        image.set_alpha(alpha_logo)
        sc.blit(image, image.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2)))

        if 205 <= time_alpha < 500:
            alpha += 4
            alpha = max(0, min(290, alpha))
        time_alpha += 1

        if time_alpha >= 625:
            running = False
            scene_start_dialog.scene()
            return 0
        if time_alpha >= 500:
            volume_sfx -= 0.003
            sfx.set_volume(volume_sfx)
            alpha_logo -= 5
            alpha -= 5
        else:
            alpha_logo += 2
            alpha_logo = max(0, min(290, alpha_logo))

        for i in range(2):
            txt_special_thanks[i].set_alpha(alpha)
        sc.blit(txt_special_thanks[0], txt_special_thanks[0].get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2-200)))
        sc.blit(txt_special_thanks[1],
                txt_special_thanks[1].get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 200)))

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
