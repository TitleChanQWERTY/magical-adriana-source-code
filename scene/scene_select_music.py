import pygame
from config import sc, clock, WINDOW_SIZE, standart_font, key_image, txt_ok, check_gamepad
import gamepad_calibrate
from scene import scene_game, scene_select_mode, scene_config
from group_conf import particle_group, fill_screen
import language_system
from screen_fill import ScreenFill
from screenshoot import TakeScreenshot, draw_notification
from sfx_colection import switch_select_menu


def scene_select_music_record():
    running: bool = True
    FPS: int = 60

    txt = ("TitleChanQWERTY - You cant die now. But you die where take my hand", 
    "TitleChanQWERTY - And your sword, take me, new life",
    "TitleChanQWERTY - Why You So Magical Perfect?",
    "Screep - battleTheme1")

    txt_music = (
        pygame.font.Font(standart_font, 20).render("TitleChanQWERTY - You die where take my hand",
                                                   scene_config.AA_TEXT, (255, 255, 255)),
        pygame.font.Font(standart_font, 20).render("TitleChanQWERTY - And your sword, take me, new life",
                                                   scene_config.AA_TEXT, (255, 255, 255)),
        pygame.font.Font(standart_font, 20).render("TitleChanQWERTY - Why You So Magical Perfect?",
                                                   scene_config.AA_TEXT, (255, 255, 255)),
        pygame.font.Font(standart_font, 20).render("Screep - battleTheme1",
                                                   scene_config.AA_TEXT, (255, 255, 255)))

    select = 0

    isKey = False
    time_switch = 0

    txt_ok_k = pygame.font.Font(standart_font, 26).render(language_system.txt_lang["TXT_CONFIRM"], scene_config.AA_TEXT, (255, 0, 255))

    max_select = len(txt_music) + 1

    play_icon = pygame.image.load("Assets/sprite/ui/play_icon.png").convert_alpha()
    play_icon = pygame.transform.scale(play_icon, (8 * 3, 8 * 3))

    xPosPlay = (WINDOW_SIZE[0] / 2 + 297, WINDOW_SIZE[0] / 2 + 326,
                WINDOW_SIZE[0] / 2 + 301, WINDOW_SIZE[0] / 2 + 195)
    yPosPlay = (WINDOW_SIZE[1] / 2 - 162, WINDOW_SIZE[1] / 2 - 107,
                WINDOW_SIZE[1] / 2 - 53, WINDOW_SIZE[1] / 2 + 3)

    music_volume = scene_config.music_volume

    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["BACK"],
                                                          scene_config.AA_TEXT,
                                                          (255, 95, 255))
    txt_exit_rect = txt_exit.get_rect(center=(90, 667))
    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    select_music_txt = pygame.font.Font(standart_font, 30).render(language_system.txt_lang["SELECT_MUSIC"]+" ->", scene_config.AA_TEXT, (255, 255, 255))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1
            if e.type == pygame.JOYHATMOTION:
                check_gamepad(gamepad_calibrate.input_sys)
                if not isKey:
                    if e.value == (0, -1):
                        switch_select_menu.play()
                        select += 1
                    elif e.value == (0, 1):
                        switch_select_menu.play()
                        select -= 1
                    select %= len(txt_music) + 1
            elif e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if not isKey:
                    if e.button == scene_config.control_gamepad["A"]:
                        if select == len(txt_music):
                            isKey = True
                            ScreenFill(1, 10, (0, 0, 0), 50)
                        else:
                            scene_game.select_music = select
                            pygame.mixer.music.load(scene_game.music_pack[scene_game.select_music])
                            pygame.mixer.music.play()
                            pygame.mixer.music.set_volume(scene_config.music_volume)
                    if e.button == scene_config.control_gamepad["B"]:
                        running = False
                        scene_select_mode.scene_difficulty()
                        return 0
            if e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if not isKey:
                    if e.key == pygame.K_ESCAPE:
                        running = False
                        scene_select_mode.scene_difficulty()
                        return 0
                    if e.key == pygame.K_RETURN or e.key == pygame.K_e:
                        if select == len(txt_music):
                            isKey = True
                            ScreenFill(1, 10, (0, 0, 0), 50)
                        else:
                            scene_game.select_music = select
                            pygame.mixer.music.load(scene_game.music_pack[scene_game.select_music])
                            pygame.mixer.music.play()
                            pygame.mixer.music.set_volume(scene_config.music_volume)
                    if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                        select += 1
                        switch_select_menu.play()
                    if e.key == pygame.K_UP or e.key == pygame.K_w:
                        switch_select_menu.play()
                        select -= 1
                    select %= len(txt_music) + 1
        sc.fill((9, 3, 9))
        sc.blit(select_music_txt, select_music_txt.get_rect(center=(WINDOW_SIZE[0]/2, 45)))
        pygame.draw.rect(sc, (255, 0, 255), (0, 95, WINDOW_SIZE[0], 2))
        selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
        match select:
            case 0:
                yPosSelector = WINDOW_SIZE[1] / 2 - 162
                xPosSelector = WINDOW_SIZE[0] / 2
                selector_img = pygame.transform.scale(selector_img, (len(txt[0]) + 61 * 8, 18 * 5))
            case 1:
                yPosSelector = WINDOW_SIZE[1] / 2 - 107
                xPosSelector = WINDOW_SIZE[0] / 2
                selector_img = pygame.transform.scale(selector_img, (len(txt[1]) + 63 * 9, 18 * 5))
            case 2:
                yPosSelector = WINDOW_SIZE[1] / 2 - 52
                xPosSelector = WINDOW_SIZE[0] / 2
                selector_img = pygame.transform.scale(selector_img, (len(txt[2]) + 64 * 8, 18 * 5))
            case 3:
                yPosSelector = WINDOW_SIZE[1] / 2 + 2
                xPosSelector = WINDOW_SIZE[0] / 2
                selector_img = pygame.transform.scale(selector_img, (len(txt[3]) + 63 * 5, 18 * 5))
            case max_select:
                yPosSelector = 649
                xPosSelector = WINDOW_SIZE[0] / 2
                selector_img = pygame.transform.scale(selector_img, (64 * 4, 14 * 5))
        if isKey:
            music_volume -= 0.02
            pygame.mixer.music.set_volume(music_volume)
            if time_switch >= 45:
                running = False
                scene_select_mode.scene_show_control()
                return 0
            time_switch += 1
        particle_group.draw(sc)
        particle_group.update()
        sc.blit(txt_music[0], txt_music[0].get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 160)))
        sc.blit(txt_music[1], txt_music[1].get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 105)))
        sc.blit(txt_music[2], txt_music[2].get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 50)))
        sc.blit(txt_music[3], txt_music[3].get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 3)))
        sc.blit(txt_ok_k, txt_ok_k.get_rect(center=(WINDOW_SIZE[0] / 2, 648)))
        sc.blit(play_icon,
                play_icon.get_rect(center=(xPosPlay[scene_game.select_music], yPosPlay[scene_game.select_music])))
        sc.blit(selector_img, selector_img.get_rect(center=(xPosSelector, yPosSelector)))
        sc.blit(key_image["enter"], key_image["enter"].get_rect(center=(WINDOW_SIZE[0] - 110, 665)))
        sc.blit(txt_ok, txt_ok.get_rect(center=(WINDOW_SIZE[0] - 175, 668)))
        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)
        fill_screen.draw(sc)
        fill_screen.update()
        draw_notification()
        pygame.display.update()
        clock.tick(FPS)
