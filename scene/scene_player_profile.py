from random import randint

import pygame
from config import sc, clock, WINDOW_SIZE, standart_font, key_image, txt_ok, joystick_m, check_gamepad
from scene import scene_config, scene_menu, scene_start_new_game
import json
import shutil
import os
from player.weapon import Standart_weapon, MagicShotgun, MagicMinigun, MagicPistol, MagicTrident
from group_conf import particle_group, fill_screen
from particle import PhysicalParticle, BackParticle
from screen_fill import ScreenFill
from screenshoot import TakeScreenshot, draw_notification
import language_system
import gamepad_calibrate
from sfx_colection import switch_select_menu, player_type_txt, player_remove_txt, player_capslock_txt, \
    player_capslock_txt_off, player_create_character
from notfication import Notification
from VirtualKeyboard import VKEY

select_profile = None
select_profile_num = 0

txt_group = pygame.sprite.Group()


class text_p(pygame.sprite.Sprite):
    def __init__(self, txt, posY):
        super().__init__()
        self.image = pygame.font.Font(standart_font, 27).render(str(txt),
                                                                scene_config.AA_TEXT, (255, 255, 255))
        self.rect = self.image.get_rect(center=(WINDOW_SIZE[0] // 2, posY))
        self.add(txt_group)

    def update(self, event, isJoy=False):
        if not isJoy:
            if event.key == pygame.K_DOWN or event.key == pygame.K_s:
                self.rect.y -= 50
            elif event.key == pygame.K_UP or event.key == pygame.K_w:
                self.rect.y += 50
        else:
            if event.value == (0, -1):
                self.rect.y -= 50
            elif event.value == (0, 1):
                self.rect.y += 50


def scene():
    global select_profile, select_profile_num

    pygame.mixer.music.stop()

    for particle in particle_group:
        particle.kill()
    for i in range(randint(135, 160)):
        size = randint(10, 26)
        BackParticle(randint(2, WINDOW_SIZE[0] - 50), randint(15, 795),
                     scene_config.menu_particle_image[randint(0, len(scene_config.menu_particle_image) - 1)],
                     (size, size), 0, -randint(1, 2),
                     True, randint(10, 19))

    if not os.path.exists("Profile"):
        os.mkdir("Profile")
    running: bool = True
    FPS: int = 60

    txt_name_write = ""
    txt_age_write = ""

    selectMax = 0

    select = 0

    posTextY: int = WINDOW_SIZE[1] // 2 - 50

    len_res = 0

    if not os.listdir("Profile"):
        isWriteName: bool = True
    else:
        res = os.listdir("Profile")
        len_res = len(res)
        for lent in range(len_res):
            selectMax = lent
            posTextY += 50
            text_p(res[lent], posTextY)

        isWriteName: bool = False

    type_profile = 0

    please_select_profile_txt = pygame.font.Font(standart_font, 31).render(
        language_system.txt_lang["SELECT_PROFILE_TXT"],
        scene_config.AA_TEXT,
        (255, 155, 255))

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (64 * 5, 16 * 5))

    yPosSelector = WINDOW_SIZE[1] // 2

    size_txt_type = 0

    txt_create_character = pygame.font.Font(standart_font,
                                            34 - len(language_system.txt_lang["CREATE_CHARACTER"]) - 1).render(
        language_system.txt_lang["CREATE_CHARACTER"], scene_config.AA_TEXT, (225, 115, 225))

    txt_create_character_rect = txt_create_character.get_rect(center=(WINDOW_SIZE[0] // 2 + 170, WINDOW_SIZE[1] // 2 + 323))

    txt_create_character_rect.right = key_image["n"].get_rect(center=(WINDOW_SIZE[0] // 2 + 249, WINDOW_SIZE[1] // 2 + 320)).left - 4

    txt_delete = pygame.font.Font(standart_font, 33 - len(language_system.txt_lang["DELETE_CHARACTER"]) - 1).render(
        language_system.txt_lang["DELETE_CHARACTER"], scene_config.AA_TEXT, (255, 85, 95))

    txt_delete_rect = txt_delete.get_rect(center=(WINDOW_SIZE[0] // 2 + 373, WINDOW_SIZE[1] // 2 + 322))

    txt_delete_rect.right = key_image["backspace"].get_rect(center=(WINDOW_SIZE[0] // 2 + 505, WINDOW_SIZE[1] // 2 + 320)).left-7

    delete_particle_sprite = [pygame.image.load("Assets/sprite/particle/txt/a.png").convert_alpha(),
                              pygame.image.load("Assets/sprite/particle/txt/b.png").convert_alpha(),
                              pygame.image.load("Assets/sprite/particle/txt/c.png").convert_alpha(),
                              pygame.image.load("Assets/sprite/particle/txt/d.png").convert_alpha(),
                              pygame.image.load("Assets/sprite/particle/txt/e.png").convert_alpha()]

    txt_name_alpha = 300
    yPosTXT_Write = WINDOW_SIZE[1] // 2

    isCaps = pygame.key.get_mods() & pygame.KMOD_CAPS

    sizeCaps = [18, 17]

    isStart = False
    time_start = 0

    key_v = VKEY(40, standart_font, sc, WINDOW_SIZE[0]/3-64, WINDOW_SIZE[1]/2+95, joystick_m)

    if isWriteName and pygame.joystick.get_count() > 0:
        isGamepadTap = True
    else:
        isGamepadTap = False

    txt_back_write = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["BACK"], scene_config.AA_TEXT, (225, 115, 225))

    txt_back_rect = txt_back_write.get_rect(center=(90, WINDOW_SIZE[1] // 2 + 322))
    txt_back_rect.left = key_image["esc"].get_rect(center=(39, WINDOW_SIZE[1] // 2 + 320)).right+8

    pygame.mixer.music.load("Assets/ost/Select new magical girl.mp3")
    pygame.mixer.music.play(-1)
    pygame.mixer.music.set_volume(scene_config.music_volume)

    volume_music_one = scene_config.music_volume

    while running:
        sc.fill((15, 5, 15))
        particle_group.draw(sc)
        particle_group.update()
        if isWriteName:
            len_prof = os.listdir("Profile")
            if len(len_prof) > 0:
                sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, WINDOW_SIZE[1] // 2 + 320)))
                sc.blit(txt_back_write, txt_back_rect)
            if txt_name_alpha < 300:
                txt_name_alpha += 5
            if size_txt_type < 27:
                size_txt_type += 2
            if type_profile == 0:
                please_type_name_txt = pygame.font.Font(standart_font, size_txt_type).render(
                    language_system.txt_lang["PLEASE_TYPE_NAME"],
                    scene_config.AA_TEXT, (255, 155, 255))
                sc.blit(please_type_name_txt, please_type_name_txt.get_rect(center=(WINDOW_SIZE[0] // 2, 290)))
                txt_name = pygame.font.Font(standart_font, size_txt_type - 5).render(txt_name_write + "|",
                                                                                     scene_config.AA_TEXT,
                                                                                     (255, 255, 255))
                txt_name.set_alpha(txt_name_alpha)
            else:
                please_type_age_txt = pygame.font.Font(standart_font, size_txt_type-2).render(
                    language_system.txt_lang["PLEASE_TYPE_AGE"],
                    scene_config.AA_TEXT, (255, 195, 255))
                sc.blit(please_type_age_txt,
                        please_type_age_txt.get_rect(center=(WINDOW_SIZE[0] // 2, 290)))
                txt_name = pygame.font.Font(standart_font, size_txt_type - 5).render(txt_age_write + "|",
                                                                                     scene_config.AA_TEXT,
                                                                                     (255, 255, 255))
                txt_name.set_alpha(txt_name_alpha)
            sc.blit(txt_name, txt_name.get_rect(center=(WINDOW_SIZE[0] // 2, yPosTXT_Write)))
            if isCaps:
                capslock_key = key_image["capslock"]
                sizeCaps[0] += 3
                sizeCaps[1] += 3
                sizeCaps[0] = max(0, min(31, sizeCaps[0]))
                sizeCaps[1] = max(0, min(30, sizeCaps[1]))
                capslock_key = pygame.transform.scale(capslock_key, sizeCaps)
                sc.blit(capslock_key, capslock_key.get_rect(center=(WINDOW_SIZE[0] / 2, yPosTXT_Write + 50)))
            else:
                sizeCaps[0] = 18
                sizeCaps[1] = 17
            sc.blit(key_image["enter"],
                    key_image["enter"].get_rect(center=(WINDOW_SIZE[0] // 2 + 500, WINDOW_SIZE[1] // 2 + 320)))
            sc.blit(txt_ok, txt_ok.get_rect(center=(WINDOW_SIZE[0] // 2 + 435, WINDOW_SIZE[1] // 2 + 323)))
        else:
            txt_group.draw(sc)
            pygame.draw.rect(sc, (15, 5, 15), (0, 0, WINDOW_SIZE[0], 110))
            pygame.draw.rect(sc, (195, 70, 195), (0, 110, WINDOW_SIZE[0], 2))
            sc.blit(please_select_profile_txt, please_select_profile_txt.get_rect(center=(WINDOW_SIZE[0] // 2, 56)))
            sc.blit(selector_img, selector_img.get_rect(center=(WINDOW_SIZE[0] // 2, yPosSelector)))
            txt_delete_rect.right = key_image["backspace"].get_rect(center=(WINDOW_SIZE[0] // 2 + 505, WINDOW_SIZE[1] // 2 + 320)).left-6
            sc.blit(key_image["backspace"],
                    key_image["backspace"].get_rect(center=(WINDOW_SIZE[0] // 2 + 507, WINDOW_SIZE[1] // 2 + 320)))
            sc.blit(txt_delete, txt_delete_rect)

            sc.blit(key_image["n"],
                    key_image["n"].get_rect(center=(WINDOW_SIZE[0] // 2 + 249, WINDOW_SIZE[1] // 2 + 320)))
            sc.blit(txt_create_character,
                    txt_create_character_rect)

            sc.blit(key_image["enter"],
                    key_image["enter"].get_rect(center=(WINDOW_SIZE[0] // 2 - 29, WINDOW_SIZE[1] // 2 + 320)))
            sc.blit(txt_ok, txt_ok.get_rect(center=(WINDOW_SIZE[0] // 2 - 98, WINDOW_SIZE[1] // 2 + 323)))
        for e in pygame.event.get():
            if isGamepadTap:
                key_v.mouse_gamepad_check(e)
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYHATMOTION and not isWriteName:
                check_gamepad(gamepad_calibrate.input_sys)
                if select < selectMax:
                    if e.value == (0, -1):
                        switch_select_menu.play()
                        select += 1
                        txt_group.update(e, True)
                if select > 0:
                    if e.value == (0, 1):
                        switch_select_menu.play()
                        select -= 1
                        txt_group.update(e, True)
            if e.type == pygame.JOYBUTTONDOWN and not isStart:
                check_gamepad(gamepad_calibrate.input_sys)
                if not isWriteName:
                    if e.button == scene_config.control_gamepad["A"]:
                        scene_config.p.weapon.clear()
                        profile_file = os.listdir("Profile")
                        with open("Profile/" + profile_file[select] + "/data.json") as file:
                            select_profile = json.load(file)
                        select_profile_num = select

                        weapon_file = os.listdir("Profile/" + profile_file[select] + "/Weapon")

                        for b, weap in enumerate(weapon_file):
                            weapon_load = None
                            with open("Profile/" + profile_file[select] + "/Weapon/"+str(b)+".json") as file_w:
                                weapon_load = json.load(file_w)
                            if weapon_load["NAME"] == "st_weapon":
                                scene_config.p.weapon.append(Standart_weapon(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            elif weapon_load["NAME"] == "pistol":
                                scene_config.p.weapon.append(MagicPistol(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            elif weapon_load["NAME"] == "trident":
                                scene_config.p.weapon.append(MagicTrident(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            elif weapon_load["NAME"] == "minigun":
                                scene_config.p.weapon.append(MagicMinigun(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            elif weapon_load["NAME"] == "shotgun":
                                scene_config.p.weapon.append(MagicShotgun(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            

                        scene_config.p.HISCORE = select_profile["SCORE"]
                        scene_config.p.MONEY = select_profile["MONEY"]

                        scene_config.p.isFirstRun = select_profile["FIRST_RUN"]

                        scene_config.p.NAME = select_profile["NAME"]
                        scene_config.p.AGE = select_profile["AGE"]

                        scene_config.p.EXP = select_profile["EXP"]
                        scene_config.p.RANK = select_profile["RANK"]

                        scene_config.p.paper_story = select_profile["PAPPER"]

                        scene_config.p.profile = select

                        scene_config.time_played = select_profile["TIME_PLAYED"]

                        pygame.time.set_timer(pygame.USEREVENT, 60000)

                        isStart = True

                        ScreenFill(1, 10, (0, 0, 0), 35)

                        if scene_config.p.NAME == "BodaMat" or scene_config.p.NAME == "ihrovary" or scene_config.p.NAME == "Ihrovary" or scene_config.p.NAME == "bodamat":
                            scene_config.p.image = pygame.image.load("Assets/sprite/player/2.png").convert_alpha()
                            scene_config.p.image = pygame.transform.scale(scene_config.p.image, (17, 24))
                            scene_config.p.egg = 1
                        else:
                            scene_config.p.egg = 0
                    if e.button == scene_config.control_gamepad["Y"]:
                        isWriteName = True
                        isGamepadTap = True
                    elif e.button == scene_config.control_gamepad["X"] and not isStart:
                        profile_file = os.listdir("Profile")
                        shutil.rmtree("Profile/" + profile_file[select])
                        size_txt_type = 0

                        posTextY: int = WINDOW_SIZE[1] // 2 - 50

                        for txt_g in txt_group:
                            txt_g.kill()

                        if not os.listdir("Profile"):
                            isGamepadTap = True
                            isWriteName = True
                        else:
                            for i in range(randint(19, 26)):
                                size = randint(4, 8)
                                PhysicalParticle(WINDOW_SIZE[0] // 2 + randint(-75, 75),
                                                    yPosSelector - 25 - randint(1, 25),
                                                    -randint(9, 17),
                                                    delete_particle_sprite[randint(0, len(delete_particle_sprite) - 1)],
                                                    (size, size + 2))
                            res = os.listdir("Profile")
                            len_res = len(res)
                            for lent in range(len_res):
                                selectMax = lent
                                posTextY += 50
                                text_p(res[lent], posTextY)
                            select = 0
                else:
                    if e.button == scene_config.control_gamepad["B"]:   
                        len_txt = os.listdir("Profile")
                        if len(len_txt) > 0:
                            isGamepadTap = False
                            txt_age_write = ""
                            txt_name_write = ""
                            isWriteName = False
                            type_profile = 0
                            size_txt_type = 0
                    if type_profile == 0:
                        if e.button == scene_config.control_gamepad["A"]:
                            size_txt_type = 0
                            type_profile = 1
                    elif type_profile == 1:
                        if e.button == scene_config.control_gamepad["A"]:
                            isGamepadTap = False
                            isWriteName = False
                            size_txt_type = 0
                            try:
                                if txt_age_write == "":
                                    txt_age_write = str(randint(14, 17))
                                create_profile(txt_name_write, txt_age_write)
                                txt_name_write = ""
                                txt_age_write = ""
                                type_profile = 0

                                posTextY: int = WINDOW_SIZE[1] // 2 - 50

                                for txt_g in txt_group:
                                    txt_g.kill()

                                res = os.listdir("Profile")
                                len_res = len(res)
                                for lent in range(len_res):
                                    selectMax = lent
                                    posTextY += 50
                                    text_p(res[lent], posTextY)
                                player_create_character.play()
                            except FileExistsError:
                                Notification(language_system.txt_lang["ERROR"], 1)
                                isGamepadTap = True
                                isWriteName = True
                                txt_name_write = ""
                                txt_age_write = ""
                                type_profile = 0
            if isGamepadTap:
                if e.type == pygame.MOUSEBUTTONDOWN:
                    if e.button == 1 and type_profile == 0 and key_v.check_tap(txt_name_write):
                        if key_v.obj_draw[key_v.check_tap(txt_name_write)].key != "<-":
                                txt_name_write += key_v.obj_draw[key_v.check_tap(txt_name_write)].key
                                player_type_txt.play()
                        elif key_v.obj_draw[key_v.check_tap(txt_name_write)].key == "<-":
                                player_remove_txt.play()
                                txt_name_write = txt_name_write[:-1]
                    elif e.button == 1 and type_profile == 1 and key_v.check_tap(txt_age_write):
                        if key_v.obj_draw[key_v.check_tap(txt_age_write)].key != "<-":
                                if key_v.obj_draw[key_v.check_tap(txt_age_write)].key.isdigit():
                                    txt_age_write += key_v.obj_draw[key_v.check_tap(txt_age_write)].key
                                    player_type_txt.play()
                        elif key_v.obj_draw[key_v.check_tap(txt_age_write)].key == "<-":
                                player_remove_txt.play()
                                txt_age_write = txt_age_write[:-1]
                if e.type == pygame.JOYBUTTONDOWN:
                    if e.button == scene_config.control_gamepad["R_BUMPER"] and type_profile == 0 and key_v.check_tap(txt_name_write):
                        if key_v.obj_draw[key_v.check_tap(txt_name_write)].key != "<-":
                                txt_name_write += key_v.obj_draw[key_v.check_tap(txt_name_write)].key
                                player_type_txt.play()
                        elif key_v.obj_draw[key_v.check_tap(txt_name_write)].key == "<-":
                                player_remove_txt.play()
                                txt_name_write = txt_name_write[:-1]
                    elif e.button == scene_config.control_gamepad["R_BUMPER"] and type_profile == 1 and key_v.check_tap(txt_age_write):
                        if key_v.obj_draw[key_v.check_tap(txt_age_write)].key != "<-":
                                if key_v.obj_draw[key_v.check_tap(txt_age_write)].key.isdigit():
                                    txt_age_write += key_v.obj_draw[key_v.check_tap(txt_age_write)].key
                                    player_type_txt.play()
                        elif key_v.obj_draw[key_v.check_tap(txt_age_write)].key == "<-":
                                player_remove_txt.play()
                                txt_age_write = txt_age_write[:-1]

            if e.type == pygame.KEYDOWN and not isStart:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if isWriteName:
                    if e.key == pygame.K_ESCAPE:
                        len_txt = os.listdir("Profile")
                        if len(len_txt) > 0:
                            txt_age_write = ""
                            txt_name_write = ""
                            isWriteName = False
                            type_profile = 0
                            size_txt_type = 0
                    if type_profile == 0:
                        if e.key == pygame.K_RETURN or e.key == pygame.K_KP_ENTER:
                            size_txt_type = 0
                            type_profile = 1
                        elif e.key == pygame.K_BACKSPACE:
                            player_remove_txt.play()
                            txt_name_write = txt_name_write[:-1]
                            txt_name_alpha = 100
                        else:
                            if e.key != pygame.K_ESCAPE and e.key != pygame.K_TAB:
                                if e.key != pygame.K_CAPSLOCK:
                                    player_type_txt.play()
                                    txt_name_alpha = 150
                                else:
                                    isCaps = not isCaps
                                    if not isCaps:
                                        player_capslock_txt_off.play()
                                    else:
                                        player_capslock_txt.play()
                                txt_name_write += e.unicode
                    elif type_profile == 1:
                        if e.key == pygame.K_RETURN or e.key == pygame.K_KP_ENTER:
                            isGamepadTap = False
                            isWriteName = False
                            size_txt_type = 0
                            try:
                                if txt_age_write == "":
                                    txt_age_write = str(randint(14, 17))
                                create_profile(txt_name_write, txt_age_write)
                                txt_name_write = ""
                                txt_age_write = ""
                                type_profile = 0

                                posTextY: int = WINDOW_SIZE[1] // 2 - 50

                                for txt_g in txt_group:
                                    txt_g.kill()

                                res = os.listdir("Profile")
                                len_res = len(res)
                                for lent in range(len_res):
                                    selectMax = lent
                                    posTextY += 50
                                    text_p(res[lent], posTextY)
                                player_create_character.play()
                            except FileExistsError:
                                Notification(language_system.txt_lang["ERROR"], 1)
                                isWriteName = True
                                txt_name_write = ""
                                txt_age_write = ""
                                type_profile = 0

                        if e.key == pygame.K_BACKSPACE:
                            player_remove_txt.play()
                            txt_age_write = txt_age_write[:-1]
                            txt_name_alpha = 100
                        else:
                            if e.unicode.isdigit():
                                player_type_txt.play()
                                txt_age_write += e.unicode
                                txt_name_alpha = 150
                else:
                    if e.key == pygame.K_RETURN:
                        scene_config.p.weapon.clear()
                        profile_file = os.listdir("Profile")
                        with open("Profile/" + profile_file[select] + "/data.json") as file:
                            select_profile = json.load(file)
                        select_profile_num = select

                        weapon_file = os.listdir("Profile/" + profile_file[select] + "/Weapon")

                        for b, weap in enumerate(weapon_file):
                            weapon_load = None
                            with open("Profile/" + profile_file[select] + "/Weapon/"+str(b)+".json") as file_w:
                                weapon_load = json.load(file_w)
                            if weapon_load["NAME"] == "st_weapon":
                                scene_config.p.weapon.append(Standart_weapon(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            elif weapon_load["NAME"] == "pistol":
                                scene_config.p.weapon.append(MagicPistol(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            elif weapon_load["NAME"] == "trident":
                                scene_config.p.weapon.append(MagicTrident(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            elif weapon_load["NAME"] == "minigun":
                                scene_config.p.weapon.append(MagicMinigun(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            elif weapon_load["NAME"] == "shotgun":
                                scene_config.p.weapon.append(MagicShotgun(weapon_load["isAim"], weapon_load["isStab"], weapon_load["isBomb"], weapon_load["isOn"]))
                            

                        scene_config.p.HISCORE = select_profile["SCORE"]
                        scene_config.p.MONEY = select_profile["MONEY"]

                        scene_config.p.isFirstRun = select_profile["FIRST_RUN"]

                        scene_config.p.NAME = select_profile["NAME"]
                        scene_config.p.AGE = select_profile["AGE"]

                        scene_config.p.EXP = select_profile["EXP"]
                        scene_config.p.RANK = select_profile["RANK"]

                        scene_config.p.paper_story = select_profile["PAPPER"]

                        scene_config.p.profile = select

                        scene_config.time_played = select_profile["TIME_PLAYED"]

                        pygame.time.set_timer(pygame.USEREVENT, 60000)

                        isStart = True

                        ScreenFill(1, 10, (0, 0, 0), 35)

                        if scene_config.p.NAME == "BodaMat" or scene_config.p.NAME == "ihrovary" or scene_config.p.NAME == "Ihrovary" or scene_config.p.NAME == "bodamat":
                            scene_config.p.image = pygame.image.load("Assets/sprite/player/2.png").convert_alpha()
                            scene_config.p.image = pygame.transform.scale(scene_config.p.image, (17, 24))
                            scene_config.p.egg = 1
                        else:
                            scene_config.p.egg = 0

                    if not isStart:
                        if e.key == pygame.K_BACKSPACE:
                            profile_file = os.listdir("Profile")
                            shutil.rmtree("Profile/" + profile_file[select])
                            size_txt_type = 0

                            posTextY: int = WINDOW_SIZE[1] // 2 - 50

                            for txt_g in txt_group:
                                txt_g.kill()

                            if not os.listdir("Profile"):
                                isWriteName = True
                            else:
                                for i in range(randint(19, 26)):
                                    size = randint(4, 8)
                                    PhysicalParticle(WINDOW_SIZE[0] // 2 + randint(-75, 75),
                                                    yPosSelector - 25 - randint(1, 25),
                                                    -randint(9, 17),
                                                    delete_particle_sprite[randint(0, len(delete_particle_sprite) - 1)],
                                                    (size, size + 2))
                                res = os.listdir("Profile")
                                len_res = len(res)
                                for lent in range(len_res):
                                    selectMax = lent
                                    posTextY += 50
                                    text_p(res[lent], posTextY)
                                select = 0
                        elif e.key == pygame.K_n:
                            isWriteName = True
                        if select < selectMax:
                            if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                                switch_select_menu.play()
                                select += 1
                                txt_group.update(e)
                        if select > 0:
                            if e.key == pygame.K_UP or e.key == pygame.K_w:
                                switch_select_menu.play()
                                select -= 1
                                txt_group.update(e)

        if isStart:
            if time_start >= 35:
                running = False
                for txt_g in txt_group:
                    txt_g.kill()
                if select_profile["FIRST_RUN"]:
                    scene_start_new_game.scene_start_dialog()
                else:
                    scene_menu.scene_press()
                return 0
            else:
                pygame.mixer.music.set_volume(volume_music_one)
                volume_music_one -= 0.01
            time_start += 1
        elif isGamepadTap:
            key_v.update()

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)


def create_profile(name, age):
    os.mkdir("Profile/" + str(name))

    with open("Profile/" + str(name) + "/Warning.txt", "w") as txt:
        txt.write("Please, dont open, and edit this file. If you can make this, you bad player!")

    data = {
        "NAME": str(name),
        "AGE": int(age),

        "SCORE": 0,
        "MONEY": 0,
        "EXP": 0,
        "RANK": 0,
        "TIME_PLAYED": 0,

        "FIRST_RUN": True,

        "PAPPER": ["paper_0"]
    }

    with open("Profile/" + str(name) + "/data.json", "w") as f:
        json.dump(data, f)
    
    if not os.path.exists("Profile/" + str(name) + "/Weapon"):
        os.mkdir("Profile/" + str(name) + "/Weapon")

    data_weapon = {
        "NAME": "st_weapon",
        "isAim": False,
        "isStab": False,
        "isBomb": False,
        "isOn": True
    }

    with open("Profile/" + str(name) + "/Weapon/0.json", "w") as d:
        json.dump(data_weapon, d)
