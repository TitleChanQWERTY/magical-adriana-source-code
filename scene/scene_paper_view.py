import pygame
from config import key_image, sc, clock, standart_font, check_gamepad, WINDOW_SIZE
from group_conf import particle_group
from screenshoot import draw_notification, TakeScreenshot
from scene import scene_config, scene_extra
import gamepad_calibrate
from sfx_colection import switch_select_menu
import language_system


def sceme():
    running = True
    FPS = 60

    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["MAIN_MENU"],
                                                          scene_config.AA_TEXT,
                                                          (255, 95, 255))
    txt_exit_rect = txt_exit.get_rect(center=(103, 667))

    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8

    txt_draw_surf = pygame.Surface((WINDOW_SIZE[0]/2+60, WINDOW_SIZE[1]-85))

    
    subtitles_txt = []

    for i, b in enumerate(scene_config.p.paper_story):
        subtitles_txt.append(language_system.txt_lang["paper_"+str(i)])

    select = 0

    index_txt = 0

    time_index = 0

    font = pygame.font.Font(standart_font, 22)
    text_lines = []

    words = subtitles_txt[0].split(" ")
    current_line = words[0]

    for word in words[1:]:
        rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
        if rendered_word.get_width() < txt_draw_surf.get_width() - 7:
            current_line += " " + word
        else:
            text_lines.append(current_line)
            current_line = word
    text_lines.append(current_line)

    yPosTXT = 45

    yPosSelect = 100

    select_max = len(scene_config.p.paper_story)

    draw_txt = []

    for b, p in enumerate(scene_config.p.paper_story):
        txt_select = pygame.font.Font(standart_font, 26).render(language_system.txt_lang["DOCUMENT_TXT"]+" "+str(b), scene_config.AA_TEXT, (255, 255, 255))
        draw_txt.append(txt_select)

    rect_txt = []

    for dt in draw_txt:
        txt_select_rect = dt.get_rect(center=(205, yPosSelect))
        rect_txt.append(txt_select_rect)
        yPosSelect += 50

    
    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (60 * 4, 15 * 4))
    yPosSelector = 100

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYHATMOTION:
                check_gamepad(gamepad_calibrate.input_sys)
                if len(scene_config.p.paper_story) > 1:
                    if e.value == (0, -1):
                        switch_select_menu.play()
                        select += 1
                        select %= select_max
                            
                        index_txt = 0

                        time_index = 0

                        font = pygame.font.Font(standart_font, 22)
                        text_lines = []

                        words = subtitles_txt[select].split(" ")
                        current_line = words[0]

                        for word in words[1:]:
                            rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
                            if rendered_word.get_width() < txt_draw_surf.get_width() - 7:
                                current_line += " " + word
                            else:
                                text_lines.append(current_line)
                                current_line = word
                        text_lines.append(current_line)
                        yPosSelector = rect_txt[select].centery
                    elif e.value == (0, 1):
                        switch_select_menu.play()
                        select -= 1
                        select %= select_max
                        yPosSelector = rect_txt[select].centery
                        index_txt = 0

                        time_index = 0

                        font = pygame.font.Font(standart_font, 22)
                        text_lines = []

                        words = subtitles_txt[select].split(" ")
                        current_line = words[0]

                        for word in words[1:]:
                            rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
                            if rendered_word.get_width() < txt_draw_surf.get_width() - 7:
                                current_line += " " + word
                            else:
                                text_lines.append(current_line)
                                current_line = word
                        text_lines.append(current_line)
                        yPosSelector = rect_txt[select].centery
            if e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.button == scene_config.control_gamepad["B"]:
                    running = False
                    scene_extra.scene()
                    return 0
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                elif e.key == pygame.K_ESCAPE:
                    running = False
                    scene_extra.scene()
                    return 0
                elif len(scene_config.p.paper_story) > 1:
                    if e.key == pygame.K_DOWN:
                        switch_select_menu.play()
                        select += 1
                        select %= select_max
                        
                        index_txt = 0

                        time_index = 0

                        font = pygame.font.Font(standart_font, 20)
                        text_lines = []

                        words = subtitles_txt[select].split(" ")
                        current_line = words[0]

                        for word in words[1:]:
                            rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
                            if rendered_word.get_width() < txt_draw_surf.get_width() - 5:
                                current_line += " " + word
                            else:
                                text_lines.append(current_line)
                                current_line = word
                        text_lines.append(current_line)
                        yPosSelector = rect_txt[select].centery
                    elif e.key == pygame.K_UP:
                        switch_select_menu.play()
                        select -= 1
                        select %= select_max
                        yPosSelector = rect_txt[select].centery
                        index_txt = 0

                        time_index = 0

                        font = pygame.font.Font(standart_font, 20)
                        text_lines = []

                        words = subtitles_txt[select].split(" ")
                        current_line = words[0]

                        for word in words[1:]:
                            rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
                            if rendered_word.get_width() < txt_draw_surf.get_width() - 5:
                                current_line += " " + word
                            else:
                                text_lines.append(current_line)
                                current_line = word
                        text_lines.append(current_line)
                        yPosSelector = rect_txt[select].centery
                
        
        sc.fill((9, 3, 9))
        particle_group.draw(sc)
        particle_group.update()
        
        txt_draw_surf.fill((0, 0, 0))
        pygame.draw.rect(txt_draw_surf, (200, 200, 200), (0, 0, txt_draw_surf.get_width(), txt_draw_surf.get_height()), 2)

        for i, line in enumerate(text_lines):
            rendered_line = font.render(line[:index_txt], scene_config.AA_TEXT, (255, 255, 255))
            if time_index > 1:
                if index_txt < len(line):
                    index_txt += 1
                    time_index = 0
            else:
                time_index += 1
            txt_draw_surf.blit(rendered_line, rendered_line.get_rect(
                center=(txt_draw_surf.get_width()/2, yPosTXT)))
            yPosTXT += 22
        yPosTXT = 45

        for draw, t in enumerate(draw_txt):
            sc.blit(draw_txt[draw], rect_txt[draw])

        sc.blit(txt_draw_surf, txt_draw_surf.get_rect(center=(WINDOW_SIZE[0]/2+210, WINDOW_SIZE[1]/2-5)))

        sc.blit(selector_img, selector_img.get_rect(center=(rect_txt[0].centerx, yPosSelector)))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)
        
        draw_notification()
        pygame.display.update()
        clock.tick(FPS)

