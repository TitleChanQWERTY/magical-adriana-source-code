import pygame
from config import sc, clock, WINDOW_SIZE, standart_font, key_image, check_gamepad
from scene import scene_config, scene_player_profile
import gamepad_calibrate
from screenshoot import draw_notification, TakeScreenshot
import language_system

class AnimCharacter:
    def __init__(self):
        self.frame = 0
        self.image = [pygame.image.load("Assets/sprite/scene_moment/start/1.png").convert_alpha(),
                      pygame.image.load("Assets/sprite/scene_moment/start/2.png").convert_alpha(),
                      pygame.image.load("Assets/sprite/scene_moment/start/3.png").convert_alpha()]
        self.image[0] = pygame.transform.scale(self.image[0], (9 * 7, 9 * 7))
        self.image[1] = pygame.transform.scale(self.image[1], (5 * 7, 6 * 7))
        self.image[2] = pygame.transform.scale(self.image[2], (7 * 9, 8 * 9))

        for i in range(3):
            self.image[i].set_alpha(0)

        self.rect = [self.image[0].get_rect(center=(WINDOW_SIZE[0]/2-75, WINDOW_SIZE[1]/2)), 
        self.image[1].get_rect(center=(WINDOW_SIZE[0]/2+75, WINDOW_SIZE[1]/2+10)),
        self.image[2].get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2))]

        self.frame_hat = 0

        self.frame_hand = 0

        self.txt_dialog = (language_system.txt_lang["START_CUTSCENE"], 
        language_system.txt_lang["START_CUTSCENE_2"],
        language_system.txt_lang["START_CUTSCENE_3"],
        language_system.txt_lang["START_CUTSCENE_4"],
        language_system.txt_lang["START_CUTSCENE_5"],
        language_system.txt_lang["START_CUTSCENE_6"],
        language_system.txt_lang["START_CUTSCENE_7"],
        language_system.txt_lang["START_CUTSCENE_8"],
        language_system.txt_lang["START_CUTSCENE_9"],
        language_system.txt_lang["START_CUTSCENE_10"],
        language_system.txt_lang["START_CUTSCENE_11"],
        language_system.txt_lang["START_CUTSCENE_12"],
        language_system.txt_lang["START_CUTSCENE_13"],
        language_system.txt_lang["START_CUTSCENE_14"],
        language_system.txt_lang["START_CUTSCENE_15"],
        language_system.txt_lang["START_CUTSCENE_16"],
        language_system.txt_lang["START_CUTSCENE_17"])

        self.select_txt = 0

        self.time_next_txt = 0

        self.draw_text = 0

        self.alpha = 0

    def dialog(self):
        if self.select_txt < len(self.txt_dialog):
            time_txt = 287 + len(self.txt_dialog[self.select_txt]) + 1
            if self.draw_text <= len(self.txt_dialog[self.select_txt]):
                self.draw_text += 1
            elif self.time_next_txt >= time_txt:
                self.draw_text = 0
                self.select_txt += 1
                self.time_next_txt = 0
                return
            txt_draw = pygame.font.Font(standart_font, 17).render(self.txt_dialog[self.select_txt][:self.draw_text], scene_config.AA_TEXT, (255, 255, 255))
            sc.blit(txt_draw, txt_draw.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2+50)))
            self.time_next_txt += 1


    def update(self):
        self.alpha += 3
        self.image[0].set_alpha(self.alpha)
        self.image[1].set_alpha(self.alpha)
        self.image[2].set_alpha(self.alpha)
        if self.frame <= 335:          
            if self.frame >= 155 and self.frame <= 200:
                self.rect[2].centery -= 5
            if self.frame >= 170 and self.frame <= 190:
                self.rect[0].centery -= 6
                self.rect[1].centery -= 5

                self.rect[0].centerx += 1
                self.rect[1].centerx -= 1
            self.frame += 1
        else:
            self.dialog()
        
        if self.frame >= 175:
            if self.frame_hat <= 11:
                self.rect[2].centery += 2
            elif self.frame_hat >= 18:
                self.rect[2].centery -= 2
            if self.frame_hat >= 28:
                self.frame_hat = 0
            self.frame_hat += 1
        if self.frame >= 190:
            if self.frame_hand <= 11:
                self.rect[1].centery += 1
                self.rect[0].centery += 1

                self.rect[0].centerx += 1
                self.rect[1].centerx -= 1

            elif self.frame_hand >= 18:
                self.rect[0].centery -= 1
                self.rect[1].centery -= 1

                self.rect[0].centerx -= 1
                self.rect[1].centerx += 1
            if self.frame_hand >= 28:
                self.frame_hand = 0
            self.frame_hand += 1
            

def scene():
    running = True
    FPS = 60

    anim = AnimCharacter()

    time_skip = 0

    rect_space = key_image["space"].get_rect(center=(WINDOW_SIZE[0]-100, WINDOW_SIZE[1]-35))

    txt_skip = pygame.font.Font(standart_font, 15).render(language_system.txt_lang["TXT_SKIP"], scene_config.AA_TEXT, (255, 255, 255))
    txt_skip_rect = txt_skip.get_rect(center=(rect_space.left-6, rect_space.centery-1))
    txt_skip_rect.right = rect_space.left-5

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYBUTTONDOWN and time_skip >= 53:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.button == scene_config.control_gamepad["A"]:
                    running = False
                    scene_next()
                    return 0
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                elif time_skip >= 53 and e.key == pygame.K_SPACE:
                    running = False
                    scene_next()
                    return 0
        
        sc.fill((0, 0, 0))
        if time_skip >= 55:
            sc.blit(key_image["space"], rect_space)
            sc.blit(txt_skip, txt_skip_rect)
        else:
            time_skip += 1
        for i in range(3):
            sc.blit(anim.image[i], anim.rect[i])
        anim.update()
        if anim.select_txt >= len(anim.txt_dialog):
            running = False
            scene_next()
            return 0

        draw_notification()
        
        pygame.display.update()
        clock.tick(FPS)

def scene_next():
    running = True
    FPS = 60

    time_tnext = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        
        sc.fill((0, 0, 0))
        if time_tnext >= 70:
            running = False
            scene_player_profile.scene()
            return 0
        time_tnext += 1

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
