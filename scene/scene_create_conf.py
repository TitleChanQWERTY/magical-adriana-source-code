import os

import pygame
import config
from group_conf import fill_screen
from scene import scene_config, scene_option, scene_player_profile, scene_start
import gamepad_calibrate
from screen_fill import ScreenFill
from screenshoot import draw_notification, TakeScreenshot
from save_progres import save_config
import language_system


def start_message():
    running = True
    FPS = 60

    size_txt_start = 0

    time_start = 0

    frame_anim_next = 0

    yPosNext = 685

    next_image = pygame.image.load("Assets/sprite/ui/txt_dialog/next.png").convert_alpha()
    next_image = pygame.transform.scale(next_image, (7 * 4, 5 * 4))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.JOYBUTTONDOWN:
                config.check_gamepad(gamepad_calibrate.input_sys)
                running = False
                select_language_conf()
                return 0
            elif e.type == pygame.KEYDOWN:
                config.check_gamepad(gamepad_calibrate.input_sys, False)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                else:
                    running = False
                    select_language_conf()
                    return 0
        if time_start >= 55:
            config.sc.fill((0, 0, 0))
            size_txt_start += 3
            size_txt_start = max(1, min(31, size_txt_start))
            txt_start = pygame.font.Font(config.standart_font, size_txt_start).render("Let's config your game", False,
                                                                                      (255, 0, 255))
            config.sc.blit(txt_start,
                           txt_start.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 1)))

            if size_txt_start >= 31:
                if frame_anim_next == 0:
                    yPosNext = config.WINDOW_SIZE[1] / 2 + 85
                if frame_anim_next < 25:
                    yPosNext -= 1
                    frame_anim_next += 1
                else:
                    frame_anim_next = 0
                config.sc.blit(next_image, next_image.get_rect(center=(config.WINDOW_SIZE[0] / 2, yPosNext)))

            draw_notification()
            pygame.display.update()
            config.clock.tick(FPS)
        else:
            time_start += 1


def select_language_conf():
    running = True
    FPS: int = 60

    size_txt = [0, 0, 0]

    select = 0

    LANG = os.listdir("Language (Dont Remove)")

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (55 * 5, 15 * 5))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.JOYBUTTONDOWN:
                if e.button == scene_config.control_gamepad["A"]:
                    if select == 1:
                        running = False
                        scene_fullscreen()
                        return 0
            elif e.type == pygame.JOYHATMOTION:
                if e.value == (0, -1):
                    select += 1
                elif e.value == (0, 1):
                    select -= 1
                if select == 0:
                    if e.value == (-1, 0):
                        language_system.select_lang -= 1
                        language_system.select_lang %= len(LANG)
                        language_system.set_language()
                        running = False
                        select_language_conf()
                        return 0
                    elif e.value == (1, 0):
                        language_system.select_lang += 1
                        language_system.select_lang %= len(LANG)
                        language_system.set_language()
                        running = False
                        select_language_conf()
                        return 0
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if e.key == pygame.K_RETURN:
                    if select == 1:
                        running = False
                        scene_fullscreen()
                        return 0
                if select == 0:
                    if e.key == pygame.K_LEFT:
                        language_system.select_lang -= 1
                        language_system.select_lang %= len(LANG)
                        language_system.set_language()
                        running = False
                        select_language_conf()
                        return 0
                    elif e.key == pygame.K_RIGHT:
                        language_system.select_lang += 1
                        language_system.select_lang %= len(LANG)
                        language_system.set_language()
                        running = False
                        select_language_conf()
                        return 0
                        
                if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                    select += 1
                if e.key == pygame.K_UP or e.key == pygame.K_w:
                    select -= 1
            select %= 2

        config.sc.fill((0, 0, 0))

        txt_language = pygame.font.Font(config.standart_font, size_txt[1]).render(
            language_system.txt_lang["LANGUAGE_NAME"], False, (255, 255, 255))

        rect_left = config.key_image["arrow_left"].get_rect(center=(config.WINDOW_SIZE[0]/2, config.WINDOW_SIZE[1]/2-56))
        rect_right = config.key_image["arrow_right"].get_rect(center=(config.WINDOW_SIZE[0]/2, config.WINDOW_SIZE[1]/2-56))

        rect_left.right = txt_language.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 55)).left-10
        rect_right.left = txt_language.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 55)).right+10

        if select == 0:
            yPosSelector = 55
            config.sc.blit(config.key_image["arrow_left"], rect_left)
            config.sc.blit(config.key_image["arrow_right"], rect_right)
        else:
            yPosSelector = -25
        for i in range(3):
            size_txt[i] += 3
        size_txt[0] = max(0, min(35, size_txt[0]))
        size_txt[1] = max(0, min(27, size_txt[1]))
        size_txt[2] = max(0, min(28, size_txt[2]))
        txt_language_info = pygame.font.Font(config.standart_font, size_txt[0]).render(
            language_system.txt_lang["SELECT_LANGUAGE"], False, (255, 0, 255))
        txt_ok = pygame.font.Font(config.standart_font, size_txt[2]).render(language_system.txt_lang["TXT_CONFIRM"], False, (255, 175, 245))
        config.sc.blit(txt_language_info,
                       txt_language_info.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 200)))
        config.sc.blit(txt_language,
                       txt_language.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 53)))

        config.sc.blit(txt_ok,
                       txt_ok.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 + 25)))
        config.sc.blit(selector_img,
                       selector_img.get_rect(
                           center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - yPosSelector)))
        if select == 1:
            config.sc.blit(config.key_image["enter"],
                        config.key_image["enter"].get_rect(center=(config.WINDOW_SIZE[0] - 110, 665)))
            config.sc.blit(config.txt_ok, config.txt_ok.get_rect(center=(config.WINDOW_SIZE[0] - 175, 668)))
        draw_notification()
        pygame.display.update()
        config.clock.tick(FPS)


def scene_fullscreen():
    running = True
    FPS: int = 60

    size_txt = [0, 0, 0]

    select = 0

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (55 * 5, 15 * 5))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.JOYBUTTONDOWN:
                if e.button == scene_config.control_gamepad["A"]:
                    if select == 0:
                        config.sc = pygame.display.set_mode(config.WINDOW_SIZE,
                                                            pygame.FULLSCREEN | pygame.SCALED | pygame.HWSURFACE)
                        scene_option.isFullscreen = True
                    running = False
                    select_aa_text()
                    return 0
            elif e.type == pygame.JOYHATMOTION:
                if e.value == (-1, 0):
                    select += 1
                elif e.value == (1, 0):
                    select -= 1
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if e.key == pygame.K_RETURN:
                    if select == 0:
                        config.sc = pygame.display.set_mode(config.WINDOW_SIZE,
                                                            pygame.FULLSCREEN | pygame.SCALED | pygame.HWSURFACE)
                        scene_option.isFullscreen = True
                    running = False
                    select_aa_text()
                    return 0
                if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                    select -= 1
                if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                    select += 1
            select %= 2
        config.sc.fill((0, 0, 0))
        if select == 0:
            xPosSelector = 150
        else:
            xPosSelector = -150
        for i in range(3):
            size_txt[i] += 3
        size_txt[0] = max(0, min(36, size_txt[0]))
        for i in range(2):
            size_txt[i + 1] = max(0, min(27, size_txt[i + 1]))
        txt_full_info = pygame.font.Font(config.standart_font, size_txt[0]).render(
            language_system.txt_lang["SELECT_FULLSCREEN"], False, (255, 0, 255))
        txt_yes = pygame.font.Font(config.standart_font, size_txt[1]).render(language_system.txt_lang["TXT_YES"], False,
                                                                             (255, 255, 255))
        txt_no = pygame.font.Font(config.standart_font, size_txt[2]).render(language_system.txt_lang["TXT_NO"], False,
                                                                            (255, 255, 255))
        config.sc.blit(txt_full_info,
                       txt_full_info.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 105)))
        config.sc.blit(txt_yes,
                       txt_yes.get_rect(center=(config.WINDOW_SIZE[0] / 2 - 150, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(txt_no,
                       txt_no.get_rect(center=(config.WINDOW_SIZE[0] / 2 + 150, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(selector_img, selector_img.get_rect(
            center=(config.WINDOW_SIZE[0] / 2 - xPosSelector, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(config.key_image["enter"],
                       config.key_image["enter"].get_rect(center=(config.WINDOW_SIZE[0] - 110, 665)))
        config.sc.blit(config.txt_ok, config.txt_ok.get_rect(center=(config.WINDOW_SIZE[0] - 175, 668)))
        draw_notification()
        pygame.display.update()
        config.clock.tick(FPS)


def select_aa_text():
    running = True
    FPS: int = 60

    size_txt = [0, 0, 0]

    select = 0

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (55 * 5, 15 * 5))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.JOYBUTTONDOWN:
                if e.button == scene_config.control_gamepad["A"]:
                    if select == 0:
                        scene_config.AA_TEXT = True
                    else:
                        scene_config.AA_TEXT = False
                    running = False
                    if pygame.joystick.get_count() > 0:
                        is_gamepad()
                    else:
                        final_conf()
                    return 0
            elif e.type == pygame.JOYHATMOTION:
                if e.value == (-1, 0):
                    select += 1
                elif e.value == (1, 0):
                    select -= 1
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if e.key == pygame.K_RETURN:
                    if select == 0:
                        scene_config.AA_TEXT = True
                    else:
                        scene_config.AA_TEXT = False
                    running = False
                    if pygame.joystick.get_count() > 0:
                        is_gamepad()
                    else:
                        final_conf()
                    return 0
                if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                    select -= 1
                if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                    select += 1
            select %= 2
        config.sc.fill((0, 0, 0))
        if select == 0:
            txt_example = pygame.font.Font(config.standart_font, 75).render(language_system.txt_lang["EXAMPLE_TXT"], True, (255, 200, 255))
            xPosSelector = 150
        else:
            txt_example = pygame.font.Font(config.standart_font, 75).render(language_system.txt_lang["EXAMPLE_TXT"], False,
                                                                            (255, 200, 255))
            xPosSelector = -150
        for i in range(3):
            size_txt[i] += 3
        size_txt[0] = max(0, min(36, size_txt[0]))
        for i in range(2):
            size_txt[i + 1] = max(0, min(27, size_txt[i + 1]))
        txt_full_info = pygame.font.Font(config.standart_font, size_txt[0]).render(
            language_system.txt_lang["SELECT_AA"], False, (255, 0, 255))
        txt_yes = pygame.font.Font(config.standart_font, size_txt[1]).render(language_system.txt_lang["TXT_YES"], False,
                                                                             (255, 255, 255))
        txt_no = pygame.font.Font(config.standart_font, size_txt[2]).render(language_system.txt_lang["TXT_NO"], False,
                                                                            (255, 255, 255))
        config.sc.blit(txt_full_info,
                       txt_full_info.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 105)))
        config.sc.blit(txt_yes,
                       txt_yes.get_rect(center=(config.WINDOW_SIZE[0] / 2 - 150, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(txt_no,
                       txt_no.get_rect(center=(config.WINDOW_SIZE[0] / 2 + 150, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(txt_example, txt_example.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 + 200)))
        config.sc.blit(selector_img, selector_img.get_rect(
            center=(config.WINDOW_SIZE[0] / 2 - xPosSelector, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(config.key_image["enter"],
                       config.key_image["enter"].get_rect(center=(config.WINDOW_SIZE[0] - 110, 665)))
        config.sc.blit(config.txt_ok, config.txt_ok.get_rect(center=(config.WINDOW_SIZE[0] - 175, 668)))
        draw_notification()
        pygame.display.update()
        config.clock.tick(FPS)


def is_gamepad():
    running = True
    FPS = 60

    size_txt = [0, 0, 0]

    select = 0

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (55 * 5, 15 * 5))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.JOYBUTTONDOWN:
                if e.button == scene_config.control_gamepad["A"]:
                    running = False
                    if select == 0:
                        gamepad_calibrate.select_input(final_conf)
                    else:
                        final_conf()
                    return 0
            elif e.type == pygame.JOYHATMOTION:
                if e.value == (-1, 0):
                    select += 1
                elif e.value == (1, 0):
                    select -= 1
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if e.key == pygame.K_RETURN:
                    running = False
                    if select == 0:
                        gamepad_calibrate.select_input(final_conf)
                    else:
                        final_conf()
                    return 0
                if e.key == pygame.K_LEFT or e.key == pygame.K_a:
                    select -= 1
                if e.key == pygame.K_RIGHT or e.key == pygame.K_d:
                    select += 1
            select %= 2
        
        config.sc.fill((0, 0, 0))
        if select == 0:
            xPosSelector = 150
        else:
            xPosSelector = -150
        for i in range(3):
            size_txt[i] += 3
        size_txt[0] = max(0, min(36, size_txt[0]))
        for i in range(2):
            size_txt[i + 1] = max(0, min(27, size_txt[i + 1]))
        txt_full_info = pygame.font.Font(config.standart_font, size_txt[0]).render(
            language_system.txt_lang["YOU_USE_GAMEPAD"], False, (255, 0, 255))
        txt_yes = pygame.font.Font(config.standart_font, size_txt[1]).render(language_system.txt_lang["TXT_YES"], False,
                                                                             (255, 255, 255))
        txt_no = pygame.font.Font(config.standart_font, size_txt[2]).render(language_system.txt_lang["TXT_NO"], False,
                                                                            (255, 255, 255))
        config.sc.blit(txt_full_info,
                       txt_full_info.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 105)))
        config.sc.blit(txt_yes,
                       txt_yes.get_rect(center=(config.WINDOW_SIZE[0] / 2 - 150, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(txt_no,
                       txt_no.get_rect(center=(config.WINDOW_SIZE[0] / 2 + 150, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(selector_img, selector_img.get_rect(
            center=(config.WINDOW_SIZE[0] / 2 - xPosSelector, config.WINDOW_SIZE[1] / 2)))
        config.sc.blit(config.key_image["enter"],
                       config.key_image["enter"].get_rect(center=(config.WINDOW_SIZE[0] - 110, 665)))
        config.sc.blit(config.txt_ok, config.txt_ok.get_rect(center=(config.WINDOW_SIZE[0] - 175, 668)))
        pygame.display.update()
        config.clock.tick(FPS)

def final_conf():
    running = True
    FPS: int = 60

    save_config(scene_option.isFullscreen)

    txt_size = 0

    frame_anim_next = 0

    yPosNext = 685

    next_image = pygame.image.load("Assets/sprite/ui/txt_dialog/next.png").convert_alpha()
    next_image = pygame.transform.scale(next_image, (7 * 4, 5 * 4))

    time_end = 0

    isKey = False

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.JOYBUTTONDOWN:
                if not isKey:
                    ScreenFill(1, 10, (0, 0, 0), 100)
                    isKey = True
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if not isKey:
                    ScreenFill(1, 10, (0, 0, 0), 100)
                    isKey = True

        config.sc.fill((0, 0, 0))
        txt_size += 3
        txt_size = max(0, min(35, txt_size))
        txt = pygame.font.Font(config.standart_font, txt_size).render(language_system.txt_lang["CONF_END"], scene_config.AA_TEXT, (255, 0, 255))
        config.sc.blit(txt, txt.get_rect(center=(config.WINDOW_SIZE[0] / 2, config.WINDOW_SIZE[1] / 2 - 1)))
        if txt_size >= 35:
            if frame_anim_next == 0:
                yPosNext = config.WINDOW_SIZE[1] / 2 + 85
            if frame_anim_next < 25:
                yPosNext -= 1
                frame_anim_next += 1
            else:
                frame_anim_next = 0
            config.sc.blit(next_image, next_image.get_rect(center=(config.WINDOW_SIZE[0] / 2, yPosNext)))
        if isKey:
            if time_end >= 100:
                running = False
                scene_start.start_warning()
                return 0
            else:
                time_end += 1

        fill_screen.draw(config.sc)
        fill_screen.update()

        draw_notification()
        pygame.display.update()
        config.clock.tick(FPS)
