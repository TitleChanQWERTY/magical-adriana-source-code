import os
from random import randint

import pygame
from config import sc, clock, WINDOW_SIZE, standart_font, key_image, txt_ok, check_gamepad
from reset_data import reset
from scene import scene_config, scene_select_mode, scene_weapon_menu, scene_player_profile, scene_option, \
    scene_statistic, scene_extra, scene_music_select
from screen_fill import ScreenFill
from screenshoot import TakeScreenshot, draw_notification
from sfx_colection import switch_select_menu, start_game_menu
from particle import BackParticle
from group_conf import particle_group, fill_screen
from save_progres import save
import gamepad_calibrate
import language_system

logo_game = pygame.image.load("Assets/sprite/ui/logo.png").convert_alpha()

frame_rect = pygame.Rect(WINDOW_SIZE[0] / 2 - 171, WINDOW_SIZE[1] / 2 - 156, 340, 310)

image_player_one = pygame.image.load("Assets/sprite/ui/main_menu/player_img/1.png").convert_alpha()
image_player_one = pygame.transform.scale(image_player_one, (97 * 4, 159 * 4))
image_player_one.set_alpha(9)

image_player_two = pygame.image.load("Assets/sprite/ui/main_menu/player_img/2.png").convert_alpha()
image_player_two = pygame.transform.scale(image_player_two, (97 * 4, 159 * 4))
image_player_two.set_alpha(9)


def scene_press():
    global logo_game

    pygame.mixer.music.load("Assets/ost/Problem with your magic.mp3")
    pygame.mixer.music.play(-1)
    pygame.mixer.music.set_volume(scene_config.music_volume)

    running: bool = True
    FPS: int = 60

    txt_press_any = pygame.font.Font(standart_font, 29).render(language_system.txt_lang["PRESS_ANY_KEY"],
                                                               scene_config.AA_TEXT, (255, 255, 255))

    frame_logo = 0
    yPosLogo = WINDOW_SIZE[1] / 2

    alpha_rect = 0

    isKey = False

    size_logo = [48 * 6, 36 * 6]

    frame_surf = pygame.Surface((WINDOW_SIZE[0], WINDOW_SIZE[1]), pygame.SRCALPHA)

    ScreenFill(255, -11, (0, 0, 0), 100)

    time_change_alpha_key = 0
    alpha_key = 300

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            
            if e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if not isKey:
                    isKey = True
                    start_game_menu.play()
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if not isKey:
                    isKey = True
                    start_game_menu.play()

        sc.fill((25, 8, 25))
        particle_group.draw(sc)
        particle_group.update()
        sc.blit(image_player_one, image_player_one.get_rect(center=(165, WINDOW_SIZE[1] // 2 + 150)))

        sc.blit(image_player_two, image_player_two.get_rect(center=(172 * 6, WINDOW_SIZE[1] // 2 + 150)))
        sc.blit(frame_surf, (0, 0))
        if isKey:
            size_logo[0] -= 5
            size_logo[1] -= 4

            size_logo[0] = max(size_logo[0], 48 * 4)
            size_logo[1] = max(size_logo[1], 36 * 4)

            if frame_logo > 5 and yPosLogo < 565:
                yPosLogo += 9
                alpha_rect += 8
            else:
                frame_logo += 1
            if yPosLogo >= 565:
                running = False
                scene()
                return 0
        else:
            if time_change_alpha_key < 30:
                alpha_key -= 9
            if time_change_alpha_key > 30:
                alpha_key += 9
                if alpha_key >= 295:
                    time_change_alpha_key = 0
            time_change_alpha_key += 1
            txt_press_any.set_alpha(alpha_key)
            sc.blit(txt_press_any, txt_press_any.get_rect(center=(WINDOW_SIZE[0] / 2, 645)))
        logo_game = pygame.image.load("Assets/sprite/ui/logo.png").convert_alpha()
        logo_game = pygame.transform.scale(logo_game, size_logo)
        sc.blit(logo_game, logo_game.get_rect(center=(WINDOW_SIZE[0] / 2, yPosLogo)))
        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)


def scene_exit():
    running: bool = True
    FPS: int = 60

    txt_exit = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["MAIN_MENU"],
                                                          scene_config.AA_TEXT,
                                                          (255, 95, 255))

    txt_exit_rect = txt_exit.get_rect(center=(103, 667))

    txt_exit_rect.left = key_image["esc"].get_rect(center=(39, 665)).right+8
    
    were_exit_txt = pygame.font.Font(standart_font, 30).render(language_system.txt_lang["WERE_YOU_CAN_EXIT"],
                                                               scene_config.AA_TEXT, (255, 255, 255))

    exit_txt = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["EXIT"],
                                                          scene_config.AA_TEXT, (200, 0, 0))

    exit_character_txt = pygame.font.Font(standart_font, 20).render(language_system.txt_lang["CHARACTER_MENU"],
                                                                    scene_config.AA_TEXT, (255, 0, 245))

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (64 * 5, 16 * 5))
    xPosSelector = WINDOW_SIZE[0] / 2 + 150

    select = 1

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.JOYHATMOTION:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.value == (-1, 0):
                    switch_select_menu.play()
                    select += 1
                elif e.value == (1, 0):
                    switch_select_menu.play()
                    select -= 1
                select %= 2
            if e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.button == scene_config.control_gamepad["A"]:
                    running = False
                    reset()
                    scene_config.p.weapon.clear()
                    if select == 1:
                        scene_player_profile.scene()
                    return 0
                elif e.button == scene_config.control_gamepad["B"]:
                    running = False
                    scene()
                    return 0
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if e.key == pygame.K_ESCAPE:
                    running = False
                    scene()
                    return 0
                if e.key == pygame.K_RETURN:
                    running = False
                    reset()
                    scene_config.p.weapon.clear()
                    if select == 1:
                        scene_player_profile.scene()
                    return 0
                if e.key == pygame.K_LEFT or e.key == pygame.K_d:
                    select += 1
                if e.key == pygame.K_RIGHT or e.key == pygame.K_a:
                    select -= 1
                select %= 2

        sc.fill((9, 3, 9))

        particle_group.draw(sc)
        particle_group.update()

        if select == 0:
            xPosSelector = WINDOW_SIZE[0] / 2 - 250
        else:
            xPosSelector = WINDOW_SIZE[0] / 2 + 250

        sc.blit(were_exit_txt, were_exit_txt.get_rect(center=(WINDOW_SIZE[0] / 2, 75)))

        sc.blit(exit_txt, exit_txt.get_rect(center=(WINDOW_SIZE[0] / 2 - 250, WINDOW_SIZE[1] / 2)))
        sc.blit(exit_character_txt, exit_character_txt.get_rect(center=(WINDOW_SIZE[0] / 2 + 250, WINDOW_SIZE[1] / 2)))

        sc.blit(selector_img, selector_img.get_rect(center=(xPosSelector, WINDOW_SIZE[1] / 2)))

        sc.blit(key_image["esc"], key_image["esc"].get_rect(center=(39, 665)))
        sc.blit(txt_exit, txt_exit_rect)

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)


def scene():
    global logo_game
    save()

    if not pygame.mixer.music.get_busy():
        pygame.mixer.music.load("Assets/ost/Problem with your magic.mp3")
        pygame.mixer.music.play(-1)
        pygame.mixer.music.set_volume(scene_config.music_volume)

    for particle in particle_group:
        particle.kill()
    for i in range(randint(165, 200)):
        size = randint(15, 21)
        BackParticle(randint(2, WINDOW_SIZE[0]), randint(15, 795),
                     scene_config.menu_particle_image[randint(0, len(scene_config.menu_particle_image) - 1)],
                     (size, size), 0, -randint(1, 2),
                     True, randint(20, 35))

    running: bool = True
    FPS: int = 60

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (64 * 5, 16 * 5))
    yPosSelector = 150

    PlayTXT = pygame.font.Font(standart_font, 36).render(language_system.txt_lang["PLAY"], scene_config.AA_TEXT,
                                                         (255, 255, 255))

    WeaponTXT = pygame.font.Font(standart_font, 31).render(language_system.txt_lang["WEAPON_MENU"],
                                                           scene_config.AA_TEXT,
                                                           (255, 255, 255))

    RecordTXT = pygame.font.Font(standart_font, 28).render(language_system.txt_lang["RECORDS"],
                                                           scene_config.AA_TEXT,
                                                           (255, 255, 255))
    OptionTXT = pygame.font.Font(standart_font, 22).render(language_system.txt_lang["OPTION"],
                                                           scene_config.AA_TEXT,
                                                           (255, 255, 255))
    ExtraTXT = pygame.font.Font(standart_font, 24).render(language_system.txt_lang["EXTRA"],
                                                           scene_config.AA_TEXT,
                                                           (255, 255, 255))
    ExitTXT = pygame.font.Font(standart_font, 22).render(language_system.txt_lang["EXIT"],
                                                         scene_config.AA_TEXT,
                                                         (255, 255, 255))

    profile_file = os.listdir("Profile")
    Select_Profile = pygame.font.Font(standart_font, 19).render(
        language_system.txt_lang["SELECT_PROFILE"] + ": " + str(profile_file[scene_player_profile.select_profile_num]),
        scene_config.AA_TEXT,
        (255, 255, 255))

    select: int = 0

    createBy = pygame.font.Font(standart_font, 15).render(language_system.txt_lang["CREATE_BY"],
                                                          scene_config.AA_TEXT, (155, 155, 155))

    ScreenFill(255, -10, (0, 0, 0), 100)

    frame_anim_logo = 0

    yPosLogo = 565

    PlayTXT.set_alpha(0)
    WeaponTXT.set_alpha(0)
    RecordTXT.set_alpha(0)
    OptionTXT.set_alpha(0)
    ExtraTXT.set_alpha(0)
    ExitTXT.set_alpha(0)

    AlphaExit = 0
    AlphaPlay = 0
    AlphaOption = 0
    AlphaWeapon = 0
    AlphaRecord = 0
    AlphaExtra = 0

    SelectorAlpha = 0
    selector_img.set_alpha(0)

    txt_character_alpha = 0

    Select_Profile.set_alpha(0)

    logo_game = pygame.transform.scale(logo_game, (50 * 4, 38 * 4))

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1
            if e.type == pygame.JOYHATMOTION:
                check_gamepad(gamepad_calibrate.input_sys)
                if e.value == (0, -1):
                    switch_select_menu.play()
                    select += 1
                elif e.value == (0, 1):
                    switch_select_menu.play()
                    select -= 1
                select %= 6
            elif e.type == pygame.JOYBUTTONDOWN:
                check_gamepad(gamepad_calibrate.input_sys)
                if yPosLogo <= 75:
                    if e.button == scene_config.control_gamepad["A"]:
                        running = False
                        match select:
                            case 0:
                                scene_select_mode.scene()
                            case 1:
                                scene_weapon_menu.scene(scene)
                            case 2:
                                scene_statistic.scene()
                            case 3:
                                scene_option.scene()
                            case 4:
                                scene_extra.scene()
                            case 5:
                                scene_exit()
                        return 0
                    
            elif e.type == pygame.KEYDOWN:
                check_gamepad(gamepad_calibrate.input_sys, True)
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if yPosLogo <= 75:
                    if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                        switch_select_menu.play()
                        select += 1
                    if e.key == pygame.K_UP or e.key == pygame.K_w:
                        switch_select_menu.play()
                        select -= 1
                    if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                        running = False
                        match select:
                            case 0:
                                scene_select_mode.scene()
                            case 1:
                                scene_weapon_menu.scene(scene)
                            case 2:
                                scene_statistic.scene()
                            case 3:
                                scene_option.scene()
                            case 4:
                                scene_extra.scene()
                            case 5:
                                scene_exit()
                        return 0

                    select %= 6

        sc.fill((20, 5, 20))

        match select:
            case 0:
                yPosSelector = 248
            case 1:
                yPosSelector = 299
            case 2:
                yPosSelector = 349
            case 3:
                yPosSelector = 399
            case 4:
                yPosSelector = 449
            case 5:
                yPosSelector = 499

        sc.blit(image_player_one, image_player_one.get_rect(center=(165, WINDOW_SIZE[1] // 2 + 150)))

        sc.blit(image_player_two, image_player_two.get_rect(center=(172 * 6, WINDOW_SIZE[1] // 2 + 150)))

        if yPosLogo < 249:
            AlphaPlay += 9
            PlayTXT.set_alpha(AlphaPlay)
            AlphaPlay = max(0, min(300, AlphaPlay))
            sc.blit(PlayTXT, PlayTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 248)))
        if yPosLogo < 299:
            AlphaWeapon += 8
            WeaponTXT.set_alpha(AlphaWeapon)
            AlphaWeapon = max(0, min(300, AlphaWeapon))
            sc.blit(WeaponTXT, WeaponTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 298)))
        if yPosLogo < 349:
            AlphaRecord += 7
            RecordTXT.set_alpha(AlphaRecord)
            AlphaRecord = max(0, min(300, AlphaRecord))
            sc.blit(RecordTXT, RecordTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 348)))
        if yPosLogo < 399:
            AlphaOption += 6
            OptionTXT.set_alpha(AlphaOption)
            AlphaOption = max(0, min(300, AlphaOption))
            sc.blit(OptionTXT, OptionTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 398)))
        if yPosLogo < 449:
            AlphaExtra += 5
            ExtraTXT.set_alpha(AlphaExtra)
            AlphaExtra = max(0, min(300, AlphaExtra))

            sc.blit(ExtraTXT, ExtraTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 448)))
        if yPosLogo < 499:
            AlphaExit += 5
            ExitTXT.set_alpha(AlphaExit)
            AlphaExit = max(0, min(300, AlphaExit))

            sc.blit(ExitTXT, ExitTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 498)))

        if yPosLogo <= 249:
            SelectorAlpha += 10
            selector_img.set_alpha(SelectorAlpha)
            SelectorAlpha = max(0, min(300, SelectorAlpha))
            sc.blit(selector_img, selector_img.get_rect(center=(WINDOW_SIZE[0] / 2, yPosSelector)))

        sc.blit(createBy, createBy.get_rect(center=(WINDOW_SIZE[0] // 2, 645)))

        if frame_anim_logo > 25 and yPosLogo > 80:
            yPosLogo -= 14
        else:
            frame_anim_logo += 1

        sc.blit(logo_game, logo_game.get_rect(center=(WINDOW_SIZE[0] // 2, yPosLogo)))

        if yPosLogo <= 160:
            txt_character_alpha += 15
            Select_Profile.set_alpha(txt_character_alpha)
            txt_character_alpha = max(0, min(300, txt_character_alpha))
            sc.blit(Select_Profile, Select_Profile.get_rect(center=(WINDOW_SIZE[0] // 2, 170)))

        particle_group.draw(sc)
        particle_group.update()

        sc.blit(key_image["enter"], key_image["enter"].get_rect(center=(WINDOW_SIZE[0] - 110, 665)))
        sc.blit(txt_ok, txt_ok.get_rect(center=(WINDOW_SIZE[0] - 175, 668)))

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
