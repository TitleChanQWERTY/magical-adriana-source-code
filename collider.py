from random import randint

import pygame.key

import language_system
from item import ammo
from player.weapon import MagicPistol, MagicMinigun, MagicShotgun, MagicTrident
from config import sc, WINDOW_SIZE, joystick_m, gamepad, key_image
from item import weapon, health
from scene import scene_config
import group_conf
from particle import PhysicalParticle
from screen_fill import ScreenFill
from sfx_colection import enemy_take_damage, player_take_damage
from txt_spawn_collect import TXT
from anim_effect import Effect

electro_shield = pygame.image.load("Assets/sprite/enemy/shield/electro/1.png").convert_alpha()
electro_shield = pygame.transform.scale(electro_shield, (20 * 3, 20 * 3))

particle_open = pygame.image.load("Assets/sprite/particle/particle/4.png").convert_alpha()

player_blood = (pygame.image.load("Assets/sprite/particle/player_blood/1.png").convert_alpha(),
                pygame.image.load("Assets/sprite/particle/player_blood/2.png").convert_alpha())

bullet_player_break = (pygame.image.load("Assets/sprite/effect/bullet_Player_break/1.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/bullet_Player_break/2.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/bullet_Player_break/3.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/bullet_Player_break/4.png").convert_alpha(),
                        pygame.image.load("Assets/sprite/effect/bullet_Player_break/5.png").convert_alpha())

timeDamage = 120


def bullet_collider():
    global timeDamage
    for enemy in group_conf.enemy_group:
        for bomb in group_conf.bomb_boom_group:
            if enemy.rect.colliderect(bomb.rect):
                if not enemy.isChain:
                    enemy_take_damage.play()
                    enemy.rect.move_ip(0, -2)
                    enemy.health -= randint(1, 3)
                    size_blood = randint(4, 8)
                    for i in range(randint(4, 8)):
                        PhysicalParticle(enemy.rect.centerx, enemy.rect.centery + 10, randint(-7, -6),
                            scene_config.blood_sprite[randint(0, 1)], (size_blood, size_blood))
            for e_b in group_conf.bullet_enemy_group:
                if e_b.rect.colliderect(bomb.rect):
                    e_b.kill()
        for p_b in group_conf.bullet_player_group:
            if not enemy.isChain:
                if enemy.rect.collidepoint(p_b.rect.center) and enemy.rect.y > 88:
                    Effect(p_b.rect.centerx, p_b.rect.centery, bullet_player_break, (54, 54), 2)
                    scene_config.p.time_take_damage += randint(1, 5)
                    scene_config.p.yPosMagicalPower += 45
                    enemy_take_damage.play()
                    if not enemy.isBoss:
                        enemy.rect.y -= 2
                    enemy.health -= scene_config.p.damage_st
                    TXT(enemy.rect.centerx, enemy.rect.centery, 275, "-"+str(scene_config.p.damage_st), color=(225, 0, 0))
                    size_blood = randint(3, 7)
                    for i in range(randint(3, 7)):
                        PhysicalParticle(enemy.rect.centerx, enemy.rect.centery + 10, randint(-6, -5),
                                    scene_config.blood_sprite[randint(0, 1)], (size_blood, size_blood))
                    p_b.kill()
        for e_b in group_conf.electro_line_group:
            if not enemy.isChain:
                if enemy.rect.colliderect(e_b.rect) and enemy.rect.y > 88:
                    if enemy.type_enemy == 0:
                        scene_config.p.time_take_damage += randint(2, 4)
                        scene_config.p.yPosMagicalPower += 45
                        enemy_take_damage.play()
                        if not enemy.isBoss:
                            enemy.rect.y -= 2
                        enemy.health -= scene_config.p.damage_st
                        TXT(enemy.rect.centerx, enemy.rect.centery, 275, "-"+str(scene_config.p.damage_st), color=(225, 0, 0))
                        size_blood = randint(3, 5)
                        for i in range(randint(3, 6)):
                            PhysicalParticle(enemy.rect.centerx, enemy.rect.centery + 10, randint(-6, -5),
                                            scene_config.blood_sprite[randint(0, 1)], (size_blood, size_blood))
                    else:
                        sc.blit(electro_shield,
                                electro_shield.get_rect(center=(enemy.rect.centerx + 1, enemy.rect.centery + 5)))

    if timeDamage >= scene_config.timeDamageMax:
        scene_config.p.image.set_alpha(300)
        for e_b in group_conf.bullet_enemy_group:
            if e_b.rect.collidepoint(scene_config.p.rect.center) and e_b.image.get_alpha() > 190:
                ScreenFill(175, -8, (255, 0, 0), 40)
                scene_config.screen_shake += 12
                if scene_config.p.ARMOR > 0:
                    scene_config.power_shake += 3
                    scene_config.p.ARMOR -= e_b.damage + 25
                else:
                    scene_config.power_shake += 5
                    scene_config.p.HEALTH -= e_b.damage
                timeDamage = 0
                scene_config.p.time_take_damage = randint(0, 1)
                player_take_damage[randint(0, len(player_take_damage) - 1)].play()
                for i in range(20):
                    size = randint(6, 12)
                    PhysicalParticle(scene_config.p.rect.centerx, scene_config.p.rect.centery + 10, randint(-9, -5),
                                     player_blood[randint(0, 1)], (size, size))
                e_b.kill()
        for enemy in group_conf.enemy_group:
            if scene_config.p.rect.colliderect(enemy.rect) and enemy.time_damage_set >= 7:
                ScreenFill(185, -6, (255, 0, 0), 45)
                scene_config.screen_shake += 11
                if scene_config.p.ARMOR > 0:
                    scene_config.power_shake += 4
                    scene_config.p.ARMOR -= 15 + 25
                else:
                    scene_config.power_shake += 5
                    scene_config.p.HEALTH -= 15
                timeDamage = 0
                scene_config.p.time_take_damage = randint(0, 1)
                player_take_damage[randint(0, len(player_take_damage) - 1)].play()
                for i in range(20):
                    size = randint(6, 12)
                    PhysicalParticle(scene_config.p.rect.centerx, scene_config.p.rect.centery + 10, randint(-9, -5),
                                     player_blood[randint(0, 1)], (size, size))
    else:
        for e_b in group_conf.bullet_enemy_group:
            e_b.kill()
        scene_config.p.image.set_alpha(100)
        timeDamage += 1
