from random import randint

import pygame
from group_conf import item_group
from animator import Animator
from scene import scene_config
from item import item_main
from txt_spawn_collect import TXT

image_money = (pygame.image.load("Assets/sprite/item/specifyc_item/money/1.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/specifyc_item/money/2.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/specifyc_item/money/3.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/specifyc_item/money/4.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/specifyc_item/money/5.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/specifyc_item/money/6.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/specifyc_item/money/7.png").convert_alpha())


class Money(item_main.Item):
    def __init__(self, x, y):
        super().__init__()
        self.image = image_money[0]
        self.image = pygame.transform.scale(self.image, (11*3, 8*3))
        self.rect = self.image.get_rect(center=(x, y))

        self.anim = Animator(randint(4, 6), image_money)

        self.money_tacke = randint(5, 13)

    def update(self):
        super().update()
        self.image = self.anim.update()
        self.image = pygame.transform.scale(self.image, (10 * 3, 7 * 3))

        if scene_config.p.rect.colliderect(self.rect):
            TXT(self.rect.centerx, self.rect.centery, 285, "+"+str(self.money_tacke), color=(0, 255, 0))
            scene_config.p.MONEY += self.money_tacke
            self.kill()
