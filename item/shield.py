import pygame
from item import item_main
from scene import scene_config
from txt_spawn_collect import TXT

shield_image = pygame.image.load("Assets/sprite/ui/icon/shield.png").convert_alpha()
shield_image = pygame.transform.scale(shield_image, (27, 27))


class Shield(item_main.Item):
    def __init__(self, x, y):
        super().__init__()
        self.image = shield_image
        self.rect = self.image.get_rect(center=(x, y))
        
    def update(self):
        super().update()
        if scene_config.p.rect.colliderect(self.rect):
            TXT(self.rect.centerx, self.rect.centery, 285, "+1", color=(255, 15, 235))
            if scene_config.p.get_shield < 2:
                scene_config.p.get_shield += 1
            else:
                scene_config.p.ARMOR += 50
                scene_config.p.size_shield += 45
                scene_config.p.get_shield = 0
                scene_config.p.isShieldActive = True
            self.kill()

