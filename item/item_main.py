import pygame
from group_conf import item_group

class Item(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()

        self.forceUp = -8
        self.forceUp_max = self.forceUp

        self.add(item_group)

    def update(self):
        self.forceUp += 0.4
        self.forceUp = max(self.forceUp_max, min(6, self.forceUp))
        self.rect.y += self.forceUp

        if self.rect.y >= 660:
            self.kill()
