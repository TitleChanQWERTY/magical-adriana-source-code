import pygame
from group_conf import item_group
from item import item_main
from scene import scene_config
from txt_spawn_collect import TXT
from random import randint

image_ammo = pygame.image.load("Assets/sprite/item/boster/AmmoPack.png").convert_alpha()
image_ammo_scale = pygame.transform.scale(image_ammo, (25, 27))


class Ammo(item_main.Item):
    def __init__(self, x, y):
        super().__init__()
        self.image = image_ammo_scale
        self.rect = self.image.get_rect(center=(x, y))

    def update(self):
        super().update()
        if scene_config.p.rect.colliderect(self.rect):
            if scene_config.difficulty == 0:
                ammo_plus = randint(55, 75)
            elif scene_config.difficulty == 1:
                ammo_plus = randint(35, 50)
            else:
                ammo_plus = randint(25, 45)
            scene_config.p.AMMO += ammo_plus
            TXT(self.rect.centerx, self.rect.centery, 295, "+"+str(ammo_plus))
            self.kill()
