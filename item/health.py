import pygame
from group_conf import item_group
from scene import scene_config
from item import item_main
from random import randint
from txt_spawn_collect import TXT

image_health = pygame.image.load("Assets/sprite/item/boster/Health.png").convert_alpha()


class Health(item_main.Item):
    def __init__(self, x, y):
        super().__init__()
        self.image = image_health
        self.image = pygame.transform.scale(self.image, (22, 23))
        self.rect = self.image.get_rect(center=(x, y))

    def update(self):
        super().update()
        if scene_config.p.rect.colliderect(self.rect):
            health_plus = randint(3, 7)
            TXT(self.rect.centerx, self.rect.centery, 285, "+" + str(health_plus), color=(255, 0, 240))
            scene_config.p.HEALTH += health_plus
            self.kill()
