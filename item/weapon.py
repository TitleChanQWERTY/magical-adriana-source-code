import pygame
from group_conf import item_group
from item import item_main
from scene import scene_config
from txt_spawn_collect import TXT
from player.weapon import MagicPistol, MagicMinigun, MagicShotgun, MagicTrident
from sfx_colection import player_take_weapon
import language_system


weapon_icon = (pygame.image.load("Assets/sprite/item/weapon/pistiol.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/weapon/shutgun.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/weapon/minigun.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/weapon/electro_trident.png").convert_alpha())


class WeaponItem(item_main.Item):
    def __init__(self, x, y, weapon, size=(24, 21)):
        super().__init__()
        self.image = weapon_icon[weapon-1]
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(item_group)

        self.id = weapon

    def update(self):
        super().update()

        if scene_config.p.rect.colliderect(self.rect):
            player_take_weapon.play()
            match self.id:
                case 1:
                    TXT(self.rect.centerx, self.rect.centery, 285, language_system.txt_lang["NEW_WEAPON"])
                    scene_config.p.weapon.append(MagicPistol())
                    self.kill()
                case 2:
                    TXT(self.rect.centerx, self.rect.centery, 285, language_system.txt_lang["NEW_WEAPON"])
                    scene_config.p.weapon.append(MagicShotgun())
                    self.kill()
                case 3:
                    TXT(self.rect.centerx, self.rect.centery, 285, language_system.txt_lang["NEW_WEAPON"])
                    scene_config.p.weapon.append(MagicMinigun())
                    self.kill()
                case 4:
                    TXT(self.rect.centerx, self.rect.centery, 285, language_system.txt_lang["NEW_WEAPON"])
                    scene_config.p.weapon.append(MagicTrident())
                    self.kill()

