import pygame
from group_conf import item_group
from scene import scene_config
from item import item_main, weapon, ammo, health
from random import randint
from particle import PhysicalParticle
from collider import particle_open
from config import sc, key_image, joystick_m

cont_sprite = (pygame.image.load("Assets/sprite/item/container/1.png").convert_alpha(),
               pygame.image.load("Assets/sprite/item/container/2.png").convert_alpha())


class Container(item_main.Item):
    def __init__(self, x, y):
        super().__init__()
        self.image = cont_sprite[0]
        self.image = pygame.transform.scale(cont_sprite[0], (25 * 2, 14 * 2))
        self.rect = self.image.get_rect(center=(x, y))

        self.alpha = 300
        self.time_alpha = 0

        self.time_live = 0

        self.id = 20

        self.max_down = randint(8, 13)

        self.time_open = 0

        self.forceUp = -10
        self.forceUp_max = self.forceUp

    def update(self):
        if self.forceUp <= self.max_down:
            self.forceUp += 0.4
            self.rect.y += self.forceUp
        if self.rect.y >= 645 or self.time_live >= 361:
            if not scene_config.p.isFirstRun:
                self.kill()
        elif self.time_live >= 210:
            match self.time_alpha:
                case 4:
                    self.alpha = 100
                case 8:
                    self.time_alpha = 0
                    self.alpha = 300
            self.time_alpha += 1
            self.image.set_alpha(self.alpha)
        
        self.time_live += 1
        
        if self.time_open >= 15:
            if scene_config.p.rect.colliderect(self.rect):
                key = pygame.key.get_pressed()
                pygame.draw.rect(sc, (255, 255, 255), (self.rect.x, self.rect.y, 21 * 2, 13 * 2), 2)
                sc.blit(key_image["e"], key_image["e"].get_rect(center=(self.rect.centerx, self.rect.centery - 30)))
                if key[pygame.K_e] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["A"]):
                    PhysicalParticle(self.rect.centerx + 1, self.rect.centery - 15, -10, particle_open, (14, 29), 0,
                                            -1)
                    PhysicalParticle(self.rect.centerx + 1, self.rect.centery - 15, -10, particle_open, (14, 29), 0,
                                            1)
                    if not scene_config.p.isFirstRun:
                        type_item = randint(1, 5)
                    else:
                        type_item = randint(1, 4)
                    if type_item == 1:
                        weapon.WeaponItem(self.rect.centerx, self.rect.centery - 30, 1)
                    elif type_item == 2:
                        weapon.WeaponItem(self.rect.centerx, self.rect.centery - 30, 2, (32, 19))
                    elif type_item == 3:
                        weapon.WeaponItem(self.rect.centerx, self.rect.centery - 30, 3, (47, 22))
                    elif type_item == 4:
                        weapon.WeaponItem(self.rect.centerx, self.rect.centery - 30, 4, (13 * 2, 13 * 2))
                    elif type_item == 5:
                        ammo.Ammo(self.rect.centerx, self.rect.centery - 30)
                    self.kill()
        else:
            self.time_open += 1


class ContainerHealth(item_main.Item):
    def __init__(self, x, y):
        super().__init__()
        self.image = cont_sprite[1]
        self.image = pygame.transform.scale(cont_sprite[1], (25 * 2, 14 * 2))
        self.rect = self.image.get_rect(center=(x, y))

        self.alpha: int = 300
        self.time_alpha: int = 0

        self.time_live: int = 0

        self.max_down = randint(7, 15)

    def update(self):
        if self.forceUp <= self.max_down:
            self.forceUp += 0.4
            self.rect.y += self.forceUp
        if self.rect.y >= 645 or self.time_live >= 361:
            if not scene_config.p.isFirstRun:
                self.kill()
        elif self.time_live >= 210:
            match self.time_alpha:
                case 4:
                    self.alpha = 100
                case 8:
                    self.time_alpha = 0
                    self.alpha = 300
            self.time_alpha += 1
            self.image.set_alpha(self.alpha)
        
        self.time_live += 1

        if scene_config.p.rect.colliderect(self.rect):
            key = pygame.key.get_pressed()
            pygame.draw.rect(sc, (255, 255, 255), (self.rect.x, self.rect.y, 21 * 2, 13 * 2), 2)
            sc.blit(key_image["e"], key_image["e"].get_rect(center=(self.rect.centerx, self.rect.centery - 30)))
            if key[pygame.K_e] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["A"]):
                PhysicalParticle(self.rect.centerx + 1, self.rect.centery - 15, -10, particle_open, (14, 29), 0,
                                         -1)
                PhysicalParticle(self.rect.centerx + 1, self.rect.centery - 15, -10, particle_open, (14, 29), 0,
                                         1)
                health.Health(self.rect.centerx, self.rect.centery-35)
                self.kill()
