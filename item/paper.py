import pygame
from item import item_main
from random import randint
from scene import scene_config
import language_system
from notfication import Notification


paper_img = pygame.image.load("Assets/sprite/item/specifyc_item/paper.png").convert_alpha()
paper_img = pygame.transform.scale(paper_img, (11 * 4, 12 * 4))

class Paper(item_main.Item):
    def __init__(self, x, y):
        super().__init__()
        self.image = paper_img
        self.rect = self.image.get_rect(center=(x, y))
    
    def update(self):
        super().update()
        if scene_config.p.rect.colliderect(self.rect):
            for number in range(7):
                if "paper_"+str(number) not in scene_config.p.paper_story:
                    scene_config.p.paper_story.append("paper_"+str(number)) 
                    Notification(language_system.txt_lang["DOCUMENT_TXT"], 3)
                    break
            self.kill()
