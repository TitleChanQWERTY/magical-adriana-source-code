import pygame
from config import sc
import time
from notfication import Notification, draw
import language_system


def TakeScreenshot():
    Notification(language_system.txt_lang["TAKE_SCREEN"])
    pygame.image.save(sc, str(int(time.time())) + ".png")


def draw_notification():
    return draw()
