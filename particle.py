from random import randint

import pygame
from group_conf import particle_group
from scene import scene_config


class PhysicalParticle(pygame.sprite.Sprite):
    def __init__(self, x, y, speed_y, image, size, typeChange=0, speed_x=0):
        super().__init__()
        self.image = image
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.image.set_alpha(randint(165, 285))

        self.speed_y = speed_y
        self.speed_x = speed_x

        self.timeChange: int = 0
        self.maxTimeChange: int = randint(1, 40)

        self.typeChange = typeChange

        if self.speed_x == 0:
            self.typeChange = randint(-1, 1)

    def update(self):
        if self.rect.y > 685:
            self.kill()
            return 0

        self.speed_y += 0.4

        if self.timeChange < self.maxTimeChange:
            if self.typeChange < 0:
                self.speed_x -= 0.1
            elif self.typeChange > 0:
                self.speed_x += 0.1
            self.timeChange += 1

        self.rect.x += self.speed_x
        self.rect.y += self.speed_y


class BackParticle(pygame.sprite.Sprite):
    def __init__(self, x, y, image, size, speed_x, speed_y, isBack=True,
                 alpha=randint(105, 245)):
        super().__init__()
        self.file = image
        self.size = size
        self.image = self.file
        self.image = pygame.transform.scale(self.image, self.size)
        self.image = pygame.transform.gaussian_blur(self.image, randint(0, 2))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.timeLive: int = 0
        self.timeLiveMax: int = randint(55, 145)

        self.speed_x = speed_x
        self.speed_y = speed_y

        self.st_speed_y = speed_y

        self.angle = 0
        self.angle_max = randint(-5, 5)

        if self.angle_max == 0:
            self.angle_max = 1

        self.x = x
        self.y = y

        if not isBack:
            self.x_standart = self.x
            self.y_standart = self.y
        else:
            self.x_standart = self.x
            self.y_standart = 750

        self.alpha = alpha
        self.image.set_alpha(alpha)

        if self.speed_y == 0:
            self.speed_y = randint(1, 2)

        self.speed_y_st = self.speed_y

        self.max_speed_y = randint(4, 14)

    def update(self):
        self.x += self.speed_x
        if not scene_config.isBoss3:
            if self.rect.y < -35 or self.rect.y > 751:
                self.angle = 0
                self.x = self.x_standart
                self.y = self.y_standart
            self.y += self.speed_y
            self.speed_y -= 1
        else:
            if self.rect.y >= 750:
                self.angle = 0
                self.x = self.x_standart
                self.y = -randint(15, 65)
            self.y += self.speed_y
            self.speed_y += 1
        self.speed_y = max(self.speed_y_st, min(self.max_speed_y, self.speed_y))

        self.image = self.file
        self.image = pygame.transform.scale(self.image, self.size)

        self.image = pygame.transform.gaussian_blur(self.image, 1)

        self.angle += self.angle_max
        self.image = pygame.transform.rotate(self.image, self.angle)

        self.image.set_alpha(self.alpha)

        self.rect = self.image.get_rect(center=(self.x, self.y))


class AreaParticle(pygame.sprite.Sprite):
    def __init__(self, x, y, image, size, alpha=randint(95, 215)):
        super().__init__()
        self.file = image
        self.size = size
        self.image = self.file
        self.image = pygame.transform.scale(self.image, self.size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.timeLive: int = 0
        self.timeLiveMax: int = randint(105, 195)

        self.speed_x = randint(-4, 4)
        self.speed_y = randint(-3, 3)

        self.angle = 0
        self.angle_max = randint(-5, 5)

        self.x = x
        self.y = y

        self.alpha = alpha
        self.image.set_alpha(alpha)

        if self.speed_y == 0:
            self.speed_y = randint(1, 2)

    def update(self):
        if self.timeLive >= self.timeLiveMax or self.rect.y < 40 or self.rect.y > 620:
            self.kill()
            return 0
        self.timeLive += 1

        self.x += self.speed_x
        self.y += self.speed_y

        self.image = self.file
        self.image = pygame.transform.scale(self.image, self.size)

        self.angle += self.angle_max
        self.image = pygame.transform.rotate(self.image, self.angle)

        self.image.set_alpha(self.alpha)

        self.rect = self.image.get_rect(center=(self.x, self.y))
