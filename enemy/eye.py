from random import randint

import pygame

from anim_effect import Effect
from config import WINDOW_SIZE, sc
from particle import AreaParticle
from scene import scene_config
from group_conf import enemy_group
from animator import Animator
from bullet import bullet
from item import money
from sfx_colection import enemy_eye_shoot_laser_1, enemy_eye_shoot_laser_2, chanel_enemy_shoot_2
from txt_spawn_collect import TXT
from cartoon_splash import Splash

eye_sprite = (pygame.image.load("Assets/sprite/enemy/eye/1.png").convert_alpha(),
              pygame.image.load("Assets/sprite/enemy/eye/2.png").convert_alpha(),
              pygame.image.load("Assets/sprite/enemy/eye/3.png").convert_alpha(),
              pygame.image.load("Assets/sprite/enemy/eye/2.png").convert_alpha())

eye_sprite_attack = (pygame.image.load("Assets/sprite/enemy/eye/attack/1.png").convert_alpha(),
                     pygame.image.load("Assets/sprite/enemy/eye/attack/2.png").convert_alpha(),
                     pygame.image.load("Assets/sprite/enemy/eye/attack/3.png").convert_alpha(),
                     pygame.image.load("Assets/sprite/enemy/eye/attack/2.png").convert_alpha())

eye_cloud = pygame.image.load("Assets/sprite/particle/eye_cloud.png").convert_alpha()


class Eye(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = eye_sprite[0]
        self.image = pygame.transform.scale(self.image, (21, 31))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(enemy_group)

        self.anim = Animator(11, eye_sprite)

        self.anim_attack = Animator(7, eye_sprite_attack)

        self.health: int = 2

        self.isChain = False

        for i in range(randint(8, 13)):
            size = randint(4, 6)
            select_particle = randint(0, 1)
            if select_particle == 1:
                AreaParticle(self.rect.centerx, self.rect.centery, scene_config.gore_sprite[2],
                             (size, size))
            else:
                AreaParticle(self.rect.centerx, self.rect.centery, eye_cloud,
                             (size + 9, size + 9))

        self.time_shoot = 0

        self.type_enemy = 0

        self.time_damage_set = 0

        self.isBoss = False

    def shoot(self):
        if self.time_shoot > 199:
            bullet.Bullet(self.rect.centerx, self.rect.centery + 320, 0, 0, bullet.bullet_Sprite[5], (7, 650),
                          damage=scene_config.enemy_bullet_damage, time_destroy=52)
            chanel_enemy_shoot_2.play(enemy_eye_shoot_laser_2)
            self.time_shoot = 0
        elif self.time_shoot == 149:
            enemy_eye_shoot_laser_1.play()
            Effect(self.rect.centerx, self.rect.centery - 5, scene_config.laser_shoot_effect, (59, 59), 12)
        elif self.time_shoot >= 150:
            pygame.draw.rect(sc, (175, 25, 25), (self.rect.centerx, self.rect.bottom, 1, WINDOW_SIZE[1]))
        self.time_shoot += 1

    def update(self):
        self.time_damage_set += 1
        self.time_damage_set = max(0, min(8, self.time_damage_set))
        if self.health <= 0:
            scene_config.p.kill_count += 1
            scene_config.p.SCORE += 25
            money.Money(self.rect.centerx, self.rect.centery - 5)
            Effect(self.rect.centerx, self.rect.centery, scene_config.enemy_die_effect_one, (42, 42), 7)
            self.kill()
            Splash("KILL", self.rect.center)
            return 0

        self.shoot()

        if self.time_shoot < 146:
            self.image = self.anim.update()
        else:
            self.image = self.anim_attack.update()
        self.image = pygame.transform.scale(self.image, (21, 31))
