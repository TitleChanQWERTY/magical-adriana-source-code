from random import randint

import pygame
from config import sc
from group_conf import enemy_group
from animator import Animator
from bullet import bullet
from scene import scene_config
from item import shield
from particle import AreaParticle, PhysicalParticle
from sfx_colection import enemy_spider_shoot
from cartoon_splash import Splash

spider_sprite = (pygame.image.load("Assets/sprite/enemy/spider/1.png").convert_alpha(),
                 pygame.image.load("Assets/sprite/enemy/spider/2.png").convert_alpha())

spider_sprite_die = pygame.image.load("Assets/sprite/enemy/spider/die.png").convert_alpha()


class Spider(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = spider_sprite[0]
        self.image = pygame.transform.scale(self.image, (44, 42))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(enemy_group)

        self.health = randint(50, 70)

        self.size_line = 1

        self.max_down = randint(120, 465)

        self.rectLine = pygame.Rect(self.rect.centerx + 1, self.rect.centery - 15, 5, 10)

        self.anim = Animator(40, spider_sprite)

        self.timeShoot: int = 0

        self.isDie: bool = False

        self.timeUp: int = 0

        self.animDieMove: int = 0

        self.color_yarn = (0, 0, 175)

        self.type_enemy = 0
        
        self.isChain = False

        self.time_damage_set = 0

        self.isBoss = False

    def shoot(self):
        if self.timeShoot > 126:
            bullet.Bullet(self.rect.centerx + 1, self.rect.centery + 17, 0, 6, bullet.bullet_Sprite[4], (11, 11), damage=scene_config.enemy_bullet_damage)
            bullet.Bullet(self.rect.centerx + 1, self.rect.centery + 17, -2, 5, bullet.bullet_Sprite[4], (11, 11), damage=scene_config.enemy_bullet_damage)
            bullet.Bullet(self.rect.centerx + 1, self.rect.centery + 17, 2, 5, bullet.bullet_Sprite[4], (11, 11), damage=scene_config.enemy_bullet_damage)
            self.timeShoot = 0
            return
        self.timeShoot += 1

    def update(self):
        self.time_damage_set += 1
        self.time_damage_set = max(0, min(8, self.time_damage_set))
        if not self.isDie:
            self.image = self.anim.update()
            self.image = pygame.transform.scale(self.image, (44, 42))
            if self.rect.y < self.max_down:
                self.rect.centery += 2
                self.size_line += 5
            else:
                self.shoot()
            if self.health <= 0:
                enemy_spider_shoot.play()
                luky_shield = randint(0, 1)
                if scene_config.p.ARMOR <= 70 and luky_shield == 1:
                    shield.Shield(self.rect.centerx, self.rect.centery-6)
                Splash("KILL", self.rect.center)
                scene_config.p.kill_count += 1
                scene_config.p.SCORE += randint(125, 155)
                for i in range(randint(25, 30)):
                    select_particle = randint(0, 1)
                    if select_particle == 0:
                        size = randint(6, 9)
                        AreaParticle(self.rect.centerx, self.rect.centery,
                                     scene_config.simple_particle_sprite[randint(0, 1)],
                                     (size, size))
                    else:
                        size = randint(13, 16)
                        AreaParticle(self.rect.centerx, self.rect.centery,
                                     scene_config.gore_sprite[randint(0, 2)],
                                     (size, size))
                self.color_yarn = (255, 155, 255)
                self.isDie = True
        else:
            size_blood = randint(2, 4)
            for i in range(randint(0, 1)):
                PhysicalParticle(self.rect.centerx + 3, self.rect.centery + 8, 0,
                                 scene_config.blood_sprite[randint(0, 1)], (size_blood, size_blood))
            self.image = spider_sprite_die
            self.image = pygame.transform.scale(self.image, (49, 47))
            self.rectLine.x = self.rect.centerx + 4
            match self.animDieMove:
                case 15:
                    self.rect.x -= 1
                case 25:
                    self.animDieMove = 0
                    self.rect.x += 1
            self.animDieMove += 1
            if self.timeUp >= 10:
                if self.rect.y > 50:
                    self.rect.y -= 5
                    self.size_line -= 6
                else:
                    self.kill()
            else:
                self.timeUp += 1
        pygame.draw.rect(sc, self.color_yarn,
                         (self.rectLine.x - 2, self.rect.y - self.size_line + 6, 2, self.size_line))
