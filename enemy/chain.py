import pygame
from group_conf import enemy_group
from random import randint
from config import sc
from particle import AreaParticle

chain_image = pygame.image.load("Assets/sprite/enemy/chain/1.png").convert_alpha()
chain_image = pygame.transform.scale(chain_image, (170 * 3, 4 * 3))

particle_break = pygame.image.load("Assets/sprite/particle/break/1.png").convert_alpha()

class Chain(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = chain_image
        self.rect = self.image.get_rect(center=(1200, randint(133, 565)))

        self.add(enemy_group)

        self.time_start = 0

        self.isChain = True

        self.isTrack = False

        self.time_damage_set = 0

        self.isBoss = False
    
    def update(self):
        self.time_damage_set += 1
        self.time_damage_set = max(0, min(8, self.time_damage_set))
        if self.time_start >= 155 and self.time_start < 245:
            if self.rect.x > 352:
                self.rect.x -= 36
            else:
                if not self.isTrack:
                    for i in range(randint(30, 40)):
                        AreaParticle(self.rect.x-randint(-10, 10), self.rect.centery, particle_break, (randint(9, 14), 3), alpha=300)
                    self.isTrack = True
        if self.time_start < 158:
            pygame.draw.rect(sc, (220, 26, 26), (355, self.rect.centery, 1005, 4))
        if self.time_start >= 240:
            self.rect.x += 11
            if self.rect.x >= 805:
                self.kill()
        if self.time_start < 125 and self.rect.x > 809:
            self.rect.x -= 1
        self.time_start += 1
