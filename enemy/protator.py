import math
from random import randint

import pygame

from config import WINDOW_SIZE
from group_conf import enemy_group
from animator import Animator
from bullet import bullet
from scene import scene_config, scene_tutorial
from item import ammo
from anim_effect import Effect
from txt_spawn_collect import TXT
from cartoon_splash import Splash

protator_sprite = [pygame.image.load("Assets/sprite/enemy/protator/1.png").convert_alpha(),
                   pygame.image.load("Assets/sprite/enemy/protator/2.png").convert_alpha(),
                   pygame.image.load("Assets/sprite/enemy/protator/3.png").convert_alpha(),
                   pygame.image.load("Assets/sprite/enemy/protator/4.png").convert_alpha(),
                   pygame.image.load("Assets/sprite/enemy/protator/5.png").convert_alpha(),
                   pygame.image.load("Assets/sprite/enemy/protator/6.png").convert_alpha()]


class Protator(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = protator_sprite[0]
        self.image = pygame.transform.scale(self.image, (10*2, 21*2))
        self.rect = self.image.get_rect(center=(x, y))

        self.health = 15

        self.add(enemy_group)

        self.maxDown = randint(122, 400)

        self.animator = Animator(11, protator_sprite)

        self.spawn_time = pygame.time.get_ticks()
        self.direct_time_max = randint(275, 295)

        self.timeShoot: int = 0

        self.lucky = randint(0, 1)

        self.type_enemy = 0

        self.move_y = randint(-1, 1)

        self.isChain = False

        self.time_damage_set = 0

        self.isBoss = False

    def shoot(self):
        if self.timeShoot >= 59:
            bullet.Bullet(self.rect.centerx, self.rect.centery, randint(-3, 3), randint(-3, 3),
                          bullet.bullet_Sprite[1], (17, 17), damage=scene_config.enemy_bullet_damage)
            self.timeShoot = 0
            return
        self.timeShoot += 1

    def update(self):
        self.time_damage_set += 1
        self.time_damage_set = max(0, min(8, self.time_damage_set))
        if self.health <= 0:
            scene_config.p.kill_count += 1
            scene_tutorial.select_tutorial = 4
            scene_tutorial.index_txt = 0
            scene_config.p.SCORE += 95
            Effect(self.rect.centerx, self.rect.centery, scene_config.enemy_die_effect_one, (42, 42), 8)
            if self.lucky == 0 or scene_config.p.isFirstRun:
                ammo.Ammo(self.rect.centerx, self.rect.centery)
            Splash("KILL", self.rect.center)
            self.kill()
            return

        elif self.rect.y < self.maxDown:
            self.rect.y += 2
        else:
            self.shoot()

            change_direction_time = self.direct_time_max
            time = (self.spawn_time + pygame.time.get_ticks()) / change_direction_time * math.pi
            movement_direction_x = round(math.copysign(1, math.sin(time)))
            movement_direction_y = round(math.copysign(self.move_y, math.sin(time)))

            self.rect.x += round(movement_direction_x)
            self.rect.y += round(movement_direction_y)

        self.image = self.animator.update()
        self.image = pygame.transform.scale(self.image, (10*2, 21*2))
