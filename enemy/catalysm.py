import pygame
from group_conf import enemy_group
from random import randint
from animator import Animator

img = [pygame.image.load("Assets/sprite/enemy/catalysm/1.png").convert_alpha(),
        pygame.image.load("Assets/sprite/enemy/catalysm/2.png").convert_alpha(),
        pygame.image.load("Assets/sprite/enemy/catalysm/3.png").convert_alpha(),
        pygame.image.load("Assets/sprite/enemy/catalysm/4.png").convert_alpha(),
        pygame.image.load("Assets/sprite/enemy/catalysm/5.png").convert_alpha(),
        pygame.image.load("Assets/sprite/enemy/catalysm/4.png").convert_alpha(),
        pygame.image.load("Assets/sprite/enemy/catalysm/3.png").convert_alpha(),
        pygame.image.load("Assets/sprite/enemy/catalysm/2.png").convert_alpha()]

for i in range(8):
    img[i] = pygame.transform.scale(img[i], (21 * 3, 11 * 3))

class Catalysm(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = img[0]
        self.rect = self.image.get_rect(center=(randint(375, 820), 5))

        self.health = randint(100, 150)

        self.anim = Animator(8, img)

        self.time_fast_move = 0
        self.time_start_fast_move = 0
        
        self.time_start_move = 0
        self.max_time_start = randint(25, 75)


        self.move_strike = [0, 0]

        self.count_fast_move = 0

        self.time_damage_set = 9

        self.isChain = False

        self.isBoss = False

        self.type_enemy = 0

        self.add(enemy_group)
    
    def update(self):
        self.image = self.anim.update()
        if self.time_start_move <= self.max_time_start:
            self.time_start_move += 1
            self.rect.y += 5
        else:
            self.time_fast_move += 1
            if self.time_start_fast_move > 95:
                if self.time_fast_move >= 9:
                    self.move_strike[0] = randint(350, 820)
                    self.move_strike[1] = randint(100, 560)
                    self.time_fast_move = 0
                    self.count_fast_move += 1
                if self.rect.x <= self.move_strike[0]:
                    self.rect.x += 11
                elif self.rect.x >= self.move_strike[0]:
                    self.rect.x -= 11

                if self.rect.y <= self.move_strike[1]:
                    self.rect.y += 11
                elif self.rect.y >= self.move_strike[1]:
                    self.rect.y -= 11

                if self.count_fast_move > 6:
                    self.count_fast_move = 0
                    self.time_start_fast_move = 0
            else:
                self.time_start_fast_move += 1

            self.rect.x = max(360, min(825, self.rect.x))
            self.rect.y = max(105, min(566, self.rect.y))
        
