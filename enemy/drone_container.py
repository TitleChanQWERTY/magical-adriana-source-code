from random import randint

import pygame

from anim_effect import Effect
from config import WINDOW_SIZE
from group_conf import enemy_group, bullet_enemy_group
from bullet import bullet
from item import container
from scene import scene_config
from particle import AreaParticle
from animator import Animator
from txt_spawn_collect import TXT
from cartoon_splash import Splash

sprite_drone = (pygame.image.load("Assets/sprite/enemy/drone_container/1.png").convert_alpha(),
                pygame.image.load("Assets/sprite/enemy/drone_container/2.png").convert_alpha(),
                pygame.image.load("Assets/sprite/enemy/drone_container/3.png").convert_alpha(),
                pygame.image.load("Assets/sprite/enemy/drone_container/2.png").convert_alpha())


drone_boom = (pygame.image.load("Assets/sprite/effect/enemy_die/3/1.png").convert_alpha(),
                pygame.image.load("Assets/sprite/effect/enemy_die/3/2.png").convert_alpha(),
                pygame.image.load("Assets/sprite/effect/enemy_die/3/3.png").convert_alpha(),
                pygame.image.load("Assets/sprite/effect/enemy_die/3/4.png").convert_alpha(),
                pygame.image.load("Assets/sprite/effect/enemy_die/3/5.png").convert_alpha())

class DroneContainer(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = sprite_drone[0]
        self.image = pygame.transform.scale(self.image, (28 * 2, 39 * 2))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(enemy_group)

        self.health = randint(45, 55)

        self.maxDown = randint(120, 350)

        self.anim = Animator(10, sprite_drone)

        self.time_bullet: int = 0

        self.time_bullet_shoot: int = 0

        self.count_bullet: int = 0

        self.type_enemy = 1

        self.lucky: int = randint(0, 2)

        self.isChain = False

        self.time_damage_set = 0

        self.isBoss = False

    def shoot(self):
        if self.time_bullet >= 118:
            if self.time_bullet_shoot > 19:
                bullet.Bullet(self.rect.centerx, self.rect.centery+24, -2, 4, bullet.bullet_Sprite[2],
                              (8 * 2, 12 * 2), bullet_enemy_group, scene_config.enemy_bullet_damage)
                bullet.Bullet(self.rect.centerx, self.rect.centery + 25, 0, 8, bullet.bullet_Sprite[2],
                              (9 * 2, 13 * 2), bullet_enemy_group, scene_config.enemy_bullet_damage)
                bullet.Bullet(self.rect.centerx, self.rect.centery + 24, 2, 4, bullet.bullet_Sprite[2],
                              (8 * 2, 12 * 2), bullet_enemy_group, scene_config.enemy_bullet_damage)
                self.count_bullet += 1
                self.time_bullet_shoot = 0
                return
            else:
                self.time_bullet_shoot += 1
            if self.count_bullet >= 2:
                self.count_bullet = 0
                self.time_bullet_shoot = 0
                self.time_bullet = 0
        self.time_bullet += 1

    def update(self):
        self.time_damage_set += 1
        self.time_damage_set = max(0, min(8, self.time_damage_set))
        self.shoot()
        if self.health <= 0:
            Splash("KILL", self.rect.center)
            scene_config.p.kill_count += 1
            scene_config.p.SCORE += randint(30, 55)
            for i in range(randint(15, 25)):
                size = randint(5, 17)
                AreaParticle(self.rect.centerx, self.rect.centery,
                             scene_config.simple_particle_sprite[randint(0, 2)], (size, size))
            if self.lucky == 0:
                container.Container(self.rect.centerx, self.rect.centery)
            elif self.lucky == 1:
                container.ContainerHealth(self.rect.centerx, self.rect.centery)
            Effect(self.rect.centerx, self.rect.centery, scene_config.enemy_die_effect_two, (23*2, 30*2), 11)
            Effect(self.rect.centerx, self.rect.centery, drone_boom, (16*8, 16*8), 5)
            self.kill()
            return

        self.rect.y += 2
        self.rect.y = min(self.rect.y, self.maxDown)

        self.image = self.anim.update()
        self.image = pygame.transform.scale(self.image, (28 * 2, 39 * 2))
