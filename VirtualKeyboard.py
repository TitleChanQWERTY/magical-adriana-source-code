import pygame
import gamepad_calibrate
import language_system
from scene import scene_config


key_pack = ["1234567890<",
            "QWERTYUIOP",
            "ASDFGHJKL",
            "ZXCVBNM"]

key_txt = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0","<-",
            "q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
            "a", "s", "d", "f", "g", "h", "j", "k", "l",
            "z", "x", "c", "v", "b", "n", "m"]

class KEYDRAW:
    def __init__(self, x, y, key, size, font):
        self.key_surf = pygame.Surface((size, size))
        self.key_rect = self.key_surf.get_rect(center=(x, y))
        self.size = size
        self.font = font
        self.key = str(key)
        self.x = x
        self.y = y

        self.alpha = 300

        self.font_draw = pygame.font.Font(self.font, self.size-5)

    def update(self, rect):
        self.key_surf.fill((15, 15, 15))
        pygame.draw.rect(self.key_surf, (255, 255, 255), (0, 0, self.key_surf.get_width(), self.key_surf.get_height()), 2)
        key_txt = self.font_draw.render(str(self.key), scene_config.AA_TEXT, (255, 255, 255))
        self.key_surf.blit(key_txt, key_txt.get_rect(center=(self.key_surf.get_width()/2-2, self.key_surf.get_height()/2+3)))
        if rect.colliderect(self.key_rect):
            self.alpha -= 19
        else:
            self.alpha += 20
        self.alpha = max(85, min(300, self.alpha))
        self.key_surf.set_alpha(self.alpha)

class VKEY:
    def __init__(self, size_key, font, surf_draw, pos_x, pos_y, gamepad=None):        
        self.obj_draw = []

        select_key = 0

        self.surf_draw = surf_draw

        self.pos_y = pos_y
        self.pos_x = pos_x

        for y, row in enumerate(key_pack):
            for x, tile in enumerate(row):
                self.obj_draw.append(KEYDRAW(pos_x + x * 53, pos_y + y * 56, key_txt[select_key], size_key, font))
                select_key += 1

        self.isMouse = False
        self.isGamepad = False

        self.gamepad = gamepad

        self.posx = surf_draw.get_width()/2
        self.posy = surf_draw.get_height()/2

        self.rect_mouse = pygame.Rect(self.posx, self.posy, 5, 5)

        self.font = font

    def mouse_gamepad_check(self, e):
        if e.type == pygame.MOUSEMOTION:
            self.isMouse = True
            self.isGamepad = False
        elif e.type == pygame.JOYAXISMOTION:
            self.isGamepad = True
            self.isMouse = False
    def check_tap(self, txt):
        for i, b in enumerate(self.obj_draw):
            if self.rect_mouse.colliderect(self.obj_draw[i].key_rect):
                return i
    def update(self):
        if gamepad_calibrate.input_sys >= 1:
            txt_tip = pygame.font.Font(self.font, 18).render(language_system.txt_lang["TIP_V_KEYBOARD_1"]+" R1", scene_config.AA_TEXT, (245, 0, 245))
        else:
            txt_tip = pygame.font.Font(self.font, 18).render(language_system.txt_lang["TIP_V_KEYBOARD_1"]+" RB", scene_config.AA_TEXT, (245, 0, 245))
        self.surf_draw.blit(txt_tip, txt_tip.get_rect(center=(self.pos_x+263, self.pos_y-39)))
        if self.isMouse:
            mouse_pos = pygame.mouse.get_pos()
            self.posx = mouse_pos[0]
            self.posy = mouse_pos[1]
        elif self.isGamepad and pygame.joystick.get_count() > 0:
            self.posx += self.gamepad.get_axis(0) * 6
            self.posy += self.gamepad.get_axis(1) * 6
        self.rect_mouse = pygame.Rect(self.posx, self.posy, 5, 5)
        for i, b in enumerate(self.obj_draw):
            self.obj_draw[i].update(self.rect_mouse)
            self.surf_draw.blit(self.obj_draw[i].key_surf, self.obj_draw[i].key_rect)
        pygame.draw.circle(self.surf_draw, (245, 0, 205), (self.rect_mouse.x, self.rect_mouse.y), 15, 5)

