from random import randint

import pygame

from bullet import bullet
from group_conf import bullet_player_group, electro_line_group
from scene import scene_config
from sfx_colection import player_electro_shoot, player_pistol_shoot, chanel_player_shoot, player_minigun_shoot, player_shotgun_shoot, player_standart_shoot
from player import Player
from config import sc, joystick_m
from progres_bar import ProgressBar

player_bullet = {"1": pygame.image.load("Assets/sprite/bullet/player/1.png").convert_alpha(),
                 "2": pygame.image.load("Assets/sprite/bullet/player/2.png").convert_alpha(),
                 "3": pygame.image.load("Assets/sprite/bullet/player/3.png").convert_alpha(),
                 "4": pygame.image.load("Assets/sprite/bullet/player/4.png").convert_alpha(),
                 "5": pygame.image.load("Assets/sprite/bullet/player/electro_line/1.png").convert_alpha(),
                 "6": pygame.image.load("Assets/sprite/bullet/player/electro_line/2.png").convert_alpha()}

gun_splash = [pygame.image.load("Assets/sprite/effect/gun_splash/electro.png").convert_alpha(),
              pygame.image.load("Assets/sprite/effect/gun_splash/pistol.png").convert_alpha(),
              pygame.image.load("Assets/sprite/effect/gun_splash/standart.png").convert_alpha(),
              pygame.image.load("Assets/sprite/effect/gun_splash/minigun.png").convert_alpha(),
              pygame.image.load("Assets/sprite/effect/gun_splash/shotgun.png").convert_alpha()]

gun_splash[0] = pygame.transform.scale(gun_splash[0], (16 * 2, 16 * 2))

gun_splash[1] = pygame.transform.scale(gun_splash[1], (16 * 2, 14 * 2))

gun_splash[2] = pygame.transform.scale(gun_splash[2], (14 * 2, 7 * 2))

gun_splash[3] = pygame.transform.scale(gun_splash[3], (9 * 2, 15 * 2))

gun_splash[4] = pygame.transform.scale(gun_splash[4], (10 * 2, 16 * 2))


class Standart_weapon:
    def __init__(self, isAim=False, isStabilization=False, isBombShoot=False, isOn=True):
        self.timeShoot: int = 0

        self.isAim: bool = isAim

        self.isStabilization: bool = isStabilization

        self.isBombShoot: bool = isBombShoot

        self.isOn: bool = isOn

        self.bombCount: int = 3

        self.name_w = "st_weapon"

    def shoot(self, rect):
        scene_config.p.WeaponIcon = pygame.image.load("Assets/sprite/item/weapon/standart.png").convert_alpha()
        scene_config.p.WeaponIcon = pygame.transform.scale(scene_config.p.WeaponIcon, (40, 46))
        scene_config.p.rectWeaponIcon.x = 263
        if pygame.key.get_pressed()[scene_config.control_keyboard["shoot"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["B"]):
            if self.timeShoot > 9:
                sc.blit(gun_splash[2], gun_splash[2].get_rect(center=(rect.centerx, rect.centery - 13)))
            if self.timeShoot > 10:
                chanel_player_shoot.play(player_standart_shoot)
                if not self.isAim:
                    scene_config.p.DAMAGE = 4
                else:
                    scene_config.p.DAMAGE = 6
                move_bullet = 1
                if not self.isStabilization:
                    move_bullet = randint(-11, 12)
                bullet.Bullet(rect.centerx + move_bullet, rect.centery - 5, 0, -12, player_bullet["1"], (12, 12),
                              bullet_player_group)
                self.timeShoot = 0
        self.timeShoot += 1


class MagicPistol:
    def __init__(self, isAim=False, isStabilization=False, isBombShoot=False, isOn=True):
        self.timeShoot: int = 0

        self.isAim: bool = isAim

        self.isStabilization: bool = isStabilization

        self.isBombShoot: bool = isBombShoot

        self.isOn: bool = isOn

        self.bombCount: int = randint(3, 4)

        self.name_w = "pistol"

    def shoot(self, rect):
        scene_config.p.WeaponIcon = pygame.image.load("Assets/sprite/item/weapon/pistiol.png").convert_alpha()
        scene_config.p.WeaponIcon = pygame.transform.scale(scene_config.p.WeaponIcon, (55, 42))
        scene_config.p.rectWeaponIcon.x = 255
        if pygame.key.get_pressed()[scene_config.control_keyboard["shoot"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["B"]):
            if scene_config.p.AMMO > 0:
                if self.timeShoot >= 9:
                    sc.blit(gun_splash[1], gun_splash[1].get_rect(center=(rect.centerx, rect.centery - 18)))
                if self.timeShoot >= 11:
                    chanel_player_shoot.play(player_pistol_shoot)
                    if not self.isAim:
                        scene_config.p.DAMAGE = 8
                    else:
                        scene_config.p.DAMAGE = 11
                    move_bullet = 1
                    if not self.isStabilization:
                        move_bullet = randint(-11, 12)
                    scene_config.p.AMMO -= 1
                    scene_config.p.yPosAmmoTXT = 319
                    bullet.Bullet(rect.centerx + move_bullet, rect.centery-10, 0, -20, player_bullet["2"], (13, 14),
                                  bullet_player_group)
                    self.timeShoot = 0
            self.timeShoot += 1


class MagicTrident:
    def __init__(self, isAim=False, isStabilization=False, isBombShoot=False, isOn=True):
        self.timeShoot: int = 0

        self.isAim: bool = isAim

        self.isStabilization: bool = isStabilization

        self.isBombShoot: bool = isBombShoot

        self.isOn: bool = isOn

        self.bombCount: int = 4

        self.name_w = "trident"

    def shoot(self, rect):
        scene_config.p.WeaponIcon = pygame.image.load("Assets/sprite/item/weapon/electro_trident.png").convert_alpha()
        scene_config.p.WeaponIcon = pygame.transform.scale(scene_config.p.WeaponIcon, (13 * 4, 13 * 4))
        scene_config.p.rectWeaponIcon.x = 255
        if pygame.key.get_pressed()[scene_config.control_keyboard["shoot"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["B"]):
            if self.timeShoot > 1 and scene_config.p.AMMO > 0:
                scene_config.screen_shake = 2
                if not self.isAim:
                    scene_config.p.DAMAGE = 1
                else:
                    scene_config.p.DAMAGE = 2
                if not self.isStabilization:
                    scene_config.power_shake = 3
                    move_bullet = randint(-5, 5)
                else:
                    scene_config.power_shake = 2
                    move_bullet = randint(-1, 1)
                if scene_config.p.time_take_damage >= scene_config.p.max_time_take_damage:
                    scene_config.power_shake += randint(2, 3)
                scene_config.p.AMMO -= 1
                scene_config.p.yPosAmmoTXT = 319
                bullet.Bullet(rect.centerx - 6 + move_bullet, rect.centery - 270, 0, 0,
                              player_bullet[str(randint(5, 6))],
                              (10, 525), electro_line_group, time_destroy=1)
                bullet.Bullet(rect.centerx + 6 + move_bullet, rect.centery - 270, 0, 0,
                              player_bullet[str(randint(5, 6))],
                              (10, 525), electro_line_group, time_destroy=1)
                chanel_player_shoot.play(player_electro_shoot)
                sc.blit(gun_splash[0], gun_splash[0].get_rect(center=(rect.centerx, rect.centery - 20)))
                self.timeShoot = 0
            self.timeShoot += 1


class MagicShotgun:
    def __init__(self, isAim=False, isStabilization=False, isBombShoot=False, isOn=True):
        self.timeShoot: int = 0

        self.isAim: bool = isAim

        self.isStabilization: bool = isStabilization

        self.isBombShoot: bool = isBombShoot

        self.isOn: bool = isOn

        self.bombCount: int = 5

        self.name_w = "shotgun"

        self.progres_shoor = ProgressBar(0, 0, (28, 9), (195, 0, 195))

    def shoot(self, rect):
        scene_config.p.WeaponIcon = pygame.image.load("Assets/sprite/item/weapon/shutgun.png").convert_alpha()
        scene_config.p.WeaponIcon = pygame.transform.scale(scene_config.p.WeaponIcon, (80, 41))
        scene_config.p.rectWeaponIcon.x = 239
        if pygame.key.get_pressed()[scene_config.control_keyboard["shoot"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["B"]):
            if scene_config.p.AMMO > 0:
                if self.timeShoot >= 25:
                    sc.blit(gun_splash[1], gun_splash[1].get_rect(center=(rect.centerx, rect.centery - 22)))
                    sc.blit(gun_splash[4], gun_splash[4].get_rect(center=(rect.centerx, rect.centery - 19)))
                if self.timeShoot >= 27:
                    self.timeShoot = 0
                    if not self.isAim:
                        scene_config.p.DAMAGE = randint(11, 14)
                    else:
                        scene_config.p.DAMAGE = randint(15, 17)
                    move_bullet = 1
                    if not self.isStabilization:
                        Player.Player_Image_Move(scene_config.p.rect.centerx, scene_config.p.rect.centery)
                        scene_config.p.rect.y += randint(15, 17)
                        move_bullet = randint(-16, 17)

                    scene_config.p.AMMO -= 3
                    scene_config.p.yPosAmmoTXT = 319
                    chanel_player_shoot.play(player_shotgun_shoot)
                    bullet.Bullet(rect.centerx + 1 + move_bullet, rect.centery - 35, 0, -25, player_bullet["3"], (17, 17),
                                  bullet_player_group)
                    bullet.Bullet(rect.centerx - 23 + move_bullet, rect.centery - 40, 0, -25, player_bullet["3"], (17, 17),
                                  bullet_player_group)
                    bullet.Bullet(rect.centerx + 24 + move_bullet, rect.centery - 40, 0, -25, player_bullet["3"], (17, 17),
                                  bullet_player_group)
            if self.timeShoot < 27:
                self.timeShoot += 1
        self.progres_shoor.rect.x = scene_config.p.rect.x-18
        self.progres_shoor.rect.y = scene_config.p.rect.centery - 36
        self.progres_shoor.update(self.timeShoot, sc)


class MagicMinigun:
    def __init__(self, isAim=False, isStabilization=False, isBombShoot=False, isOn=True):
        self.timeShoot: int = 0

        self.isAim: bool = isAim

        self.isStabilization: bool = isStabilization

        self.isBombShoot: bool = isBombShoot

        self.isOn: bool = isOn

        self.bombCount: int = 2

        self.name_w = "minigun"

    def shoot(self, rect):
        scene_config.p.WeaponIcon = pygame.image.load("Assets/sprite/item/weapon/minigun.png").convert_alpha()
        scene_config.p.WeaponIcon = pygame.transform.scale(scene_config.p.WeaponIcon, (102, 42))
        scene_config.p.rectWeaponIcon.x = 228
        if pygame.key.get_pressed()[scene_config.control_keyboard["shoot"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["B"]):
            if self.timeShoot > 6 and scene_config.p.AMMO > 0:
                self.timeShoot = 0
                scene_config.p.AMMO -= 1
                scene_config.p.yPosAmmoTXT = 319
                if not self.isAim:
                    scene_config.p.DAMAGE = 3
                else:
                    scene_config.p.DAMAGE = 4
                move_bullet = 1
                if not self.isStabilization:
                    Player.Player_Image_Move(scene_config.p.rect.centerx, scene_config.p.rect.centery - 2)
                    move_bullet = randint(-21, 26)
                    scene_config.p.rect.y += randint(3, 4)
                for i in range(2):
                    bullet.Bullet(rect.centerx + randint(-9, 10) + move_bullet, rect.centery - 25, randint(-2, 2), -30,
                                  player_bullet["4"], (8, 8),
                                  bullet_player_group)
                sc.blit(gun_splash[3], gun_splash[3].get_rect(center=(rect.centerx, rect.centery - 25)))
                bullet.Bullet(rect.centerx, rect.centery - 25, 0, -30, player_bullet["4"], (9, 9),
                              bullet_player_group)
                chanel_player_shoot.play(player_minigun_shoot)
            self.timeShoot += 1
