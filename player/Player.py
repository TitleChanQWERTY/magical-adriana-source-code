from random import randint

import pygame
import language_system

from config import sc, WINDOW_SIZE, standart_font, joystick_m
from scene import scene_config
from group_conf import particle_group, txt_spawn
from item import ammo
from bullet import bomb
from animator import Animator
from sfx_colection import player_switch_gun, max_magical_voice, chanel_voice
from txt_spawn_collect import TXT

HeartImage = pygame.image.load("Assets/sprite/ui/icon/heatlh.png").convert_alpha()
HeartImage = pygame.transform.scale(HeartImage, (25, 25))

ShieldImage = pygame.image.load("Assets/sprite/ui/icon/shield.png").convert_alpha()
ShieldImage = pygame.transform.scale(ShieldImage, (25, 25))

MindImage = pygame.image.load("Assets/sprite/ui/icon/mind.png").convert_alpha()
MindImage = pygame.transform.scale(MindImage, (34, 34))

player_sprite = pygame.image.load("Assets/sprite/player/1.png").convert_alpha()

fire_sprite = (pygame.image.load("Assets/sprite/effect/fire_player/1.png").convert_alpha(),
               pygame.image.load("Assets/sprite/effect/fire_player/2.png").convert_alpha(),
               pygame.image.load("Assets/sprite/effect/fire_player/3.png").convert_alpha())

upgrade_icon = [pygame.image.load("Assets/sprite/weapon_upgrade_set/1.png").convert_alpha(),
                pygame.image.load("Assets/sprite/weapon_upgrade_set/2.png").convert_alpha(),
                pygame.image.load("Assets/sprite/weapon_upgrade_set/3.png").convert_alpha()]

upgrade_icon[0] = pygame.transform.scale(upgrade_icon[0], (11 * 2, 13 * 2))
upgrade_icon[1] = pygame.transform.scale(upgrade_icon[1], (9 * 2, 8 * 2))
upgrade_icon[2] = pygame.transform.scale(upgrade_icon[2], (11 * 2, 11 * 2))


class Player_Image_Move(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = player_sprite
        self.image = pygame.transform.scale(self.image, (15, 22))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.alpha = 145

        self.time_start_alpha: int = 0
        self.time_start_alpha_max = 7

    def update(self):
        if self.alpha <= 19:
            self.kill()
            return 0
        elif self.time_start_alpha >= self.time_start_alpha_max:
            self.alpha -= 15
        else:
            self.time_start_alpha += 1
        self.image.set_alpha(self.alpha)


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y, speed):
        super().__init__()
        self.image = player_sprite
        self.image = pygame.transform.scale(self.image, (20, 27))
        self.rect = self.image.get_rect(center=(x, y))

        self.NAME = None
        self.AGE = None
        self.isFirstRun = None

        self.kill_count = 0

        self.HEALTH = 100
        self.AMMO = 150
        self.ARMOR = 100
        self.MIND = 0

        self.MONEY = 0
        self.SCORE = 0
        self.HISCORE = 0
        self.EXP = 0
        self.RANK = 0

        self.DAMAGE = 3

        self.BOSS_KILL_COUNT = 0

        self.damage_st = self.DAMAGE

        self.st_speed = speed - 1

        self.maxSpeed = speed

        self.speed = [0, 0, 0, 0]

        self.select_weapon = 0

        self.weapon = []
        self.active_weapon = []

        self.WeaponIcon = pygame.image.load("Assets/sprite/item/weapon/standart.png").convert_alpha()
        self.rectWeaponIcon = self.WeaponIcon.get_rect(center=(265, 311))

        self.timeDamageMind: int = 0

        ammo_sprite = ammo.image_ammo
        self.ammo_sprite = pygame.transform.scale(ammo_sprite, (19, 20))

        self.time_take_damage = 0
        self.max_time_take_damage = 850

        self.color_back = [29, 2, 21]

        self.anim_fire = Animator(7, fire_sprite)

        self.time_up_txt_magical: int = 0
        self.yPosMagicalTXT = WINDOW_SIZE[1] // 2

        self.yPosAmmoTXT = 334

        self.alphaMind = 0

        self.egg = 0

        self.profile = None

        self.yPosWeapon = 311

        self.size_heart = 25

        self.time_heart = 0

        self.get_shield = 0

        self.shield_image = (ShieldImage, ShieldImage, ShieldImage)
        self.shield_image_pos = (WINDOW_SIZE[0]/2-70, WINDOW_SIZE[0]/2, WINDOW_SIZE[0]/2+70)

        self.isShieldActive = False

        self.size_shield = 25
        self.time_shield = 0

        self.isDash = False

        self.yPosMagicalPower = 135

        self.time_shoot_bomb = 0

        self.isMaxVoice = False

        self.paper_story = ["paper_0"]

        self.stat_player_font = pygame.font.Font(standart_font, 22)

        self.money_font = pygame.font.Font(standart_font, 19)
        self.bomb_font = pygame.font.Font(standart_font, 20 - len(language_system.txt_lang["BOMB"]))

    def set_weapon(self):
        self.active_weapon.clear()
        for weapon in self.weapon:
            if weapon.isOn:
                self.active_weapon.append(weapon)

    def update(self, activeScene):
        if self.SCORE >= self.HISCORE:
            self.HISCORE = self.SCORE

        if self.HEALTH > 0:
            key = pygame.key.get_pressed()
            sc.blit(scene_config.aim_image[scene_config.select_aim],
                    scene_config.aim_image[scene_config.select_aim].get_rect(
                        center=(self.rect.centerx, self.rect.centery - 50)))
            self.shoot()
            self.move(key)

        HealthTXT = self.stat_player_font.render(str(self.HEALTH),
                                                    scene_config.AA_TEXT,
                                                    (255, 15, 25))

        ArmorTXT = self.stat_player_font.render(str(self.ARMOR),
                                                    scene_config.AA_TEXT,
                                                    (255, 145, 255))

        MindTXT = self.stat_player_font.render(str(self.MIND),
                                                scene_config.AA_TEXT,
                                                (255, 145, 255))

        if self.time_take_damage < self.max_time_take_damage:
            MagicalPowerTXT = self.money_font.render("-> "+
                language_system.txt_lang["MAGICAL_POWER"] + ": " + str(self.time_take_damage)+"\\"+str(self.max_time_take_damage)+" <-", scene_config.AA_TEXT,
                (255, 155, 255))
        else:
            MagicalPowerTXT = self.money_font.render(
                language_system.txt_lang["MAGICAL_POWER"] + ": MAX!", scene_config.AA_TEXT, (255, 255, 255))

        self.yPosMagicalPower -= 2
        self.yPosMagicalPower = max(125, min(136, self.yPosMagicalPower))

        sc.blit(MagicalPowerTXT, MagicalPowerTXT.get_rect(center=(WINDOW_SIZE[0] // 2, self.yPosMagicalPower)))
        
        self.yPosWeapon += 4
        self.yPosWeapon = max(295, min(309, self.yPosWeapon))
        self.rectWeaponIcon.y = self.yPosWeapon

        if activeScene:

            self.time_shoot_bomb += 1
            self.time_shoot_bomb = max(0, min(16, self.time_shoot_bomb))

            sc.blit(HealthTXT, (WINDOW_SIZE[0] // 2, 634))
            sc.blit(ArmorTXT, (WINDOW_SIZE[0] // 2 - 154, 639))
            sc.blit(MindTXT, (WINDOW_SIZE[0] // 2 + 144, 639))
            
            shield = ShieldImage
            shield = pygame.transform.scale(shield, (self.size_shield, self.size_shield))

            self.size_shield -= 1
            self.size_shield = max(25, min(54, self.size_shield))

            sc.blit(shield, shield.get_rect(center=(WINDOW_SIZE[0] / 2-172, 649)))
            sc.blit(MindImage, (WINDOW_SIZE[0] // 2 + 114, 629))

            if self.HEALTH <= 55:
                if self.time_heart <= 47:
                    self.size_heart -= 1
                    self.time_heart += 1
                else:
                    self.size_heart += 45
                    self.time_heart = 0
                self.size_heart = max(25, min(60, self.size_heart))
            else:
                self.size_heart = 25
                 
            heart_image = HeartImage
            heart_image = pygame.transform.scale(heart_image, (self.size_heart, self.size_heart))

            sc.blit(heart_image, heart_image.get_rect(center=((WINDOW_SIZE[0] / 2-21, 644))))

            moneyTXT = self.money_font.render(
                language_system.txt_lang["MONEY"] + ": " + str(self.MONEY), scene_config.AA_TEXT, (0, 250, 0))

            sc.blit(moneyTXT, moneyTXT.get_rect(center=(WINDOW_SIZE[0] // 2 - 160, 86)))

            WeaponTXT = self.stat_player_font.render(
                language_system.txt_lang["WEAPON"],
                scene_config.AA_TEXT,
                (255, 255, 255))

            sc.blit(WeaponTXT, WeaponTXT.get_rect(center=(275, 247)))

            sc.blit(self.WeaponIcon, self.rectWeaponIcon)

            sc.blit(self.ammo_sprite, self.ammo_sprite.get_rect(center=(231 * 4, 297)))

            for sh in range(self.get_shield):
                sc.blit(self.shield_image[sh], self.shield_image[sh].get_rect(center=(self.shield_image_pos[sh], 565)))

            if self.AMMO <= 0:
                color_ammo = (240, 0, 0)
            else:
                color_ammo = (255, 245, 255)
            AmmoTXT = self.stat_player_font.render(
                language_system.txt_lang["AMMO"],
                scene_config.AA_TEXT,
                (255, 255, 255))

            AmmoCountTXT = pygame.font.Font(standart_font, 30 - len(str(self.AMMO))).render(str(self.AMMO),
                                                                                            scene_config.AA_TEXT,
                                                                                            color_ammo)

            sc.blit(AmmoTXT, AmmoTXT.get_rect(center=(231 * 4, 247)))

            self.yPosAmmoTXT += 5

            self.yPosAmmoTXT = max(230, min(337, self.yPosAmmoTXT))

            sc.blit(AmmoCountTXT, AmmoCountTXT.get_rect(center=(231 * 4, self.yPosAmmoTXT)))

            pygame.draw.rect(sc, (255, 165, 255), (395, 610, 405, 75), 1)
            pygame.draw.rect(sc, (245, 155, 245), (214, 260, 125, 140), 2)
            pygame.draw.rect(sc, (245, 155, 245), (216 * 4, 260, 125, 140), 2)

            self.AMMO = max(self.AMMO, 0)
            self.ARMOR = max(self.ARMOR, 0)

            if self.active_weapon[self.select_weapon].isAim:
                sc.blit(upgrade_icon[0], upgrade_icon[0].get_rect(center=(235, 380)))

            if self.active_weapon[self.select_weapon].isStabilization:
                sc.blit(upgrade_icon[1], upgrade_icon[1].get_rect(center=(280, 380)))

            if self.active_weapon[self.select_weapon].isBombShoot:
                sc.blit(upgrade_icon[2], upgrade_icon[2].get_rect(center=(320, 380)))
                BombTXT = self.bomb_font.render(
                    language_system.txt_lang["BOMB"] + ": " + str(self.active_weapon[self.select_weapon].bombCount),
                    scene_config.AA_TEXT,
                    (255, 255, 255))
                sc.blit(BombTXT, BombTXT.get_rect(center=(276, 412)))

            ScoreTXT = pygame.font.Font(standart_font, 26 - len(language_system.txt_lang["SCORE"])).render(
                language_system.txt_lang["SCORE"] + ": " + str(self.SCORE),
                scene_config.AA_TEXT,
                (255, 255, 0))

            sc.blit(ScoreTXT, ScoreTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 86)))

            DamageTXT = pygame.font.Font(standart_font, 26 - len(language_system.txt_lang["DAMAGE"])).render(
                language_system.txt_lang["DAMAGE"] + ": " + str(self.damage_st),
                scene_config.AA_TEXT,
                (255, 255, 255))

            sc.blit(DamageTXT, DamageTXT.get_rect(center=(WINDOW_SIZE[0] // 2 + 160, 86)))

            txt_spawn.draw(sc)
            txt_spawn.update()

    def switch_gun(self, event):
        if self.HEALTH > 0:
            if event.type == pygame.KEYDOWN:
                self.set_weapon()
                if len(self.active_weapon) > 1:
                    if event.key == scene_config.control_keyboard["prev_weapon"]:
                        player_switch_gun.play()
                        self.yPosWeapon -= 20
                        self.select_weapon -= 1
                        self.select_weapon %= len(self.active_weapon)
                        return
                    elif event.key == scene_config.control_keyboard["next_weapon"]:
                        player_switch_gun.play()
                        self.yPosWeapon -= 20
                        self.select_weapon += 1
                        self.select_weapon %= len(self.active_weapon)
                        return
                    if self.select_weapon >= len(self.active_weapon):
                        self.select_weapon = len(self.active_weapon) - 1
                if event.key == scene_config.control_keyboard["shoot_bomb"] and self.active_weapon[self.select_weapon].isBombShoot \
                        and self.active_weapon[self.select_weapon].bombCount > 0:
                    if self.time_shoot_bomb >= 15:
                        self.time_shoot_bomb = 0
                        self.active_weapon[self.select_weapon].bombCount -= 1
                        bomb.Bomb(self.rect.centerx, self.rect.centery - 10)
            elif event.type == pygame.JOYBUTTONDOWN:
                self.set_weapon()
                if len(self.active_weapon) > 1:
                    if event.button == scene_config.control_gamepad["L_BUMPER"]:
                        player_switch_gun.play()
                        self.yPosWeapon -= 20
                        self.select_weapon -= 1
                        self.select_weapon %= len(self.active_weapon)
                    elif event.button == scene_config.control_gamepad["R_BUMPER"]:
                        player_switch_gun.play()
                        self.yPosWeapon -= 20
                        self.select_weapon += 1
                        self.select_weapon %= len(self.active_weapon)
                    if self.select_weapon >= len(self.active_weapon):
                        self.select_weapon = len(self.active_weapon) - 1
                if event.button == scene_config.control_gamepad["Y"] and self.active_weapon[self.select_weapon].isBombShoot \
                        and self.active_weapon[self.select_weapon].bombCount > 0:
                    if self.time_shoot_bomb >= 15:
                        self.time_shoot_bomb = 0
                        self.active_weapon[self.select_weapon].bombCount -= 1
                        bomb.Bomb(self.rect.centerx, self.rect.centery - 10)


    def shoot(self):
        var = self.active_weapon[self.select_weapon]
        var.shoot(self.rect)

    def draw_fire(self):
        fire = self.anim_fire.update()
        fire.set_alpha(190)
        fire = pygame.transform.scale(fire, (13 * 2, 19 * 2))
        sc.blit(fire, fire.get_rect(center=(self.rect.centerx, self.rect.centery - 3)))

    def move(self, key):
        if not self.isFirstRun:
            if self.time_take_damage >= self.max_time_take_damage:
                if not self.isMaxVoice:
                    chanel_voice.play(max_magical_voice)
                    self.isMaxVoice = True
                if self.color_back[0] < 90 and self.color_back[2] < 90:
                    self.color_back[0] += 3
                    self.color_back[2] += 3
                elif self.color_back[1] < 8:
                    self.color_back[1] += 3

                self.maxSpeed = self.st_speed + 1
                Player_Image_Move(self.rect.centerx, self.rect.centery)
                self.damage_st = self.DAMAGE + 9

                color_magical = (randint(35, 255), randint(10, 255), randint(35, 255))

                max_magical_txt = pygame.font.Font(standart_font, 38).render(language_system.txt_lang["MAX_MAGICAL"],
                                                                             scene_config.AA_TEXT,
                                                                             color_magical)

                if self.time_up_txt_magical >= 60:
                    self.yPosMagicalTXT -= 10
                    self.yPosMagicalTXT = max(35, min(990, self.yPosMagicalTXT))
                else:
                    self.time_up_txt_magical += 1

                sc.blit(max_magical_txt, max_magical_txt.get_rect(center=(WINDOW_SIZE[0] // 2, self.yPosMagicalTXT)))

            else:
                self.isMaxVoice = False
                self.color_back = [29, 2, 21]
                self.damage_st = self.DAMAGE
                self.maxSpeed = self.st_speed
                self.time_up_txt_magical = 0
                self.yPosMagicalTXT = WINDOW_SIZE[1] // 2


        if not self.isDash:
            self.MIND += 3
        if self.MIND < 10:
            self.isDash = False
        elif self.MIND >= 100:
            self.isDash = True
        
        self.MIND = max(0, min(100, self.MIND))
        
        axis_x = 0
        axis_y = 0

        if pygame.joystick.get_count() > 0:
            axis_x = joystick_m.get_axis(0)
            axis_y = joystick_m.get_axis(1)

        if key[scene_config.control_keyboard["move_up"]] or axis_y <= -0.6:
            if self.isDash:
                if key[scene_config.control_keyboard["dash"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["X"]):
                    self.MIND -= 20
                    Player_Image_Move(self.rect.centerx, self.rect.centery)
                    self.rect.centery -= 14
            self.speed[0] = self.maxSpeed
            self.rect.y -= self.speed[0]
        else:
            if self.speed[0] >= 0:
                self.speed[0] -= 0.4
                self.rect.y -= self.speed[0]

        if key[scene_config.control_keyboard["move_down"]] or axis_y >= 0.6:
            if self.isDash:
                if key[scene_config.control_keyboard["dash"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["X"]):
                    self.MIND -= 20
                    Player_Image_Move(self.rect.centerx, self.rect.centery)
                    self.rect.centery += 14
            self.speed[1] = self.maxSpeed
            self.rect.y += self.speed[1]
        else:
            if self.speed[1] >= 0.1:
                self.speed[1] -= 0.4
                self.rect.y += self.speed[1]

        if key[scene_config.control_keyboard["move_left"]] or axis_x <= -0.6:
            if self.isDash:
                if key[scene_config.control_keyboard["dash"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["X"]):
                    self.MIND -= 20
                    Player_Image_Move(self.rect.centerx, self.rect.centery)
                    self.rect.centerx -= 14
            self.speed[2] = self.maxSpeed
            self.rect.x -= self.speed[2]
        else:
            if self.speed[2] >= 0:
                self.speed[2] -= 0.4
                self.rect.x -= self.speed[2]

        if key[scene_config.control_keyboard["move_right"]] or axis_x >= 0.6:
            if self.isDash:
                if key[scene_config.control_keyboard["dash"]] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["X"]):
                    self.MIND -= 20
                    Player_Image_Move(self.rect.centerx, self.rect.centery)
                    self.rect.centerx += 14
            self.speed[3] = self.maxSpeed
            self.rect.x += self.speed[3]
        else:
            if self.speed[3] >= 0:
                self.speed[3] -= 0.4
                self.rect.x += self.speed[3]
                

        self.rect.x = max(360, min(825, self.rect.x))
        self.rect.y = max(105, min(566, self.rect.y))
