import pygame
from group_conf import particle_group
from config import WINDOW_SIZE, standart_font, sc
from scene import scene_config
import language_system


class BossSplash(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.font.Font(standart_font, 30).render(language_system.txt_lang["BOSS_WARNING"], scene_config.AA_TEXT, (245, 0, 0))
        self.rect = self.image.get_rect(center=(WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2))

        
        self.add(particle_group)

        self.txt_draw = 0

        self.time_live = 0

        self.alpha = 300

        self.time_add = 0

    def update(self):
        if self.txt_draw <= len(language_system.txt_lang["BOSS_WARNING"]):
            if self.time_add >= 3:
                self.txt_draw += 1
                self.time_add = 0
            self.time_add += 1
        self.image = pygame.font.Font(standart_font, 30).render(language_system.txt_lang["BOSS_WARNING"][:self.txt_draw], scene_config.AA_TEXT, (200, 0, 0))

        if self.time_live >= 190:
            self.alpha -= 7
            self.image.set_alpha(self.alpha)
        if self.time_live >= 245:
            self.kill()
            return
        self.time_live += 1

