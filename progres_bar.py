import pygame


class ProgressBar:
    def __init__(self, x, y, size, color):
        self.size = size
        self.x = x
        self.y = y
        self.color = color
        self.rect = pygame.Rect(self.x, self.y, self.size[0], self.size[1])

    def update(self, stat, sc):
        stat = max(0, stat)
        pygame.draw.rect(sc, self.color, (self.rect.centerx, self.rect.centery, stat % self.size[0], self.size[1]))
        pygame.draw.rect(sc, (255, 255, 255), (self.rect.centerx, self.rect.centery, self.size[0], self.size[1]), 2)
