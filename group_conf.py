from pygame import sprite

bullet_player_group = sprite.RenderUpdates()
bullet_enemy_group = sprite.RenderUpdates()
enemy_group = sprite.RenderUpdates()

item_group = sprite.RenderUpdates()

particle_group = sprite.RenderUpdates()

notification_group = sprite.RenderUpdates()

electro_line_group = sprite.RenderUpdates()

bomb_boom_group = sprite.RenderUpdates()

fill_screen = sprite.RenderUpdates()

txt_spawn = sprite.RenderUpdates()
