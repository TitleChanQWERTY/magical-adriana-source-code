from random import randint

import pygame
from config import sc, clock, standart_font, WINDOW_SIZE
from group_conf import fill_screen
from scene import scene_config, scene_menu, scene_weapon_menu
from screen_fill import ScreenFill
from screenshoot import TakeScreenshot, draw_notification
from story import scene_story_game
from animator import Animator
import language_system

map = [".............",
       "......o......",
       "......o......",
       "......o......",
       "......o......",
       "......o......",
       "......O......",
       "............."]

SIZE_TILE = 85


class LvlOBJ(pygame.sprite.Sprite):
    def __init__(self, x, y, lvl, is_friend=False, is_boss=False):
        super().__init__()

        self.x = x
        self.y = y

        self.lvl = lvl

        if not is_friend:
            self.color = (255, 255, 255)
        else:
            self.color = (0, 255, 0)
        if is_boss:
            self.color = (255, 0, 0)

        self.friend = is_friend

        if scene_config.difficulty == 0:
            self.kill_count = randint(3, 10)
        elif scene_config.difficulty == 1:
            self.kill_count = randint(11, 17)
        elif scene_config.difficulty == 2:
            self.kill_count = randint(25, 35)

    def check_lvl(self):
        scene_story_game.scene(self.kill_count, self.friend, self.lvl)

    def update(self):
        pygame.draw.circle(sc, self.color, (self.x, self.y), 30, 2)
        pygame.draw.circle(sc, (0, 0, 0), (self.x, self.y), 28)
        txt_draw_lvl = pygame.font.Font(standart_font, 25).render(str(self.lvl), scene_config.AA_TEXT, (255, 255, 255))
        sc.blit(txt_draw_lvl, txt_draw_lvl.get_rect(center=(self.x-1, self.y+2)))


def get_closest_object(object_positions):
    connections = {}

    for i, start_pos in enumerate(object_positions):
        for j, end_pos in enumerate(object_positions):
            if i != j:
                closest_object = j
                connections[i] = closest_object

    return connections


def get_all_object_coordinates(map_data):
    object_positions = []

    for y, row in enumerate(map_data):
        for x, tile in enumerate(row):
            if tile != ".":
                object_positions.append((x * SIZE_TILE + SIZE_TILE + 15, y * SIZE_TILE + SIZE_TILE + 10))

    return object_positions


def draw_lines_between_objects(map_data):
    object_positions = get_all_object_coordinates(map_data)
    connections = get_closest_object(object_positions)

    for start_object, end_object in connections.items():
        start_pos = object_positions[start_object]
        end_pos = object_positions[end_object]
        pygame.draw.line(sc, (255, 0, 0), start_pos, end_pos, 2)


def scene():
    running = True
    FPS: int = 60

    world_map_txt = pygame.font.Font(standart_font, 29).render(language_system.txt_lang["STORY_TREE"],
                                                               scene_config.AA_TEXT, (255, 255, 255))

    select = 0

    select_max = 0

    pos_selector = [0, 0]

    lvl = 0

    lvl_obj_count = []

    for y, row in enumerate(map):
        for x, tile in enumerate(row):
            rect = pygame.Rect(x * SIZE_TILE + SIZE_TILE, y * SIZE_TILE + SIZE_TILE, 30, 30)
            if tile == "o":
                select_max += 1
                lvl_obj_count.append(LvlOBJ(rect.centerx, rect.centery, lvl))
                lvl += 1
            if tile == "O":
                select_max += 1
                lvl_obj_count.append(LvlOBJ(rect.centerx, rect.centery, lvl, False, True))
                lvl += 1
            elif tile == "f":
                select_max += 1
                lvl_obj_count.append(LvlOBJ(rect.centerx, rect.centery, lvl, True))
                lvl += 1

    sprite_selector = (pygame.image.load("Assets/sprite/ui/selector_rect_anim/1.png").convert_alpha(),
                       pygame.image.load("Assets/sprite/ui/selector_rect_anim/2.png").convert_alpha(),
                       pygame.image.load("Assets/sprite/ui/selector_rect_anim/3.png").convert_alpha())

    anim_select = Animator(11, sprite_selector)

    isStart = False

    time_start = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if not isStart:
                    if e.key == pygame.K_RETURN:
                        isStart = True
                        ScreenFill(1, 5, (0, 0, 0), 55)
                    if e.key == pygame.K_ESCAPE:
                        running = False
                        scene_confirm_exit()
                        return 0
                    if e.key == pygame.K_k:
                        running = False
                        scene_weapon_menu.scene(scene)
                        return 0
                    if e.key == pygame.K_DOWN:
                        select += 1
                    if e.key == pygame.K_UP:
                        select -= 1
                    select %= select_max

        sc.fill((0, 0, 0))
        selector = anim_select.update()
        selector = pygame.transform.scale(selector, (17 * 5, 17 * 5))
        draw_lines_between_objects(map)
        for i in range(len(lvl_obj_count)):
            lvl_obj_count[i].update()
        pos_selector[0] = lvl_obj_count[select].x
        pos_selector[1] = lvl_obj_count[select].y

        sc.blit(world_map_txt, world_map_txt.get_rect(center=(WINDOW_SIZE[0] / 2, 45)))
        sc.blit(selector, selector.get_rect(center=(pos_selector[0], pos_selector[1])))
        if isStart:
            time_start += 1
            if time_start >= 56:
                if select != select_max:
                    running = False
                    lvl_obj_count[select].check_lvl()
                    return 0

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)


def scene_confirm_exit():
    running: bool = True
    FPS: int = 60

    size_txt_confirm = 1

    size_txt_yes_no = 1

    select = 0

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (55 * 5, 15 * 5))
    xPosSelector = 200

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                if e.key == pygame.K_LEFT:
                    select -= 1
                if e.key == pygame.K_RIGHT:
                    select += 1
                if e.key == pygame.K_RETURN:
                    if select == 1:
                        running = False
                        scene()
                        return 0
                    else:
                        running = False
                        scene_menu.scene()
                        return 0
                select %= 2

        sc.fill((0, 0, 0))

        size_txt_confirm += 3
        size_txt_confirm = max(1, min(38, size_txt_confirm))

        size_txt_yes_no += 3
        size_txt_yes_no = max(1, min(35, size_txt_yes_no))

        if select == 0:
            xPosSelector = WINDOW_SIZE[0] / 2 - 200
        else:
            xPosSelector = WINDOW_SIZE[0] / 2 + 200

        txt_confirm_exit = pygame.font.Font(standart_font, size_txt_confirm).render(
            language_system.txt_lang["CONFIRM_EXIT"],
            scene_config.AA_TEXT, (255, 255, 255))

        txt_yes = pygame.font.Font(standart_font, size_txt_yes_no).render(language_system.txt_lang["TXT_YES"],
                                                                          scene_config.AA_TEXT,
                                                                          (255, 0, 0))
        txt_no = pygame.font.Font(standart_font, size_txt_yes_no).render(language_system.txt_lang["TXT_NO"],
                                                                         scene_config.AA_TEXT,
                                                                         (0, 255, 0))
        sc.blit(txt_confirm_exit, txt_confirm_exit.get_rect(center=(WINDOW_SIZE[0] / 2, 135)))
        sc.blit(txt_yes, txt_yes.get_rect(center=(WINDOW_SIZE[0] / 2 - 200, WINDOW_SIZE[1] / 2)))
        sc.blit(txt_no, txt_no.get_rect(center=(WINDOW_SIZE[0] / 2 + 200, WINDOW_SIZE[1] / 2 - 1)))

        sc.blit(selector_img, selector_img.get_rect(center=(xPosSelector + 1, WINDOW_SIZE[1] / 2 - 5)))

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
