from random import randint

import pygame
import language_system
from config import sc, clock, WINDOW_SIZE, standart_font, key_image, rank_max_exp, joystick_m, gamepad
import group_conf
from notfication import Notification
from particle import BackParticle, AreaParticle
from scene import scene_config, scene_menu
from story import scene_story_map
from collider import bullet_collider
from screen_fill import ScreenFill
from screenshoot import TakeScreenshot, draw_notification
from reset_data import reset
from sfx_colection import switch_select_menu, stop_sound

zone_play_rect = pygame.Rect(355, 100, 495, 495)

group_start_object = pygame.sprite.Group()

isStartSpawn = False

select_music = 0

music_pack = ("Assets/ost/You cant die now. But you die where take my hand.mp3",
              "Assets/ost/And your sword, take me, new life.mp3",
              "Assets/ost/Why You So Magical Perfect?.mp3")


class StartObject(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("Assets/sprite/item/ActivateRecordMode.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (17 * 3, 22 * 3))
        self.rect = self.image.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 85))

        self.add(group_start_object)

        self.back_particle = [pygame.image.load("Assets/sprite/particle/particle/5.png").convert_alpha(),
                              pygame.image.load("Assets/sprite/particle/particle/6.png").convert_alpha()]

        self.frame = 0

    def update(self):
        global isStartSpawn
        key = pygame.key.get_pressed()
        if scene_config.p.rect.colliderect(self.rect):
            if pygame.joystick.get_count() > 0:
                    sc.blit(gamepad["A"], gamepad["A"].get_rect(center=(self.rect.centerx+1, self.rect.centery - 45)))
            else:
                sc.blit(key_image["e"], key_image["e"].get_rect(center=(self.rect.centerx, self.rect.centery - 65)))
            if key[pygame.K_e] or pygame.joystick.get_count() > 0 and joystick_m.get_button(scene_config.control_gamepad["A"]):
                ScreenFill(256, -5, (255, 0, 245), 75)
                for i in range(25):
                    size = randint(8, 14)
                    AreaParticle(self.rect.centerx, self.rect.centery + randint(0, 5),
                                 self.back_particle[randint(0, 1)], (size, size),
                                 300)
                isStartSpawn = True
                pygame.mixer.music.load(music_pack[select_music])
                pygame.mixer.music.play(-1)
                pygame.mixer.music.set_volume(scene_config.music_volume)
                self.kill()

        if self.frame < 21:
            self.rect.y -= 1
        if self.frame > 21:
            self.rect.y += 1
        if self.frame > 40:
            self.frame = 0
        self.frame += 1

        self.rect.y = max(WINDOW_SIZE[1] / 2 - 100, min(WINDOW_SIZE[1] / 2 - 86, self.rect.y))


def draw():
    group_conf.particle_group.draw(sc)
    group_conf.item_group.draw(sc)
    group_start_object.draw(sc)
    group_conf.bullet_player_group.draw(sc)
    group_conf.electro_line_group.draw(sc)
    if scene_config.p.HEALTH > 0:
        if scene_config.p.time_take_damage >= scene_config.p.max_time_take_damage:
            scene_config.p.draw_fire()
        sc.blit(scene_config.p.image, scene_config.p.rect)
    group_conf.bullet_enemy_group.draw(sc)
    group_conf.enemy_group.draw(sc)


def update():
    if isStartSpawn and scene_config.p.HEALTH > 0:
        scene_config.spawnEnemy()

    group_start_object.update()

    group_conf.enemy_group.update()
    group_conf.bomb_boom_group.draw(sc)
    group_conf.bullet_player_group.update()
    group_conf.bullet_enemy_group.update()
    group_conf.bomb_boom_group.update()
    group_conf.electro_line_group.update()
    group_conf.item_group.update()

    group_conf.particle_group.update()

    pygame.draw.rect(sc, (255, 255, 255), zone_play_rect, 3)

    pygame.draw.rect(sc, scene_config.p.color_back, (0, 0, 355, 750))
    pygame.draw.rect(sc, scene_config.p.color_back, (850, 0, 355, 750))
    pygame.draw.rect(sc, scene_config.p.color_back, (0, 0, 1250, 100))
    pygame.draw.rect(sc, scene_config.p.color_back, (0, 595, 1250, 110))

    pygame.draw.rect(sc, (0, 100, 0), (0, 0, WINDOW_SIZE[0], WINDOW_SIZE[1]), 1)

    scene_config.p.update(isStartSpawn)


logoPause = pygame.image.load("Assets/sprite/ui/logo/inGame/1.png").convert_alpha()
logoPause = pygame.transform.scale(logoPause, (59 * 4, 61 * 4))

lvl = 0


def reset_story():
    scene_config.p.kill_count = 0
    scene_config.timeCreateDroneContainer = 0
    scene_config.p.time_take_damage = 0
    scene_config.timeCreateEye = 0
    scene_config.timeCreateProtator = 0
    scene_config.timeCreateProtator = 0
    for enemy in group_conf.enemy_group:
        enemy.kill()
    for particle in group_conf.particle_group:
        particle.kill()


def scene(kill_count, is_friend=False, lvl_set=0):
    global isStartSpawn, lvl
    for particle in group_conf.particle_group:
        particle.kill()
    for i in range(33):
        size = randint(16, 22)
        BackParticle(randint(360, 825), randint(65, 530),
                     scene_config.menu_particle_image[randint(0, len(scene_config.menu_particle_image) - 1)],
                     (size, size), 0, -randint(1, 2), True, randint(35, 56))

    running: bool = True
    FPS: int = 60

    isPause: bool = False

    fill_pause_surf = pygame.Surface((WINDOW_SIZE[0], WINDOW_SIZE[1]))
    alpha_pause: int = 10
    frame_fill_pause: int = 0

    BackTXT = pygame.font.Font(standart_font, 45).render(language_system.txt_lang["RESUME"],
                                                         scene_config.AA_TEXT,
                                                         (255, 255, 255))
    ExitTXT = pygame.font.Font(standart_font, 40).render(language_system.txt_lang["STORY_TREE"],
                                                         scene_config.AA_TEXT,
                                                         (255, 255, 255))

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img,
                                          (64 * 4 + len(language_system.txt_lang["RESUME"]) + 28, 16 * 4 + 1))
    yPosSelector = 250

    M_G_SLEEP = pygame.font.Font(standart_font, 25).render(language_system.txt_lang["MAGICAL_GIRL_SLEEP"],
                                                           scene_config.AA_TEXT,
                                                           (255, 155, 255))

    select: int = 0

    back_logo_game = pygame.image.load("Assets/sprite/ui/inGameLogo.png").convert_alpha()
    back_logo_game = pygame.transform.scale(back_logo_game, (53 * 4, 59 * 4))

    back_logo_game2 = pygame.image.load("Assets/sprite/ui/inGameLogo.png").convert_alpha()
    back_logo_game2 = pygame.transform.scale(back_logo_game2, (53 * 4, 59 * 4))
    back_logo_game2 = pygame.transform.flip(back_logo_game2, True, False)

    scene_config.p.set_weapon()

    StartObject()

    time_show_record_frame = 0

    size_frame = [0, 0]

    volume_music = scene_config.music_volume

    index_txt_score = 0
    index_txt_hiscore = 0
    index_txt_kill = 0
    index_txt_exp = 0

    press_any_key = pygame.font.Font(standart_font, 19).render(language_system.txt_lang["PRESS_ANY_KEY"],
                                                               scene_config.AA_TEXT, (255, 255, 255))

    gray_surf = pygame.Surface(WINDOW_SIZE)
    gray_surf.set_alpha(115)
    gray_surf.fill((40, 40, 40))

    green_surf = pygame.Surface(WINDOW_SIZE)
    green_surf.set_alpha(115)
    green_surf.fill((0, 50, 0))

    lvl = lvl_set

    while running:
        pygame.event.pump()
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.USEREVENT:
                scene_config.time_played += 1
            elif e.type == pygame.JOYHATMOTION and isPause:
                if e.value == (0, -1):
                    switch_select_menu.play()
                    select += 1
                elif e.value == (0, 1):
                    switch_select_menu.play()
                    select -= 1
                select %= 3
            elif e.type == pygame.JOYBUTTONDOWN:
                if scene_config.p.HEALTH <= 0 and time_show_record_frame >= 270:
                    pygame.mixer.music.stop()
                    for start in group_start_object:
                        start.kill()
                    isStartSpawn = False
                    reset()
                    running = False
                    scene_menu.scene()
                    return 0
                if e.button == scene_config.control_gamepad["START"]:
                    if not isPause:
                        stop_sound()
                        isPause = True
                    else:
                        alpha_pause = 10
                        frame_fill_pause = 0
                        isPause = False
                elif e.button == scene_config.control_gamepad["A"]:
                    match select:
                            case 0:
                                alpha_pause = 10
                                frame_fill_pause = 0
                                isPause = False
                            case 1:
                                for start in group_start_object:
                                    start.kill()
                                isStartSpawn = False
                                reset()
                                running = False
                                pygame.mixer.music.stop()
                                scene()
                                return 0
                            case 2:
                                for start in group_start_object:
                                    start.kill()
                                isStartSpawn = False
                                reset()
                                running = False
                                pygame.mixer.music.stop()
                                scene_menu.scene()
                                return 0
            elif e.type == pygame.KEYDOWN:
                if scene_config.p.HEALTH <= 0 and time_show_record_frame >= 300 or scene_config.p.kill_count >= kill_count and time_show_record_frame >= 300:
                    pygame.mixer.music.stop()
                    for start in group_start_object:
                        start.kill()
                    isStartSpawn = False
                    running = False
                    if scene_config.p.HEALTH <= 0:
                        reset()
                        scene_menu.scene()
                    else:
                        reset_story()
                        scene_story_map.scene()
                    return 0
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                elif e.key == pygame.K_F10:
                    running = False
                    pygame.quit()
                elif e.key == pygame.K_ESCAPE:
                    if not isPause:
                        stop_sound()
                        isPause = True
                    else:
                        alpha_pause = 10
                        frame_fill_pause = 0
                        isPause = False
                if isPause:
                    if e.key == pygame.K_e or e.key == pygame.K_RETURN:
                        match select:
                            case 0:
                                alpha_pause = 10
                                frame_fill_pause = 0
                                isPause = False
                            case 1:
                                reset_story()
                                for start in group_start_object:
                                    start.kill()
                                isStartSpawn = False
                                reset()
                                running = False
                                pygame.mixer.music.stop()
                                scene_story_map.scene()
                                return 0

                    if e.key == pygame.K_DOWN or e.key == pygame.K_s:
                        switch_select_menu.play()
                        select += 1
                    elif e.key == pygame.K_UP or e.key == pygame.K_w:
                        switch_select_menu.play()
                        select -= 1
                    select %= 2
            scene_config.p.switch_gun(e)

        sc.fill((0, 0, 0))
        draw()
        txt_kill_count = pygame.font.Font(standart_font, 27).render(str(scene_config.p.kill_count)+"/"+str(kill_count), scene_config.AA_TEXT, (255, 0, 0))
        sc.blit(txt_kill_count, txt_kill_count.get_rect(center=(WINDOW_SIZE[0]/2, 565)))
        if not isPause:
            update()
            if scene_config.p.HEALTH <= 0 or scene_config.p.kill_count >= kill_count:
                if scene_config.p.HEALTH <= 0:
                    sc.blit(gray_surf, (0, 0))
                else:
                    sc.blit(green_surf, (0, 0))
                if time_show_record_frame < 1:
                    current_rank = 0
                    scene_config.p.EXP += scene_config.p.kill_count
                    for i in range(len(rank_max_exp)):
                        if scene_config.p.EXP > rank_max_exp[i]:
                            current_rank = i
                    if current_rank > scene_config.p.RANK:
                        scene_config.p.RANK = current_rank
                        print(scene_config.p.RANK)
                        Notification(language_system.txt_lang["NEW_RANK"], 2)
                        scene_config.p.EXP = 0
                if time_show_record_frame >= 270:
                    sc.blit(press_any_key,
                            press_any_key.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 120)))
                if time_show_record_frame > 50:
                    pygame.mixer.music.set_volume(volume_music)
                    volume_music -= 0.01
                    volume_music = max(volume_music, 0.0)
                if time_show_record_frame > 220:
                    score_txt = language_system.txt_lang["SCORE"] + ": " + str(scene_config.p.SCORE)
                    hiscore_txt = language_system.txt_lang["HISCORE"] + ": " + str(scene_config.p.HISCORE)
                    kill_txt = language_system.txt_lang["KILL"] + ": " + str(scene_config.p.kill_count)
                    exp_txt = language_system.txt_lang["EXP"] + ": " + str(scene_config.p.EXP)
                    if index_txt_score < len(score_txt):
                        index_txt_score += 1
                    if index_txt_hiscore < len(hiscore_txt):
                        index_txt_hiscore += 1
                    if index_txt_kill < len(kill_txt):
                        index_txt_kill += 1
                    if index_txt_exp < len(exp_txt):
                        index_txt_exp += 1
                    txt_score = pygame.font.Font(standart_font, 27).render(score_txt[:index_txt_score],
                                                                           scene_config.AA_TEXT, (255, 255, 0))
                    txt_hiscore = pygame.font.Font(standart_font, 27).render(hiscore_txt[:index_txt_hiscore],
                                                                             scene_config.AA_TEXT, (255, 100, 0))
                    txt_kill = pygame.font.Font(standart_font, 27).render(kill_txt[:index_txt_kill],
                                                                          scene_config.AA_TEXT, (255, 255, 255))
                    txt_exp = pygame.font.Font(standart_font, 27).render(exp_txt[:index_txt_exp],
                                                                         scene_config.AA_TEXT, (255, 255, 255))
                    sc.blit(txt_score, txt_score.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 116)))
                    sc.blit(txt_hiscore, txt_hiscore.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 - 56)))
                    sc.blit(txt_kill, txt_kill.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 10)))
                    sc.blit(txt_exp, txt_exp.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + 65)))
                if time_show_record_frame > 150:
                    if size_frame[0] < 40 * 8:
                        size_frame[0] += 11
                    if size_frame[1] < 51 * 6:
                        size_frame[1] += 11
                    frame_record_image = pygame.image.load("Assets/sprite/ui/final_record_frame.png").convert_alpha()
                    frame_record_image = pygame.transform.scale(frame_record_image, size_frame)
                    sc.blit(frame_record_image,
                            frame_record_image.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2)))
                time_show_record_frame += 1
                for enemy in group_conf.enemy_group:
                    enemy.kill()
            else:
                bullet_collider()
            sc.blit(back_logo_game, back_logo_game.get_rect(center=(150, 580)))
            sc.blit(back_logo_game2, back_logo_game2.get_rect(center=(150 * 7, 580)))

            render_offset = [0, 0]
            if scene_config.screen_shake > 0:
                scene_config.screen_shake -= 1
                render_offset[0] = randint(-scene_config.power_shake, scene_config.power_shake)
                render_offset[1] = randint(-scene_config.power_shake, scene_config.power_shake)
                sc.blit(pygame.transform.scale(sc, WINDOW_SIZE), render_offset)
        else:
            if frame_fill_pause < 14:
                alpha_pause += 16
                fill_pause_surf.set_alpha(alpha_pause)
                frame_fill_pause += 1

            match select:
                case 0:
                    yPosSelector = 244
                case 1:
                    yPosSelector = 445

            sc.blit(fill_pause_surf, (0, 0))
            sc.blit(M_G_SLEEP, M_G_SLEEP.get_rect(center=(WINDOW_SIZE[0] // 2, 95)))
            sc.blit(BackTXT, BackTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 250)))
            sc.blit(ExitTXT, ExitTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 450)))
            sc.blit(selector_img, selector_img.get_rect(center=(WINDOW_SIZE[0] // 2, yPosSelector)))

            sc.blit(logoPause, (95, WINDOW_SIZE[1] // 2 - 145))

        group_conf.fill_screen.draw(sc)
        group_conf.fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
