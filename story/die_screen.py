from random import randint

import pygame

import language_system
from config import standart_font, sc, WINDOW_SIZE, clock
from group_conf import fill_screen
from scene import scene_config, scene_menu
from screen_fill import ScreenFill
from screenshoot import draw_notification
from reset_data import reset
from sfx_colection import player_take_damage


def scene(index):
    running: bool = True
    FPS: int = 60

    ScreenFill(205, -5, (255, 5, 5), 50)

    txt_kill_index = (language_system.txt_lang["KILL_MESSAGE_AYA"],
                      language_system.txt_lang["KILL_MESSAGE_SATORA"])

    txt_kill = pygame.font.Font(standart_font, 45 - len(str(scene_config.p.NAME))).render(
        "\"" + str(scene_config.p.NAME) + "\"" + " " +
        txt_kill_index[index],
        scene_config.AA_TEXT, (255, 0, 0))

    time_next = 0

    player_take_damage[1].play()

    reset()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
        sc.fill((20, 0, 0))

        sc.blit(txt_kill, txt_kill.get_rect(
            center=(WINDOW_SIZE[0] / 2 + randint(-10, 10), WINDOW_SIZE[1] / 2 + randint(-10, 10))))

        if time_next == 250:
            ScreenFill(1, 5, (0, 0, 0), 100)
        if time_next >= 350:
            running = False
            scene_menu.scene_press()
            return 0
        time_next += 1

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()
        pygame.display.update()
        clock.tick(FPS)
