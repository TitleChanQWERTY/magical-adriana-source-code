import pygame
from config import sc, clock, WINDOW_SIZE, standart_font
import language_system
from random import randint
from story import scene_story_game
from group_conf import fill_screen
from scene import scene_config
from screen_fill import ScreenFill
from screenshoot import TakeScreenshot, draw_notification
from sfx_colection import game_type_txt


def scene():
    running: bool = True
    FPS: int = 60

    index_txt = 0

    txt_draw = [language_system.txt_lang["START_SCENE_1"],
                language_system.txt_lang["START_SCENE_2"],
                language_system.txt_lang["START_SCENE_2_2"],
                language_system.txt_lang["START_SCENE_3"],
                language_system.txt_lang["START_SCENE_4"],
                language_system.txt_lang["START_SCENE_5"],
                language_system.txt_lang["START_SCENE_6"]]

    font = pygame.font.Font(standart_font, 20)

    select = 0

    is_select = False

    player_true = (scene_config.p.NAME, scene_config.p.AGE, language_system.txt_lang["START_SCENE_PLAYER_WHO_YOU"])

    select_true = 0

    selector_img = pygame.image.load("Assets/sprite/ui/selector.png").convert_alpha()
    selector_img = pygame.transform.scale(selector_img, (64 * 5, 16 * 5))

    is_key = False

    isNext = False

    time_next = 0

    next_image = pygame.image.load("Assets/sprite/ui/txt_dialog/next.png").convert_alpha()
    next_image = pygame.transform.scale(next_image, (7 * 4, 5 * 4))

    frame_anim_next = 0
    yPosNext = 660

    stick = pygame.image.load("Assets/sprite/scene_moment/start/1.png").convert_alpha()
    stick = pygame.transform.scale(stick, (9 * 7, 9 * 7))

    heart = pygame.image.load("Assets/sprite/scene_moment/start/2.png").convert_alpha()
    heart = pygame.transform.scale(heart, (5 * 7, 6 * 7))

    hat = pygame.image.load("Assets/sprite/scene_moment/start/3.png").convert_alpha()
    hat = pygame.transform.scale(hat, (7 * 9, 8 * 9))

    alpha_per = 0

    xPosStick = WINDOW_SIZE[0]/2-50
    xPosHeart = WINDOW_SIZE[0]/2+50
    xPosHat = WINDOW_SIZE[0]/2

    yPosStick = WINDOW_SIZE[1]/2-105
    yPosHeart = WINDOW_SIZE[1]/2-104
    yPosHat = WINDOW_SIZE[1]/2-215

    time_change_stick = 0
    time_change_heart = 0
    time_change_hat = 0

    speed_move_x = [-1, 1]
    speed_move_y = [0, 0, 0]

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            if e.type == pygame.USEREVENT:
                scene_config.time_played += 1
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    TakeScreenshot()
                else:
                    if is_key and not isNext:
                        if not is_select:
                            if select > 5:
                                isNext = True
                                ScreenFill(1, 5, (0, 0, 0), 100)
                            else:
                                index_txt = 0
                                if select == 0 or select == 1 or select == 2:
                                    is_select = True
                                select += 1
                                if select == 2:
                                    if scene_config.p.AGE <= 14:
                                        txt_draw[2] = language_system.txt_lang["START_SCENE_2_2"]
                                    if scene_config.p.AGE >= 16:
                                        txt_draw[2] = language_system.txt_lang["START_SCENE_2_3"]
                                    if scene_config.p.AGE >= 25:
                                        txt_draw[2] = language_system.txt_lang["START_SCENE_2_4"]
                        else:
                            index_txt = 0
                            if e.key == pygame.K_RETURN:
                                is_select = False
                                select_true += 1

        sc.fill((0, 0, 0))

        sc.blit(stick, stick.get_rect(center=(xPosStick, yPosStick)))
        sc.blit(heart, heart.get_rect(center=(xPosHeart, yPosHeart)))
        sc.blit(hat, hat.get_rect(center=(xPosHat, yPosHat)))

        stick.set_alpha(alpha_per)
        heart.set_alpha(alpha_per)
        hat.set_alpha(alpha_per)

        alpha_per += 1
        alpha_per = max(0, min(195, alpha_per))

        if time_change_stick >= 22:
            if speed_move_x[0] >= 1:
                speed_move_x[0] = -1
            else:
                speed_move_x[0] = 1
            if speed_move_y[0] >= 1:
                speed_move_y[0] = -1
            else:
                speed_move_y[0] = 1
            time_change_stick = 0
        if time_change_heart >= 22:
            if speed_move_x[1] >= 1:
                speed_move_x[1] = -1
            else:
                speed_move_x[1] = 1
            if speed_move_y[1] >= 1:
                speed_move_y[1] = -1
            else:
                speed_move_y[1] = 1 
            time_change_heart = 0
        if time_change_hat >= 26:
            if speed_move_y[2] >= 1:
                speed_move_y[2] = -1
            else:
                speed_move_y[2] = 1  
            time_change_hat = 0
        
        time_change_stick += 1
        time_change_heart += 1
        time_change_hat += 1

        xPosStick += speed_move_x[0]
        xPosHeart += speed_move_x[1]

        yPosStick += speed_move_y[0]
        yPosHeart += speed_move_y[1]
        yPosHat += speed_move_y[2]          

        if isNext:
            if time_next >= 100:
                running = False
                scene_story_game.scene(10, lvl_set=0)
                return 0
            time_next += 1

        if not is_select:
            text_lines = []

            if not isNext:
                words = txt_draw[select].split(" ")
            current_line = words[0]

            for word in words[1:]:
                rendered_word = font.render(current_line + " " + word, scene_config.AA_TEXT, (255, 255, 255))
                if rendered_word.get_width() <= sc.get_width():
                    current_line += " " + word
                else:
                    text_lines.append(current_line)
                    current_line = word
            text_lines.append(current_line)

            for i, line in enumerate(text_lines):
                if not isNext:
                    if index_txt < len(txt_draw[select]):
                        index_txt += 1
                        game_type_txt.play()
                        is_key = False
                    else:
                        if frame_anim_next == 0:
                            yPosNext = 685
                        if frame_anim_next < 25:
                            yPosNext -= 1
                            frame_anim_next += 1
                        else:
                            frame_anim_next = 0
                        sc.blit(next_image, next_image.get_rect(center=(WINDOW_SIZE[0] / 2, yPosNext)))
                        is_key = True
                rendered_line = font.render(line[:index_txt], scene_config.AA_TEXT, (255, 255, 255))
                sc.blit(rendered_line, rendered_line.get_rect(
                    center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2 + i * rendered_line.get_height() + 35)))

        else:
            txt_true_draw = pygame.font.Font(standart_font, 20).render(str(player_true[select_true]), scene_config.AA_TEXT,
                                                                       (255, 255, 255))
            sc.blit(txt_true_draw, txt_true_draw.get_rect(center=(WINDOW_SIZE[0] / 2, 550)))
            sc.blit(selector_img, selector_img.get_rect(center=(WINDOW_SIZE[0] / 2, 550)))

        fill_screen.draw(sc)
        fill_screen.update()

        draw_notification()

        pygame.display.update()
        clock.tick(FPS)
