import math
from random import randint

import pygame

from config import WINDOW_SIZE, sc, standart_font
from group_conf import enemy_group
from animator import Animator
from bullet import bullet
from scene import scene_config, scene_tutorial
from item import ammo, paper
from sfx_colection import enemy_first_shoot, chanel_enemy_shoot, boss_die_mecha, boss_down_fall, boss_mecha_walk
from anim_effect import Effect
from txt_spawn_collect import TXT
from cartoon_splash import Splash
from screen_fill import ScreenFill

walk_anim = [pygame.image.load("Assets/sprite/enemy/lancetus/run/1.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/run/2.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/run/3.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/run/4.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/run/5.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/run/6.png").convert_alpha()]

idle_anim = [pygame.image.load("Assets/sprite/enemy/lancetus/idle/1.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/idle/2.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/idle/3.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/idle/3.png").convert_alpha(),
            pygame.image.load("Assets/sprite/enemy/lancetus/idle/2.png").convert_alpha()]

die_anim = [pygame.image.load("Assets/sprite/enemy/lancetus/die/1.png").convert_alpha(),
pygame.image.load("Assets/sprite/enemy/lancetus/die/2.png").convert_alpha()]

for w in range(6):
    walk_anim[w] = pygame.transform.scale(walk_anim[w], (29 * 4, 30 * 4))

for i in range(5):
    idle_anim[i] = pygame.transform.scale(idle_anim[i], (29 * 4, 30 * 4)) 

for d in range(2):
    die_anim[d] = pygame.transform.scale(die_anim[d], (29 * 4, 30 * 4)) 

bullet_img = (pygame.image.load("Assets/sprite/bullet/enemy/7.png").convert_alpha(), pygame.image.load("Assets/sprite/bullet/enemy/2.png").convert_alpha(),
                pygame.image.load("Assets/sprite/bullet/enemy/5.png").convert_alpha(), pygame.image.load("Assets/sprite/bullet/enemy/4.png").convert_alpha(),
                pygame.image.load("Assets/sprite/bullet/enemy/1.png").convert_alpha(), pygame.image.load("Assets/sprite/bullet/enemy/8.png").convert_alpha(),
                pygame.image.load("Assets/sprite/bullet/enemy/9.png").convert_alpha())
 
class Lancetus(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = walk_anim[0]
        self.rect = self.image.get_rect(center=(WINDOW_SIZE[0]/2, 0))

        self.health_max = randint(2050, 2275)

        self.health = self.health_max

        self.add(enemy_group)

        self.anim_walk = Animator(4, walk_anim)
        self.anim_idle = Animator(7, idle_anim)

        self.timeShoot = 0

        self.lucky = randint(0, 1)

        self.type_enemy = 0

        self.move_y = randint(-1, 1)

        self.isChain = False

        self.start_time = 0

        self.time_move = 0

        self.isMove = False
        self.count_move = 0
        self.count_move_max = randint(15, 35)

        self.move = [0, 0]

        self.move_now = [0, 0]

        self.select_shoot = 0

        self.time_shoot = [0, 0, 0, 0, 0, 0]

        self.count_shoot = [0, 0, 0, 0]

        self.time_next_shoot = 0

        self.time_next_shoot_max = randint(45, 65)

        self.starting_shoot = 0

        self.shoot_pattern = (self.shoot_1, self.shoot_3, self.shoot_4, self.shoot_2, self.shoot_5, self.shoot_6)

        self.anim_die = Animator(9, die_anim)
        
        self.time_die = 0

        self.isShoot4 = False

        self.isSoundFall = False

        self.time_damage_set = 0

        self.isBoss = True

        self.time_pre_shoot = 10

        self.font_draw = pygame.font.Font(standart_font, 25)

    def shoot_6(self):
        if self.count_shoot[3] <= 5:
            if self.time_shoot[5] >= 50:
                bullet.Bullet(self.rect.centerx, self.rect.bottom, 0, 5, bullet_img[6], (15 * 4, 15 * 4))
                bullet.Bullet(self.rect.centerx-3, self.rect.bottom, -5, 5, bullet_img[6], (15 * 4, 15 * 4))
                bullet.Bullet(self.rect.centerx, self.rect.centery, -5, 0, bullet_img[6], (15 * 4, 15 * 4), isStat=True)

                bullet.Bullet(self.rect.centerx-3, self.rect.top, -5, -5, bullet_img[6], (15 * 4, 15 * 4))
                bullet.Bullet(self.rect.centerx, self.rect.top, 0, -5, bullet_img[6], (15 * 4, 15 * 4))
                bullet.Bullet(self.rect.centerx+3, self.rect.top, 5, -5, bullet_img[6], (15 * 4, 15 * 4))

                bullet.Bullet(self.rect.centerx, self.rect.centery, 5, 0, bullet_img[6], (15 * 4, 15 * 4), isStat=True)
                bullet.Bullet(self.rect.centerx+3, self.rect.bottom, 5, 5, bullet_img[6], (15 * 4, 15 * 4))
                self.time_shoot[5] = 0
                self.count_shoot[3] += 1
                return
            else:
                self.time_shoot[5] += 1
                return
        else:
            self.starting_shoot = 0
            self.time_shoot[5] = 0
            self.count_shoot[3] = 0
            self.time_next_shoot = 0
            self.time_next_shoot_max = randint(45, 65)
            return

    def shoot_5(self):
        if self.time_shoot[4] <= 100:
            bullet.Bullet(self.rect.centerx, self.rect.bottom, 0, 6, bullet_img[5], (14 * 3, 16 * 3))
            bullet.Bullet(self.rect.centerx, self.rect.bottom, -2, 6, bullet_img[5], (12 * 3, 14 * 3))
            bullet.Bullet(self.rect.centerx, self.rect.bottom, 2, 6, bullet_img[5], (12 * 3, 14 * 3))
            self.time_shoot[4] += 1
        else:
            self.starting_shoot = 0
            self.time_shoot[4] = 0
            self.time_next_shoot = 0
            self.time_next_shoot_max = randint(65, 85)
            return

    def shoot_4(self):
        if self.rect.y > 100 and not self.isShoot4:
            self.image = self.anim_walk.update()
            boss_mecha_walk.play()
            self.rect.y -= 9
            return
        else:
            self.isShoot4 = True
            if self.time_shoot[3] <= 85:
                self.time_shoot[3] += 1
                if self.rect.bottom >= 580:
                    self.image = self.anim_idle.update()
                    if not self.isSoundFall:
                        bullet.Bullet(WINDOW_SIZE[0]/2-250, 100, 0, 3, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2-200, 95, 0, 3, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2-150, 95, 0, 4, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2-100, 95, 0, 4, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2-50, 95, 0, 6, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2, 95, 0, 6, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2+50, 95, 0, 6, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2+100, 95, 0, 4, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2+150, 95, 0, 4, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2+200, 95, 0, 3, bullet_img[4], (7 * 3, 10 * 3))
                        bullet.Bullet(WINDOW_SIZE[0]/2+250, 100, 0, 3, bullet_img[4], (7 * 3, 10 * 3))
                        scene_config.screen_shake += 20
                        scene_config.power_shake += 6
                        boss_down_fall.play()
                        self.isSoundFall = True
                else:
                    self.image = self.anim_walk.update()
                    self.time_pre_shoot += 1
                    if self.time_pre_shoot >= 3:
                        bullet.Bullet(self.rect.centerx, self.rect.centery, 2, 0, bullet_img[6], (7 * 3, 7 * 3))
                        bullet.Bullet(self.rect.centerx, self.rect.centery, -2, 0, bullet_img[6], (7 * 3, 7 * 3))
                        self.time_pre_shoot = 0
                    self.rect.y += 20
                return
            else:
                self.isSoundFall = False
                self.isShoot4 = False
                self.starting_shoot = 0
                self.time_shoot[3] = 0
                self.time_next_shoot = 0
                self.time_next_shoot_max = randint(35, 58)
                return

    def shoot_3(self):
        if self.count_shoot[2] < 15:
            if self.time_shoot[2] >= 17:
                chanel_enemy_shoot.play(enemy_first_shoot)
                bullet.Bullet(self.rect.centerx, self.rect.bottom, 0, 5, bullet_img[3], (10 * 3, 9 * 3))
                bullet.Bullet(self.rect.centerx-3, self.rect.bottom, -4, 5, bullet_img[3], (11 * 3, 11 * 3))
                bullet.Bullet(self.rect.centerx, self.rect.centery, -5, 0, bullet_img[3], (10 * 3, 10 * 3), isStat=True)

                bullet.Bullet(self.rect.centerx-3, self.rect.top, -4, -5, bullet_img[3], (11 * 3, 11 * 3))
                bullet.Bullet(self.rect.centerx, self.rect.top, 0, -5, bullet_img[3], (10 * 3, 10 * 3))
                bullet.Bullet(self.rect.centerx+3, self.rect.top, 4, -5, bullet_img[3], (11 * 3, 11 * 3))

                bullet.Bullet(self.rect.centerx, self.rect.centery, 5, 0, bullet_img[3], (10 * 3, 10 * 3), isStat=True)
                bullet.Bullet(self.rect.centerx+3, self.rect.bottom, 4, 5, bullet_img[3], (11 * 3, 11 * 3))

                self.time_shoot[2] = 0
                self.count_shoot[2] += 1
            else:
                self.time_shoot[2] += 1
        else:
            self.starting_shoot = 0
            self.count_shoot[2] = 0
            self.time_shoot[2] = 0
            self.time_next_shoot = 0
            self.time_next_shoot_max = randint(45, 80)
    def shoot_2(self):
        if self.count_shoot[1] < 4:
            if self.time_shoot[1] >= 10:
                chanel_enemy_shoot.play(enemy_first_shoot)
                bullet.Bullet(self.rect.centerx, self.rect.bottom, 0, 5, bullet_img[2], (9 * 3, 9 * 3))
                bullet.Bullet(self.rect.centerx-3, self.rect.bottom, -4, 5, bullet_img[1], (11 * 3, 11 * 3))
                bullet.Bullet(self.rect.centerx, self.rect.centery, -5, 0, bullet_img[2], (9 * 3, 9 * 3), isStat=True)

                bullet.Bullet(self.rect.centerx-3, self.rect.top, -4, -5, bullet_img[2], (11 * 3, 11 * 3))
                bullet.Bullet(self.rect.centerx, self.rect.top, 0, -5, bullet_img[1], (9 * 3, 9 * 3))
                bullet.Bullet(self.rect.centerx+3, self.rect.top, 4, -5, bullet_img[2], (11 * 3, 11 * 3))

                bullet.Bullet(self.rect.centerx, self.rect.centery, 5, 0, bullet_img[2], (9 * 3, 9 * 3), isStat=True)
                bullet.Bullet(self.rect.centerx+3, self.rect.bottom, 4, 5, bullet_img[1], (11 * 3, 11 * 3))

                self.time_shoot[1] = 0
                self.count_shoot[1] += 1
            else:
                self.time_shoot[1] += 1
        else:
            self.starting_shoot = 0
            self.count_shoot[1] = 0
            self.time_shoot[1] = 0
            self.time_next_shoot = 0
            self.time_next_shoot_max = randint(65, 85)

    def shoot_1(self):
        if self.starting_shoot >= 26:
            if self.count_shoot[0] < 5:
                if self.time_shoot[0] >= 4:
                    chanel_enemy_shoot.play(enemy_first_shoot)
                    self.count_shoot[0] += 1
                    bullet.Bullet(self.rect.centerx-8, self.rect.bottom-17, -2, 5, bullet_img[0], (12 * 3, 12 * 3))
                    bullet.Bullet(self.rect.centerx, self.rect.bottom-10, 0, 5, bullet_img[0], (12 * 3, 12 * 3))
                    bullet.Bullet(self.rect.centerx+8, self.rect.bottom-17, 2, 5, bullet_img[0], (12 * 3, 12 * 3))
                    self.time_shoot[0] = 0
                else:
                    self.time_shoot[0] += 1
            else:
                self.starting_shoot = 0
                self.count_shoot[0] = 0
                self.time_shoot[0] = 0
                self.time_next_shoot = 0
                self.time_next_shoot_max = randint(40, 60)
        else:
            self.starting_shoot += 1
        
    def update(self):
        self.time_damage_set += 1
        self.time_damage_set = max(0, min(10, self.time_damage_set))
        hp_txt = self.font_draw.render(str(self.health)+"/"+str(self.health_max), scene_config.AA_TEXT, (255, 230, 255))
        sc.blit(hp_txt, hp_txt.get_rect(center=(self.rect.centerx, self.rect.bottom+12)))
        if self.health <= 0:
            self.health = max(0, self.health)
            if self.time_die < 65:
                self.image = self.anim_die.update()
                self.time_die += 1
            else:
                Effect(self.rect.centerx, self.rect.centery, scene_config.boss_die_effect, (25*8, 25*8), 8)
                scene_config.time_spawn_bos = 0
                scene_config.isBoss = False
                scene_config.isBoss2 = False
                scene_config.isBoss3 = False
                ScreenFill(265, -5, (255, 255, 255), 99)
                scene_config.screen_shake += 30
                scene_config.power_shake += 40
                boss_die_mecha.play()
                scene_config.p.BOSS_KILL_COUNT += 1
                scene_config.p.EXP += randint(2, 6)
                scene_config.p.kill_count += 1
                scene_tutorial.select_tutorial = 4
                scene_tutorial.index_txt = 0
                scene_config.p.SCORE += 1575
                if scene_config.difficulty == 0:
                    scene_config.maxTimeCreateProtator = randint(270, 280)
                    scene_config.maxTimeCreateSpider = randint(430, 449)
                    scene_config.maxTimeCreateEye = randint(760, 780)
                    scene_config.maxTimeCreateDroneContainer = randint(850, 860)
                    scene_config.maxTimeCreateDroneChain = randint(295, 310)
                elif scene_config.difficulty == 1:
                    scene_config.maxTimeCreateProtator = randint(195, 200)
                    scene_config.maxTimeCreateSpider = randint(220, 245)
                    scene_config.maxTimeCreateEye = randint(550, 600)
                    scene_config.maxTimeCreateDroneContainer = randint(630, 650)
                    scene_config.maxTimeCreateDroneChain = randint(265, 285)
                elif scene_config.difficulty == 2:
                    scene_config.maxTimeCreateProtator = randint(130, 140)
                    scene_config.maxTimeCreateSpider = randint(195, 213)
                    scene_config.maxTimeCreateEye = randint(315, 320)
                    scene_config.maxTimeCreateDroneContainer = randint(470, 480)
                    scene_config.maxTimeCreateDroneChain = randint(235, 240)
                ammo.Ammo(self.rect.centerx, self.rect.centery)
                ammo.Ammo(self.rect.centerx-9, self.rect.centery)
                ammo.Ammo(self.rect.centerx+9, self.rect.centery-15)
                if len(scene_config.p.paper_story) < 7:
                    paper.Paper(self.rect.centerx, self.rect.centery-95)
                Splash("KILL", self.rect.center)
                self.kill()
            self.rect.x = max(360, min(725, self.rect.x))
            self.rect.y = max(105, min(400, self.rect.y))
        else:
            if self.start_time <= 39:
                boss_mecha_walk.play()
                self.rect.y += 6
                self.start_time += 1
                self.image = self.anim_walk.update()
            else:
                if self.time_next_shoot == self.time_next_shoot_max:
                    self.select_shoot = randint(0, 5)
                if self.time_next_shoot > self.time_next_shoot_max:
                    self.shoot_pattern[self.select_shoot]()
                else:
                    self.time_next_shoot += 1
                    
                if self.isMove and not self.isShoot4:
                    if self.count_move <= self.count_move_max:
                        self.image = self.anim_walk.update()
                        self.rect.x += self.move_now[0]
                        self.rect.y += self.move_now[1]
                        boss_mecha_walk.play()
                        self.count_move += 1
                    else:
                        self.isMove = False
                else:
                    if not self.isShoot4:
                        self.image = self.anim_idle.update()

                        if self.time_move >= 37:
                            self.count_move_max = randint(20, 65)
                            self.count_move = 0
                            if self.rect.x < 720 or self.rect.x > 365:
                                self.move[0] = randint(-4, 3)
                            else:
                                if self.rect.x >= 720:
                                    self.move[0] = randint(-5, -2)
                                elif self.rect.x <= 365:
                                    self.move[0] = randint(2, 5)

                            if self.rect.centery < 365:
                                self.move[1] = randint(-5, 5)
                            else:
                                self.move[1] = randint(-5, -2)
                            if self.move[0] != self.move_now[0]:
                                self.move_now[0] = self.move[0]
                            else:
                                self.move_now[0] = -self.move[0]
                            if self.move[1] != self.move_now[1]:
                                self.move_now[1] = self.move[1]
                            else:
                                self.move_now[1] = -self.move[1]
                            self.isMove = True
                            self.time_move = 0
                        else:
                            self.time_move += 1
        
                self.rect.x = max(360, min(725, self.rect.x))
                self.rect.y = max(95, min(465, self.rect.y))
