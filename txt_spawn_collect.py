import pygame
from group_conf import txt_spawn
from scene import scene_config
from config import standart_font


class TXT(pygame.sprite.Sprite):
    def __init__(self, x, y, timeLive, txt, size=23, color=(255, 0, 255)):
        super().__init__()
        self.image = pygame.font.Font(standart_font, size).render(str(txt), scene_config.AA_TEXT, color)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(txt_spawn)

        self.Alpha = 335

        self.timeLive = 0
        self.maxTimeLive = timeLive+1

    def update(self):
        if self.maxTimeLive <= 5 or self.rect.y <= 105:
            self.kill()
        elif self.timeLive <= 20:
            self.Alpha -= 5
        self.timeLive -= 1
        self.image.set_alpha(self.Alpha)
        self.rect.y -= 1
