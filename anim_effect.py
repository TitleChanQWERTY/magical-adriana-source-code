import pygame
from group_conf import particle_group


class Effect(pygame.sprite.Sprite):
    def __init__(self, x, y, image, size, speed_anim):
        super().__init__()
        self.image_anim = image
        self.image = self.image_anim[0]
        self.size = size
        self.image = pygame.transform.scale(self.image, self.size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.frame: int = 0

        self.time_out: int = 0

        self.speed_anim = speed_anim

    def update(self):
        self.image = self.image_anim[self.frame]
        self.image = pygame.transform.scale(self.image, self.size)

        if self.time_out > self.speed_anim:
            self.time_out = 0
            self.frame += 1
        if self.frame > len(self.image_anim) - 1:
            self.kill()
        self.time_out += 1
