import json
from pygame import font
import os
import config


def check_lang():
    if not os.path.exists("Language (Dont Remove)"):
        os.mkdir("Language (Dont Remove)")
    if not os.listdir("Language (Dont Remove)"):
        data = {
                "LANGUAGE_NAME": "English",

                "TAKE_SCREEN": "Take Screenshot!",
                "ERROR": "ERROR!",
                "NEW_RANK": "NEW RANK!",
                "NEW_FRIEND": "NEW FRIEND!",

                "SELECT_LANGUAGE": "Select Language",
                "SELECT_AA": "Turn On AA Text?",
                "EXAMPLE_TXT": "Example",
                "SELECT_FULLSCREEN": "FULLSCREEN?",
                "YOU_USE_GAMEPAD": "Set up a Gamepad? (Recommended)",
                "PRESS_TXT": "Press",
                "CONF_END": "Configuration is end",

                "GAMEPAD_UNCONECTED": "If your gamepad doesn't work, plug it in and restart the game",
                "GAMEPAD_PRESS_CALL": "PRESS",

                "CONTROL": "CONTROL",
                "MOVE": "MOVE",
                "SHOOT": "SHOOT",
                "NEXT_WEAPON": "NEXT WEAPON",
                "PREVIEW_WEAPON": "PREVIOUS WEAPON",
                "SHOOT_BOMB": "SHOOT BOMB",

                "CREATE_BY": "CREATED BY Bogdan \"TitleChanQWERTY\" Praporshikov",
                "AUTOR_NAME": "Bogdan \"TitleChanQWERTY\" Praporshikov",
                "SELECT_PROFILE": "SELECT CHARACTER",
                "PRESS_ANY_KEY": "PRESS ANY KEY",
                "PLAY": "PLAY",
                "WEAPON_MENU": "WEAPON MENU",
                "RECORDS": "STATISTICS",
                "OPTION": "OPTION",
                "EXTRA": "EXTRA",
                "EXIT": "EXIT",

                "TXT_CONFIRM": "Confirm",

                "EXTRA_CREDITS": "END?",

                "WERE_YOU_CAN_EXIT": "WERE YOU CAN EXIT?",
                "CHARACTER_MENU": "CHARACTER MENU",

                "SELECT_MODE": "SELECT MODE",
                "RECORD_MODE": "RECORD MODE",
                "SELECT_MUSIC": "SELECT MUSIC",
                "STORY_MODE": "STORY MODE",
                "IN_BUILD": "IN BUILDING",

                "SELECT_DIFFICULTY": "SELECT DIFFICULTY",
                "EASY_DIFFICULTY": "EASY",
                "NORMAL_DIFFICULTY": "NORMAL",
                "HARD_DIFFICULTY": "HARD",

                "MAGICAL_POWER": "MAGICAL POWER",
                "WEAPON": "WEAPON",
                "SCORE": "SCORE",
                "KILL": "KILL",
                "DAMAGE": "DAMAGE",
                "BOMB": "BOMB",
                "MAX_MAGICAL": "MAX MAGICAL",
                "MONEY": "MONEY",
                "AMMO": "AMMO",
                "AMMO_EMPTY": "EMPTY",
                "RESUME": "Resume",
                "RESTART": "Restart",
                "MAIN_MENU": "Main Menu",
                "BACK": "Back",
                "MAGICAL_GIRL_SLEEP": "MAGICAL GIRL SLEEP",
                "TXT_FINISH": "Finish",

                "NEW_WEAPON": "NEW WEAPON!",

                "PRICE": "PRICE",
                "UPGRADE_AIM": "AIM",
                "UPGRADE_STABILIZATION": "STABILIZATION",
                "UPGRADE_BOMB": "BOMB SHOOT",
                "INFO_AIM": "INFO: The magical girl begins to aim correctly, thereby increasing the damage",
                "INFO_STABILIZ": "INFO: Magical girl begins to get less nervous, therefore she shoots more steadily",
                "INFO_BOMB": "INFO: The magical girl stuck the bomb bay and can now shoot them",
                "OFF_WEAPON": "Disable",
                "CELL": "CELL",

                "FULLSCREEN": "FULLSCREEN",
                "LANGUAGE": "LANGUAGE",
                "AA_FONT": "ANTI-ALIASING FONT",
                "SFX_VOLUME": "SFX VOLUME",
                "MUSIC_VOLUME": "MUSIC VOLUME",
                "SELECT_AIM": "AIM",
                "SET_GAMEPAD": "Set up a Gamepad",

                "RANK": "RANK",
                "RANK_REQUEST": "RANK REQUEST",
                "EXP": "EXP",
                "HISCORE": "HISCORE",
                "TIME_PLAYED": "TIME PLAYED",

                "PLEASE_TYPE_NAME": "PLEASE TYPE CHARACTER NAME",
                "SET_RANDOM_NAME": "Set a random name",
                "PLEASE_TYPE_AGE": "PLEASE TYPE CHARACTER AGE (Leave blank to assign randomly)",
                "SELECT_PROFILE_TXT": "PLEASE SELECT CHARACTER ->",
                "DELETE_CHARACTER": "DELETE CHARACTER",
                "CREATE_CHARACTER": "CREATE CHARACTER",

                "WARNING_INFO": "WARNING! This game may contain a large number of violent, unpleasant, and disgusting scenes. The game also contains a large number of sharp flashes. The game can also cause different reactions to events. Be careful",

                "TXT_START_GAME": "I... I'm a magical girl. I am the one who stands before man and his fear. I am the one who wants to rid the world of all evil that exists. I should not be evil, I should not have fears... I should have nothing but a weapon with which to kill the elders.",
                "TXT_START_GAME_NAME": "My magic name is",
                "TXT_START_GAME_AGE": "My magical age",
                "TXT_START_GAME_NEXT": "I want everyone in this holy place to believe in me, and in my power, because it was given to me by the goddess Adriana herself",
                "TXT_START_GAME_FINAL": "I will not resign as a magical girl, and I will be faithful to this world",

                "TXT_ACCEPT_RULE": "Do you accept all this?",
                "TXT_YES": "YES",
                "TXT_NO": "no",
                "TXT_SKIP": "Skip",

                "TUTORIAL_TXT_ONE": "For collect item press key \"E\"",
                "TUTORIAL_TXT_TWO": "For open container press key \"E\"",
                "TUTORIAL_TXT_THREE": "For switch weapon press key \"J\" or \"L\"",
                "TUTORIAL_TXT_FOUR": "You have to kill this enemy. Press key \"K\" to shoot",
                "TUTORIAL_TXT_FINAL": "Now you know how to kill",
                "TUTORIAL_TXT_FINAL_TWO": "Kill with all the power that is within you",

                "TUTORIAL_TXT_ONE_GAMEPAD": "For collect item press button \"A\"",
                "TUTORIAL_TXT_TWO_GAMEPAD": "For open container press button \"A\"",
                "TUTORIAL_TXT_THREE_GAMEPAD": "For switch weapon press \"LB\" or \"RB\"",
                "TUTORIAL_TXT_FOUR_GAMEPAD": "You have to kill this enemy. Press button \"B\" to shoot",

                "TUTORIAL_TXT_ONE_GAMEPAD_D": "For collect item press button \"X\"",
                "TUTORIAL_TXT_TWO_GAMEPAD_D": "For open container press button \"X\"",
                "TUTORIAL_TXT_THREE_GAMEPAD_D": "For switch weapon press \"L1\" or \"R1\"",
                "TUTORIAL_TXT_FOUR_GAMEPAD_D": "You have to kill this enemy. Press button \"O\" to shoot",

                "STORY_TREE": "STORY TREE",

                "CONFIRM_EXIT": "Do you really want to leave?",

                "KILL_MESSAGE_ANGLE": "KILLED BY THE HANDS OF AN ANGLE",

                "START_SCENE_1": "Tell me your name...",
                "START_SCENE_2": "How old are you?",

                "START_SCENE_2_2": "Is it just me, or are you too young.",
                "START_SCENE_2_3": "A great age for such a responsible job.",
                "START_SCENE_2_4": "Are you too old for this.",

                "START_SCENE_3": "Me? Someone who knows you.",
                "START_SCENE_4": "I am an overseer, a director, an advisor, and an entity that watches over this world.",

                "START_SCENE_5": "Adriana gave me another role in this world. I have to recruit magical girls like you.",
                "START_SCENE_6": "I have great hope that your story will be good. Do your best to make it work this time.",

                "START_SCENE_PLAYER_WHO_YOU": "Who are you?",


                "START_CUTSCENE": "Hey!",
                "START_CUTSCENE_2": "Did you really decide to come here?",
                "START_CUTSCENE_3": "I have to tell you something",
                "START_CUTSCENE_4": "So...",
                "START_CUTSCENE_5": "This is a world where your every fear is a living creature",
                "START_CUTSCENE_6": "They can harm you and others",
                "START_CUTSCENE_7": "At one point, the gods got tired of it all and created magic",
                "START_CUTSCENE_8": "At the same time, they created a new faith, and a god for it",
                "START_CUTSCENE_9": "A girl who died a horrible death became God",
                "START_CUTSCENE_10": "Her name was",
                "START_CUTSCENE_11": "Adriana",
                "START_CUTSCENE_12": "She protected this world from fears and other bad things",
                "START_CUTSCENE_13": "But at one point, it created those who are supposed to protect us from fears",
                "START_CUTSCENE_14": "These were people who received the magical power to kill. They were magical girls who sacrificed their lives for us",
                "START_CUTSCENE_15": "We should be grateful to them for sacrificing their lives for us",
                "START_CUTSCENE_16": "now...",
                "START_CUTSCENE_17": "Become a magical girl and protect us all!",

                "END_SCENE_1": "\"Kisser\": End?",
                "END_SCENE_2": "Adriana: No. She did not cope with the task",
                "END_SCENE_3": "\"Kisser\": But how?",
                "END_SCENE_4": "Adriana: Destroying one evil does not mean that it will disappear",
                "END_SCENE_5": "Adriana: Evil is something that can multiply from other evil",
                "END_SCENE_6": "\"Kisser\": So we should look for new people?",
                "END_SCENE_7": "Adriana: Yes...",
                "END_SCENE_8": "Adriana: Find me new girls, and let them destroy evil",
                "END_SCENE_9": "\"Kisser\": All for you",

                "CREDITS_PROGRAMING": "Programing:",
                "CREDITS_ART": "Art:",
                "CREDITS_SFX": "SFX:",
                "CREDITS_MUSIC": "Music:",
                "CREDITS_TOOLS": "Using Tools:",
                "CREDITS_SPECIAL": "Special Thanks:",
                "CREDITS_YOU": "And YOU)",

                "TIPS_1": "Tip: You can use \"Space\" to quickly move around",
                "TIPS_1_2": "Tip: You can use \"X\" to quickly move around",
                "TIPS_1_3": "Tip: You can use \"SQUARE\" to quickly move around",
                "TIPS_2": "Tip: If you kill enemies, you accumulate a \"MAGICAL POWER\" that increases your damage and speed",
                "TIPS_3": "Tip: Collect 3 shields to increase your defense by 50",
                "TIPS_4": "Magical girls are evil",
                "TIPS_5": "Tip: Trident (electric shocker) cannot wound some enemies",
                "TIPS_6": "Tip: Upgrade your weapons in the \"Weapons Menu\"",
                "TIPS_7": "Tip: If the \"MAGICAL POWER\" is maxed out, damage and speed increase.",

                "BOSS_WARNING": "ATTENTION BOSS IS COMING!!!",

                "TIP_V_KEYBOARD_1": "Use the gamepad stick, or touch screen to press a key. To enter on the gamepad, press",

                "SELECT_ICON_GAMEPAD": "Select your gamepad's control icons"
            }


        with open("Language (Dont Remove)/" + "ENG.json", "w") as f:
            json.dump(data, f)

    LANG = os.listdir("Language (Dont Remove)")
    return LANG


LANGUAGE = check_lang()

txt_lang = None

select_lang = 0


def set_language():
    global txt_lang
    with open("Language (Dont Remove)/" + LANGUAGE[select_lang], encoding='utf-8') as lang:
        txt_lang = json.load(lang)


set_language()
