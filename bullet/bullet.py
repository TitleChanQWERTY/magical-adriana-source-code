import pygame
from group_conf import bullet_enemy_group
from config import sc
from scene import scene_config

bullet_Sprite = (pygame.image.load("Assets/sprite/bullet/enemy/1.png").convert_alpha(),
                 pygame.image.load("Assets/sprite/bullet/enemy/2.png").convert_alpha(),
                 pygame.image.load("Assets/sprite/bullet/enemy/3.png").convert_alpha(),
                 pygame.image.load("Assets/sprite/bullet/enemy/4.png").convert_alpha(),
                 pygame.image.load("Assets/sprite/bullet/enemy/5.png").convert_alpha(),
                 pygame.image.load("Assets/sprite/bullet/enemy/6.png").convert_alpha())


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, speed_x, speed_y, filename, size, group=bullet_enemy_group,
                 damage=3, time_destroy=900, isStat=False):
        super().__init__()
        self.image = filename
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.size = size

        self.add(group)

        self.group = group

        self.damage = scene_config.enemy_bullet_damage

        self.speed_x = speed_x
        self.speed_y = speed_y

        if group == bullet_enemy_group and self.speed_y == 0 and time_destroy > 90 and not isStat:
            self.speed_y = 1

        self.timeDestroy = time_destroy

        self.alpha = 300

    def update(self):
        if self.group == bullet_enemy_group:
            pygame.draw.circle(sc, (255, 0, 0), self.rect.center, self.size[0]-2, 2)

        if self.rect.y >= 593 or self.rect.y < 80 or self.timeDestroy < 0:
            self.kill()
        elif self.timeDestroy < 55:
            self.alpha -= 4
            self.image.set_alpha(self.alpha)

        self.timeDestroy -= 1
        self.rect.move_ip(self.speed_x, self.speed_y)
