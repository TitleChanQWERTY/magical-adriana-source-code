from random import randint

import pygame
from scene import scene_config
from group_conf import bomb_boom_group, enemy_group

sprite_bomb = pygame.image.load("Assets/sprite/bullet/player/bomb/1.png").convert()
area_bomb = pygame.image.load("Assets/sprite/bullet/player/bomb/area_bomb.png").convert_alpha()


class AreaBomb(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.max_size = randint(140, 150)
        self.size = 0
        self.image = area_bomb
        self.image = pygame.transform.scale(self.image, (self.size, self.size))
        self.rect = self.image.get_rect(center=(x, y))

        self.x = x
        self.y = y

        self.add(bomb_boom_group)

        self.timeShow = 0

        self.Alpha = 300

    def update(self):
        if self.timeShow >= 29:
            self.Alpha -= 15
            self.image.set_alpha(self.Alpha)
            if self.Alpha <= 20:
                self.kill()
        else:
            self.timeShow += 1

        if self.size < self.max_size:
            self.size += 15
            self.image = area_bomb
            self.image = pygame.transform.scale(self.image, (self.size, self.size))
            self.rect = self.image.get_rect(center=(self.x, self.y))


class Bomb(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = sprite_bomb
        self.image = pygame.transform.scale(self.image, (11 * 2, 11 * 2))
        self.rect = self.image.get_rect(center=(x, y))

        self.x = x
        self.y = y

        self.add(bomb_boom_group)

        self.force_move = 0.5

        self.time_boom = 0

        self.angle: int = 0

    def update(self):
        if self.rect.y <= 90:
            self.force_move = -randint(12, 15)
        else:
            self.force_move += 0.5
        self.y -= self.force_move

        if self.time_boom >= 33 and self.rect.y > 105:
            AreaBomb(self.rect.centerx, self.rect.centery)
            self.kill()

        for enemy in enemy_group:
            if self.rect.colliderect(enemy.rect):
                AreaBomb(self.rect.centerx, self.rect.centery)
                self.kill()

        self.time_boom += 1

        self.image = sprite_bomb
        self.angle += 1
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.image = pygame.transform.scale(self.image, (8 * 2, 8 * 2))
        self.rect = self.image.get_rect(center=(self.x, self.y))
