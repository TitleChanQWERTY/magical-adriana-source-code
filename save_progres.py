import os
import json
import pygame
import shutil

import language_system
from config import WINDOW_SIZE
from player.weapon import Standart_weapon, MagicPistol, MagicShotgun, MagicMinigun, MagicTrident
from scene import scene_config
from group_conf import notification_group
import gamepad_calibrate

save_icon_img = (pygame.image.load("Assets/sprite/ui/notf_icon/save_icon.png").convert_alpha(),
                 pygame.image.load("Assets/sprite/ui/notf_icon/save_option_icon.png").convert_alpha())


class SaveIcon(pygame.sprite.Sprite):
    def __init__(self, icon=save_icon_img[0], x=WINDOW_SIZE[0] - 150, size=(12 * 3, 13 * 3)):
        super().__init__()
        self.image = icon
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, 60))

        self.alpha: int = 300
        self.time_live = 0

        self.add(notification_group)

    def update(self):
        if self.time_live > 70:
            self.kill()
        if self.alpha < 20:
            self.alpha = 300

        self.alpha -= 8
        self.time_live += 1

        self.image.set_alpha(self.alpha)


def save_config(fullscreen):
    try:
        os.remove("config.json")
    except FileNotFoundError:
        pass
    data = {
        "FULLSCREEN": fullscreen,
        "LANGUAGE": language_system.select_lang,
        "AA_TEXT": scene_config.AA_TEXT,
        "sfx_volume": scene_config.sfx_volume,
        "music_volume": scene_config.music_volume,
        "select_aim": scene_config.select_aim,
        "input_sys": gamepad_calibrate.input_sys
    }
    with open("config.json", "w") as file:
        json.dump(data, file)
    SaveIcon(save_icon_img[1], WINDOW_SIZE[0] - 85, (13 * 3, 13 * 3))
    return 0


def save():
    profile_file = os.listdir("Profile")
    os.remove("Profile/" + profile_file[scene_config.p.profile] + "/data.json")
    shutil.rmtree("Profile/" + profile_file[scene_config.p.profile] + "/Weapon")
    weapon_st = []    

    data = {
        "NAME": str(scene_config.p.NAME),
        "AGE": int(scene_config.p.AGE),

        "SCORE": scene_config.p.HISCORE,
        "MONEY": scene_config.p.MONEY,
        "EXP": scene_config.p.EXP,
        "RANK": scene_config.p.RANK,
        "TIME_PLAYED": scene_config.time_played,

        "FIRST_RUN": scene_config.p.isFirstRun,
        
        "PAPPER": scene_config.p.paper_story

    }
    with open("Profile/" + profile_file[scene_config.p.profile] + "/data.json", "w") as file:
        json.dump(data, file)

    os.mkdir("Profile/"+profile_file[scene_config.p.profile]+"/Weapon")

    for b, weapon in enumerate(scene_config.p.weapon):
        data_weapon = {
            "NAME": weapon.name_w,
            "isAim": weapon.isAim,
            "isStab": weapon.isStabilization,
            "isBomb": weapon.isBombShoot,
            "isOn": weapon.isOn
        }
        with open("Profile/"+profile_file[scene_config.p.profile]+"/Weapon/"+str(b)+".json", "w") as file_w:
            json.dump(data_weapon, file_w)

    SaveIcon()
    return 0
