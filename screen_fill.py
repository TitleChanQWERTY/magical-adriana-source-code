import pygame
from group_conf import fill_screen
from config import WINDOW_SIZE


class ScreenFill(pygame.sprite.Sprite):
    def __init__(self, alpha, alpha_speed, color, time):
        super().__init__()
        self.image = pygame.Surface(WINDOW_SIZE)
        self.alpha = alpha
        self.image.set_alpha(self.alpha)
        self.image.fill(color)
        self.rect = self.image.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2))

        self.alpha_speed = alpha_speed

        self.time_live = 0

        self.time_max = time

        self.add(fill_screen)

    def update(self):
        if self.time_live >= self.time_max:
            self.kill()
            return
        self.time_live += 1

        self.alpha += self.alpha_speed

        self.image.set_alpha(self.alpha)
