import json

from pygame import display, time, init, image, transform, font, NOFRAME, joystick

init()
joystick.init()

joystick_m = None

if joystick.get_count() > 0:
    joystick_m = joystick.Joystick(0)
    joystick_m.init()

WINDOW_SIZE = (1200, 700)

config_file = None

sc = display.set_mode((50 * 17, 18 * 17), NOFRAME)

clock = time.Clock()

key_image = {"e": image.load("Assets/sprite/ui/keycaps/e.png").convert_alpha(),
             "backspace": image.load("Assets/sprite/ui/keycaps/backspace.png").convert_alpha(),
             "n": image.load("Assets/sprite/ui/keycaps/n.png").convert_alpha(),
             "enter": image.load("Assets/sprite/ui/keycaps/enter.png").convert_alpha(),
             "esc": image.load("Assets/sprite/ui/keycaps/esc.png").convert_alpha(),
             "capslock": image.load("Assets/sprite/ui/keycaps/capslock.png").convert_alpha(),
             "space": image.load("Assets/sprite/ui/keycaps/space.png").convert_alpha(),
             "arrow_right": image.load("Assets/sprite/ui/keycaps/arrow.png").convert_alpha(),
             "arrow_left": image.load("Assets/sprite/ui/keycaps/arrow.png").convert_alpha(),
             "j": image.load("Assets/sprite/ui/keycaps/j.png").convert_alpha(),
             "l": image.load("Assets/sprite/ui/keycaps/j.png").convert_alpha()}

key_image["e"] = transform.scale(key_image["e"], (25, 25))
key_image["j"] = transform.scale(key_image["j"], (16 * 3, 16 * 3))
key_image["l"] = transform.scale(key_image["l"], (16 * 3, 16 * 3))
key_image["n"] = transform.scale(key_image["n"], (25, 25))
key_image["esc"] = transform.scale(key_image["esc"], (27, 26))
key_image["capslock"] = transform.scale(key_image["capslock"], (28, 27))
key_image["backspace"] = transform.scale(key_image["backspace"], (45 * 2, 16 * 2))
key_image["enter"] = transform.scale(key_image["enter"], (45 * 2, 16 * 2))
key_image["space"] = transform.scale(key_image["space"], (45 * 2, 15 * 2))

key_image["arrow_right"] = transform.scale(key_image["arrow_right"], (25, 25))

key_image["arrow_left"] = transform.scale(key_image["arrow_left"], (25, 25))
key_image["arrow_left"] = transform.flip(key_image["arrow_left"], True, False)

gamepad = {"A": image.load("Assets/sprite/ui/gamepad_key/xbox/a.png").convert_alpha(),
           "B": image.load("Assets/sprite/ui/gamepad_key/xbox/b.png").convert_alpha(),
           "Y": image.load("Assets/sprite/ui/gamepad_key/xbox/y.png").convert_alpha(),
           "X": image.load("Assets/sprite/ui/gamepad_key/xbox/x.png").convert_alpha(),
           "LB": image.load("Assets/sprite/ui/gamepad_key/xbox/lb.png").convert_alpha(),
           "RB": image.load("Assets/sprite/ui/gamepad_key/xbox/rb.png").convert_alpha()}

gamepad["A"] = transform.scale(gamepad["A"], (15 * 2, 15 * 2))
gamepad["B"] = transform.scale(gamepad["B"], (15 * 2, 15 * 2))
gamepad["Y"] = transform.scale(gamepad["Y"], (15 * 2, 15 * 2))
gamepad["X"] = transform.scale(gamepad["X"], (15 * 2, 15 * 2))
gamepad["RB"] = transform.scale(gamepad["RB"], (17 * 3, 17 * 3))
gamepad["LB"] = transform.scale(gamepad["LB"], (17 * 3, 17 * 3))

gamepad_d = {"A": image.load("Assets/sprite/ui/gamepad_key/play/x.png").convert_alpha(),
           "B": image.load("Assets/sprite/ui/gamepad_key/play/circle.png").convert_alpha(),
           "Y": image.load("Assets/sprite/ui/gamepad_key/play/triangle.png").convert_alpha(),
           "X": image.load("Assets/sprite/ui/gamepad_key/play/square.png").convert_alpha(),
           "LB": image.load("Assets/sprite/ui/gamepad_key/play/l1.png").convert_alpha(),
           "RB": image.load("Assets/sprite/ui/gamepad_key/play/r1.png").convert_alpha()}


gamepad_d["A"] = transform.scale(gamepad_d["A"], (15 * 2, 15 * 2))
gamepad_d["B"] = transform.scale(gamepad_d["B"], (15 * 2, 15 * 2))
gamepad_d["Y"] = transform.scale(gamepad_d["Y"], (15 * 2, 15 * 2))
gamepad_d["X"] = transform.scale(gamepad_d["X"], (15 * 2, 15 * 2))
gamepad_d["RB"] = transform.scale(gamepad_d["RB"], (17 * 3, 17 * 3))
gamepad_d["LB"] = transform.scale(gamepad_d["LB"], (17 * 3, 17 * 3))

input_type_s = 0

def check_gamepad(input=0, isKeyboard=False):
    global input_type_s, key_image
    if not isKeyboard:
        if joystick.get_count() > 0 and input == 1:
            input_type_s = 1
            key_image["enter"] = gamepad_d["A"]
            key_image["space"] = gamepad_d["A"]
            key_image["e"] = gamepad_d["A"]
            key_image["n"] = gamepad_d["Y"]
            key_image["backspace"] = gamepad_d["X"]
            key_image["esc"] = gamepad_d["B"]
            key_image["j"] = gamepad_d["LB"]
            key_image["l"] = gamepad_d["RB"]
            return
        elif joystick.get_count() > 0 and input == 2:
            key_image["enter"] = gamepad["A"]
            key_image["space"] = gamepad["A"]
            key_image["n"] = gamepad["Y"]
            key_image["e"] = gamepad["A"]
            key_image["backspace"] = gamepad["X"]
            key_image["esc"] = gamepad["B"]
            key_image["j"] = gamepad_d["LB"]
            key_image["l"] = gamepad_d["RB"]
            return
        elif joystick.get_count() > 0:
            key_image["enter"] = gamepad["A"]
            key_image["space"] = gamepad["A"]
            key_image["n"] = gamepad["Y"]
            key_image["e"] = gamepad["A"]
            key_image["backspace"] = gamepad["X"]
            key_image["esc"] = gamepad["B"]
            key_image["j"] = gamepad["LB"]
            key_image["l"] = gamepad["RB"]
            return
    else:
        key_image["enter"] = image.load("Assets/sprite/ui/keycaps/enter.png").convert_alpha()
        key_image["space"] = image.load("Assets/sprite/ui/keycaps/space.png").convert_alpha()
        key_image["e"] = image.load("Assets/sprite/ui/keycaps/e.png").convert_alpha()
        key_image["n"] = image.load("Assets/sprite/ui/keycaps/n.png").convert_alpha()
        key_image["backspace"] = image.load("Assets/sprite/ui/keycaps/backspace.png").convert_alpha()
        key_image["esc"] = image.load("Assets/sprite/ui/keycaps/esc.png").convert_alpha()
        key_image["j"] = image.load("Assets/sprite/ui/keycaps/j.png").convert_alpha()
        key_image["l"] = image.load("Assets/sprite/ui/keycaps/l.png").convert_alpha()

        key_image["e"] = transform.scale(key_image["e"], (25, 25))
        key_image["n"] = transform.scale(key_image["n"], (25, 25))
        key_image["esc"] = transform.scale(key_image["esc"], (27, 26))
        key_image["capslock"] = transform.scale(key_image["capslock"], (28, 27))
        key_image["backspace"] = transform.scale(key_image["backspace"], (45 * 2, 16 * 2))
        key_image["enter"] = transform.scale(key_image["enter"], (45 * 2, 16 * 2))
        key_image["space"] = transform.scale(key_image["space"], (45 * 2, 15 * 2))
        key_image["j"] = transform.scale(key_image["j"], (16 * 3, 16 * 3))
        key_image["l"] = transform.scale(key_image["l"], (16 * 3, 16 * 3))
    


standart_font = "Assets/font/Hardpixel.otf"

rank_image = [image.load("Assets/sprite/ui/rank/0.png").convert_alpha(),
              image.load("Assets/sprite/ui/rank/1.png").convert_alpha(),
              image.load("Assets/sprite/ui/rank/2.png").convert_alpha(),
              image.load("Assets/sprite/ui/rank/3.png").convert_alpha(),
              image.load("Assets/sprite/ui/rank/4.png").convert_alpha(),
              image.load("Assets/sprite/ui/rank/5.png").convert_alpha(),
              image.load("Assets/sprite/ui/rank/6.png").convert_alpha()]

rank_image[0] = transform.scale(rank_image[0], (16 * 4, 16 * 4))
rank_image[1] = transform.scale(rank_image[1], (15 * 4, 5 * 4))
rank_image[2] = transform.scale(rank_image[2], (15 * 4, 5 * 4))
rank_image[3] = transform.scale(rank_image[3], (15 * 4, 10 * 4))
rank_image[4] = transform.scale(rank_image[4], (15 * 4, 10 * 4))
rank_image[5] = transform.scale(rank_image[5], (9 * 4, 9 * 4))
rank_image[6] = transform.scale(rank_image[6], (15 * 4, 15 * 4))

rank_max_exp = [25, 35, 80, 145, 250, 300, 350]

txt_ok = font.Font(standart_font, 22).render("OK", False, (255, 155, 255))
